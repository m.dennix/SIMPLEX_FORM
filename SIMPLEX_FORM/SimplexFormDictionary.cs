﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace simplex_FORM
{
    /// <summary>
    /// Classe che rappresenta un elemento della lista concatenata <b>SimplexFormDictionary</b>.<p></p>
    /// Ha tutti attributi protetti (internal) di cui tre accessibili in sola lettura mediante properties.<p></p>
    /// Non è possibile creare un'istanza di questa classe in modo esplicito.<p></p>
    /// <p></p>
    /// La classe viene impiegata per salvare lo stato dei controlli di una maschera senza usare l'oggetto DAO 
    /// di supporto (vedi gli oggetti della libreria Simplex_ORM). Ciò è particolarmente utile nelle maschere di ricerca e
    /// di il lancio dei report che non hanno un oggetto DAO di supporto.
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 13/01/2018 - Monterotondo - v.1.0.0.0
    /// </pre>
    /// </summary>
    public class SimplexFormEntry
    {
        #region ATTRIBUTI
        private String _ID = null;
        private String _Text = null;
        private String _ctrlType = "default";

        internal SimplexFormEntry next=null;
        internal SimplexFormEntry prev = null;

        #endregion

        #region PROPERTY
        /// <summary>
        /// Id del controllo della maschera
        /// </summary>
        public String ID
        {
            get
            {
                return _ID;
            }
        }

        /// <summary>
        /// Testo del controllo della maschera
        /// </summary>
        public String Text
        {
            get
            {
                return _Text;
            }
        }

        /// <summary>
        /// Nome del controllo web, omesso il namespace System.web.UI.Controls.
        /// </summary>
        public String ctrlType
        {
            get
            {
                return _ctrlType;
            }
        }
        #endregion

        #region COSTRUTTORE
        /// <summary>
        /// Costruttore di default privato.
        /// </summary>
        private SimplexFormEntry()
        {
            //NOP
        }

        /// <summary>
        /// Costruttore interno al namespace di una Entry.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 12/01/2017
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Proprietà ID del controllo web. Se null lancia un'eccezione.</param>
        /// <param name="p_Text">Proprietà Text del contollo web (nullable) </param>
        /// <param name="p_ctrlType">Nome del Tipo del controllo web, omesso il namespace System.Web.UI.WebControls. (nullable - default "default")</param>
        internal SimplexFormEntry(String p_ID, String p_Text, String p_ctrlType)
        {
            if (p_ID == null)
                throw new Exception("Il parametro ID non è stato valorizzato");

            _ID = p_ID;

            _Text = p_Text;

            if (p_ctrlType == null)
                _ctrlType = "default";
            else
                _ctrlType = p_ctrlType;
        }
        #endregion
    }

    /// <summary>
    /// Classe che rappresenta un iteratore sulla lista concatenata.
    /// Implementa i metodi MoveNext(), Reset() e la propertu Currrent.
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 13/01/2018 - Monterotondo - v.1.0.0.0
    /// </pre>
    /// </summary>
    public class SimplexFormEnumerator : System.Collections.IEnumerator
    {
        private SimplexFormEntry _first = null;
        private SimplexFormEntry _last = null;
        private SimplexFormEntry _current = null;
        private int _idx = -1;                      //posizione inziale.

        #region COSTRUTTORI
        /// <summary>
        /// Costruttore da NON usare.
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018
        /// </summary>
        private SimplexFormEnumerator()
        {
            //NOP
        }


        /// <summary>
        /// Costruttore DA USARE.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018
        /// </pre>
        /// </summary>
        /// <param name="p_lista">Elemento di testa della lista concatenata.</param>
        internal SimplexFormEnumerator(SimplexFormEntry p_lista)
        {
            _current = _first = p_lista;

            /// alorizzazione di _last.
            while (_current != null)
            {
                _last = _current;
                _current = _current.next;
            }

            // Appena creato l'enumeratore è posizionato prima del primo elemento.
            _current = null;
        }

        #endregion

        #region Implementazione IEnumerator
        /// <summary>
        /// Avanza di un'Entry.
        /// La prima invocazione di MoveNext posiziona l'enumeratore sul primo elemento.
        /// Una volta arrivato a fondo corsa,l'enumeratore permane dopo l'ultimo elemento 
        /// fintanto che non viene chiamato Reset().
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 12/01/2018
        /// </pre>
        /// </summary>
        /// <returns>True se è avanzato, false altrimenti.</returns>
        public bool MoveNext()
        {
            //if (_current != null && _current.next != null)
            if (_current == null && _idx==-1 && _first!=null)
            {
                _current = _first;
                _idx = 0;
                return true;
            }

            if (_current != null)
            {
                _current = _current.next;
                if (_current == null)
                    return false;
                _idx += 1;
                return true;
            }
            else
                return false;
        }

        /// <summary> 	
        ///Imposta l'enumeratore sulla propria posizione iniziale, 
        /// ovvero prima del primo elemento nella lista concatenata.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018
        /// </pre>
        /// </summary>
        public void Reset()
        {
            _idx = -1;
            _current = null;
        }

        /// <summary>
        /// Ottiene l'elemento della raccolta in corrispondenza della posizione corrente dell'enumeratore.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018
        /// </pre>
        /// </summary>
        public Object Current
        {
            get
            {
                return _current;
            }
        }

        #endregion
    }

    /// <summary>
    /// Classe IDictionary, IEnumerable a tre attributi: <p></p>
    /// - <b>ID</b>:           chiave.<p></p>
    /// - <b>Text</b>:         Valore.<p></p>
    /// - <b>Type</b>:         Tipo di valore.<p></p>
    /// Il "Tipo di Valore" è un'informazione descrittiva che rappresenta la tipologia del valore (cioè dell'attributo Text). <br></br>
    /// Ad esempio, se le coppie (ID, Text) raccolte nel Dictionary rappresentano gli identificativi ed i corrispondenti 
    /// valori dei controlli di una pagina web, l'attributo <b>Type</b> può essere usato per fornire indicazioni sui tipi 
    /// di tali controlli web, come "<i>TextBox</i>" o "<i>DropDownList</i>" etc.. <br></br>
    /// Per default il valore di <b>Type</b> è: "default".<p></p>
    /// 
    /// Il Dictionary viene implementato come una lista non ordinata a concatenazione doppia di elementi di classe <i>SimplexFormEntry</i>.  <br></br>
    /// L'accesso è a scorrimento lineare monodirezionale.<p></p>
    /// 
    /// Possiede metodi per:<p></p>
    /// - (publ.) Accodare (Append); <p></p>
    /// - (publ.) Inserire in testa (AddOnTop); <p></p>
    /// - (publ.) Ottenere una Entry data per chiave (getElement).<p></p>
    /// - (publ.) Ottenere la Entry in una data posizione (getElementAt);<p></p>
    /// - (publ.) Inserire in una data posizione; <p></p>
    /// - (publ.) Rimuovere la Entry in una data posizione (Remove);<p></p>
    /// - (publ.) Rimuovere la Entry data per chiave (Remove);<p></p>
    /// - (publ.) Cancellare la lista concatenata (rimuovere tutte le entry).<p></p>
    /// - (publ.) contains(): La classe NON ammette duplicazioni di chiavi. Esiste, quindi, un metodo pubblico che 
    ///   verifica l'esistenza di una chiave nella lista.<p></p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 13/01/2018
    /// Mdfd: 25/05/2018 v. 1.2.0.3: 
    ///                             - Corretto il BUG su getElement()
    ///                             - Cambiato il tipo restituito dal metodo "this[String p_Key]".
    ///     
    /// </pre>
    /// <remarks>
    /// Se si tenta di inserire per posizione una chiave esistente, la nuova chiave sostiuisce la precedente che, indipendentemente dalla posizione
    /// in cui si trova, viene rimossa.
    /// </remarks>
    /// </summary>
    public class SimplexFormDictionary:System.Collections.IEnumerable, System.Collections.IDictionary
    {
        #region ATTRIBUTI
        /// <summary>
        /// Primo elemento della lista concatenata
        /// </summary>
        protected SimplexFormEntry _first = null;
        /// <summary>
        /// Ultimo elemento della lista concatenata
        /// </summary>
        protected SimplexFormEntry _last = null;
        #endregion

        #region METODI E PROPERTIES DI ICollection
        /// <summary>
        /// Ottiene un oggetto che può essere usato per sincronizzare l'accesso a ICollection.<br></br>
        /// L'oggetto è della classe SimpleFormDictionary stesso.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        object System.Collections.ICollection.SyncRoot
        {
            get
            {
                return this;
            }
        }

        /// <summary>
        /// Restituisce il numero di elementi nella Collection
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <returns>Numero di elementi nella Collection</returns>
        public int Count
        {
            get
            {
                return count();
            }
        }

        /// <summary>
        /// Restituisce il numero di elementi nella Collection.<br>
        /// </br>
        /// Diciarazione esplicita.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <returns>Numero di elementi nella Collection</returns>
        int System.Collections.ICollection.Count
        {
            get
            {
                return count();
            }
        }

        /// <summary>
        /// L'oggetto non è sincronizzato.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Copia gli elementi di SimplexFormDictionary in un Array a partire da una sua particolare posizione specificata da un indice.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/01/2018
        /// </pre>
        /// </summary>
        /// <param name="p_Array">Array di destinazione di lunghezza sufficiente.</param>
        /// <param name="p_from">Indice (0-BASED) della cella dell'array a partire dal quale copiare gli elementi del dictionary.</param>
        /// <exception cref="ArgumentNullException">il Parametro p_Array non è stato valorizzato</exception>
        /// <exception cref="ArgumentException">Array multidimensionale OPPURE Lunghezza insufficiente a contenere gli elementi della Collection OPPURE impossibile eseguire il cast implicito sul tipo delle celle dell'array.</exception>
        /// <exception cref="ArgumentOutOfRangeException">L'indice specificato è negativo.</exception>
        public void CopyTo(System.Array p_Array, int p_from)
        {
            if (p_Array == null)
                throw new ArgumentNullException("p_Array", "Il parametro p_Array non è stato valorizzato");

            if (p_from < 0)
                throw new ArgumentOutOfRangeException("p_Array", "L'indice specificato è negativo");

            if (p_Array.Rank > 1)
                throw new ArgumentException("Array multidimensionale", "p_Array");

            if(p_Array.Length - p_from < Count)
                throw new ArgumentException("Lunghezza insufficiente a contenere gli elementi della Collection.", "p_Array");

            // ciclo di estrazione
            SimplexFormEntry _current = _first;
            int _idx = 0;
            while (_current != null && _idx<p_Array.Length)
            {
                p_Array.SetValue(_current,_idx);
                _current = _current.next;
                _idx++;
            }
            return;
        }
        #endregion

        #region METODI E PROPERTIES DI IDictionary
        /// <summary>
        /// Rimuove l'elemento con la chiave specificata dall'oggetto IDictionary.
        /// <param name="p_Obj">Oggetto di tipo stringa che rappresenta la chiave dell'elemento da rimuovere</param>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 27/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        /// <exception cref="ArgumentNullException">se la chiave non è specificata</exception>
        /// <exception cref="ArgumentException">se l'oggetto chiave non è una stringa.</exception>
        void System.Collections.IDictionary.Remove(object p_Obj)
        {
            if (p_Obj == null)
                throw new ArgumentNullException("p_Obj");
            if (p_Obj.GetType().Name.ToLower().CompareTo("string") != 0)
                throw new ArgumentException("p_Obj");
            remove(p_Obj.ToString());
        }

        /// <summary>
        /// Ottiene o imposta l'elemento con la chiave specificata. Dichiarazione esplicita.<br></br>
        /// In inserimento, se la chiave esiste, il suo valore viene sostituito con quello nuovo fornito.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Key">Chiave (non nullabile)</param>
        /// <returns>Elemento con la chiave specificata oppure null se la chiave non esiste.</returns>
        public object this[object p_Key]
        {
            get
            {
                if (p_Key == null)
                    throw new ArgumentNullException("p_Key");

                if (p_Key.GetType().Name.ToLower().CompareTo("string") != 0)
                    throw new ArgumentException("p_Key");

                if (contains(p_Key.ToString()) == false)
                    return null;

                int _idx=0;
                _idx = indexOf(p_Key.ToString());

                return getElementAt(_idx);
            }
            set
            {
                if (p_Key == null)
                    throw new ArgumentNullException("p_Key");

                if (p_Key.GetType().Name.ToLower().CompareTo("string") != 0)
                    throw new ArgumentException("p_Key");
                
                this.Append(p_Key.ToString(), value.ToString(), null);
            }
        }
        /// <summary>
        /// Da accesso in RO al dictionary per indice.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 26/02/2018
        /// </pre>
        /// </summary>
        /// <param name="p_i"></param>
        /// <returns></returns>
        public object this[int p_i]
        {
            get
            {
                return getElementAt(p_i);
            }
        }


        /// <summary>
        /// Restituisce un oggetto ICollection contenente i valori del Dictionary.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        System.Collections.ICollection System.Collections.IDictionary.Values
        {
            get
            {
                System.Collections.Generic.List<String> _vals = new List<string>();
                SimplexFormEntry _current = _first;
                while (_current != null)
                {
                    _vals.Add(_current.Text);
                    _current = _current.next;
                }
                return _vals;
            }
        }

        /// <summary>
        /// Restituisce un oggetto ICollection contenente le chiavi del Dictionary.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        System.Collections.ICollection System.Collections.IDictionary.Keys
        {
            get
            {
                System.Collections.Generic.List<String> _keys = new List<string>();
                SimplexFormEntry _current = _first;
                while (_current != null)
                {
                    _keys.Add(_current.ID);
                    _current = _current.next;
                }
                return _keys;
            }
        }

        /// <summary>
        /// Restituisce un valore che indica se l'oggetto IDictionary è di sola lettura.<pre></pre>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2017 Monterotondo
        /// </pre>
        /// </summary>
        bool System.Collections.IDictionary.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determina se l'oggetto IDictionary contiene un elemento con la chiave specificata.<pre></pre>
        /// Per SmplexFormDictionary è sempre false.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        bool System.Collections.IDictionary.IsFixedSize
        {
            get
            {
                return IsFixedSize;
            }
        }

        /// <summary>
        /// Determina se l'oggetto IDictionary contiene un elemento con la chiave specificata.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Key">Chiave</param>
        /// <returns>True se il Dictionary contiene la chiave specificata</returns>
        /// <exception cref="ArgumentNullException">Chiave specificata nulla.</exception>
        /// <exception cref="ArgumentException">Chiave specificata non è una stringa.</exception>
        bool System.Collections.IDictionary.Contains(object p_Key)
        {
            if (p_Key == null)
                throw new ArgumentNullException("p_Key");

            if(p_Key.GetType().Name.ToLower().CompareTo("string")!=0)
                throw new ArgumentException("L'argomento non è una stringa", "p_Key");

            return contains(p_Key.ToString());
        }

        /// <summary>
        /// Rimuove tutti gli elementi dall'oggetto IDictionary.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </summary>
        void System.Collections.IDictionary.Clear()
        {
            clear();
        }
        
        /// <summary>
        /// Restituisce un oggetto che implementa l'interfaccia di classe SimplexFormEnumerator
        /// Implementazione esplicita dell'interfaccia System.Collections.IDictionaryEnumerator.<p></p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <returns>Un enumeratore di classe System.Collections.IDictionaryEnumerator.</returns>
        System.Collections.IDictionaryEnumerator System.Collections.IDictionary.GetEnumerator()
        {
            return (System.Collections.IDictionaryEnumerator)GetEnumerator();
        }

        /// <summary>
        /// Aggiunge un elemento con la chiave e il valore forniti all'oggetto IDictionary.<br></br>
        /// Se il valore della chiave esiste genera eccezione.<br></br>
        /// </summary>
        /// <param name="p_Key">Oggetto chiave (non nullabile)</param>
        /// <param name="p_Value">Oggetto Valore (nullabile)</param>
        /// <exception cref="ArgumentNullException">In caso di chiave nulla.</exception>
        /// <exception cref="ArgumentException">Se un argomento è un oggetto di tipo diverso da String o string.</exception>
        void System.Collections.IDictionary.Add(object p_key, object p_value)
        {
            if (p_key == null)
                throw new ArgumentNullException("p_key");

            if (p_key.GetType().Name.ToLower().CompareTo("string") != 0)
                throw new ArgumentException("I parametri devono essere delle stringhe", "p_key");

            if (p_value.GetType().Name.ToLower().CompareTo("string") != 0)
                throw new ArgumentException("I parametri devono essere delle stringhe", "p_value");

            if(contains(p_key.ToString())==true)
                throw new ArgumentException("La chiave già esiste", "p_key");

            add(p_key.ToString(), p_value.ToString());
        }

        #endregion

        #region METODI PUBBLICI PROPRI DELLA CLASSE

        /// <summary>
        /// Aggiunge un elemento in coda al Dictionary se non è presente un altro elemento
        /// con il medesimo valore della chiave ID, altrimenti esegue una sostituzione
        /// nella posizione di quest'ultimo.<p></p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:14/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Chiave (obbl.)</param>
        /// <param name="p_Text">Valore.</param>
        /// <param name="p_Type">Tipo.</param>
        public void Append(String p_ID, String p_Text, String p_Type)
        {
            append(p_ID, p_Text, p_Type);
        }

        /// <summary>
        /// Ottiene o imposta l'elemento con la chiave specificata.<br></br>
        /// In inserimento, se la chiave esiste, il suo valore viene sostituito con quello nuovo fornito.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// Mdfd: 25/05/2018 v.1.2.0.3 - restituisce una stringa e non più un oggetto di classe SimplexFormEntry.
        /// </pre>
        /// </summary>
        /// <param name="p_Key">Chiave (non nullabile)</param>
        /// <returns>Elemento con la chiave specificata oppure null se la chiave non esiste.</returns>
        public String this[String p_Key]
        {            
            get
            {
                SimplexFormEntry _x = null;

                if (p_Key == null)
                    throw new ArgumentNullException("p_Key");

                if (contains(p_Key) == false)
                    return null;

                int _idx = indexOf(p_Key);


                _x = (SimplexFormEntry)getElementAt(_idx);
                if (_x != null)
                    return _x.Text;
                else
                    return null;
            }
            set
            {
                if (p_Key == null)
                    throw new ArgumentNullException("p_Key");
                this.append(p_Key, value.ToString(), null);
            }
        }

        /// <summary>
        /// Restituisce un oggetto ICollection contenente i valori del Dictionary.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        public System.Collections.Generic.List<String> Values
        {
            get
            {
                System.Collections.Generic.List<String> _vals = new List<string>();
                SimplexFormEntry _current = _first;
                while (_current != null)
                {
                    _vals.Add(_current.Text);
                    _current = _current.next;
                }
                return _vals;
            }
        }


        /// <summary>
        /// Restituisce un oggetto List contenente le chiavi del Dictionary.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        public System.Collections.Generic.List<String> Keys
        {
            get
            {
                System.Collections.Generic.List<String> _keys = new List<string>();
                SimplexFormEntry _current = _first;
                while (_current != null)
                {
                    _keys.Add(_current.ID);
                    _current = _current.next;
                }
                return _keys;
            }
        }

        /// <summary>
        /// Restituisce un valore che indica se l'oggetto di classe IDictionary è di sola lettura.<pre></pre>
        /// SimplexFormDictionary è sempre in lettura/scrittura.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2017 Monterotondo
        /// </pre>
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determina se l'oggetto IDictionary contiene un elemento con la chiave specificata.<pre></pre>
        /// Per SmplexFormDictionary è sempre false.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        public bool IsFixedSize
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Rimuove tutti gli elementi dall'oggetto IDictionary.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        public void clear()
        {
            _first = _last = null;
        }

        /// <summary>
        /// Aggiunge un elemento con la chiave e il valore forniti all'oggetto IDictionary.<br></br>
        /// Se il valore della chiave esiste, lo sostituisce.<br></br>
        /// Invocare esplicitamente il metodo dall'Interfaccia se la sostituzione NON deve essere consentita.<br></br>
        /// Invoca Append(). A differenza di Append() evoca un'eccezione in caso di chiave nulla.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2018 Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Key">Stringa chiave (non nullabile)</param>
        /// <param name="p_Value">Stringa Valore (nullabile)</param>
        /// <exception cref="ArgumentNullException">In caso di chiave nulla.</exception>
        public void add(String p_Key, String p_Value)
        {
            if (p_Key == null)
                throw new ArgumentNullException("p_Key");
            append(p_Key, p_Value, null);
        }

        /// <summary>
        /// Verifica l'esistenza di una chiave nella lista e ne restituisce la posizione come numero intero positivo.<p></p>
        /// Il valore restituito è un numero intero negativo (-1) se la chiave specificata NON è nella lista o se la lista è
        /// vuota.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Chiave da cercare.</param>
        /// <returns>La posizione (0 BASE) della chiave o -1 in caso la chiave non sia presente o la lista sia vuota.</returns>
        public int indexOf(String p_ID)
        {
            int _idx=0;
            if (_first == null)
                return -1;
            SimplexFormEntry _current = _first;
            while (_current != null)
            {
                if (_current.ID.CompareTo(p_ID) == 0)
                    return _idx;

                _current = _current.next;
                _idx++;
            }
            return -1;
        }

        /// <summary>
        /// Verifica l'esistenza di una chiave nella lista.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Chiave da cercare.</param>
        /// <returns>TRUE se la chiave è presente, FALSE altrimenti.</returns>
        public Boolean contains(String p_ID)
        {
            return !(indexOf(p_ID) == -1);
        }

        /// <summary>
        /// Aggiunge un elemento in coda al Dictionary se non è presente un altro elemento
        /// con il medesimo valore della chiave ID, altrimenti esegue una sostituzione
        /// nella posizione di quest'ultimo.<p></p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:14/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Chiave (obbl.)</param>
        /// <param name="p_Text">Valore.</param>
        /// <param name="p_Type">Tipo.</param>
        public void append(String p_ID, String p_Text, String p_Type)
        {
            SimplexFormEntry _new = null;   //nuovo
            SimplexFormEntry _cnt = null;   //current
            SimplexFormEntry _prv = null;   //previous

            if (p_ID == null)
                return;

            _new = new SimplexFormEntry(p_ID, p_Text, p_Type);

            // Caso di elemento vuoto
            if (_first == null)
            {
                _first = _new;
                _last = _new;
                return;
            }

            _cnt = _first;
            while (_cnt != null)
            {
                if (_cnt.ID.CompareTo(_new.ID) == 0)
                {
                    // sostituzione
                    _new.prev = _cnt.prev;
                    _new.next = _cnt.next;

                    if (_cnt.prev != null)
                        (_cnt.prev).next = _new;
                    if (_cnt.next != null)
                        (_cnt.next).prev = _new;

                    if (_first == _cnt)
                        _first = _new;
                    if (_last == _cnt)
                        _last = _new;

                    _cnt = _new;

                    return;
                }
                _cnt = _cnt.next;
            }

            // l'elemento non esiste: accodamento
            _last.next = _new;
            _new.prev = _last;
            _last = _new;
        } //fine

        /// <summary>
        /// Rimuove con scorrimento a sinistra dall'oggetto IDictionary l'elemento specificato.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:27/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Elemento da rimuovere</param>
        public void remove(SimplexFormEntry p_Entry)
        {
            if(contains(p_Entry.ID)==true)
            {
                remove(p_Entry.ID);
            }
        }

        /// <summary>
        /// Rimuove con scorrimento a sinistra dall'oggetto IDictionary l'elemento con la chiave specificata.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:14/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Chiave dell'elemento da rimuovere</param>
        public void remove(String p_ID)
        {
            SimplexFormEntry _cnt = null;   //current
            SimplexFormEntry _prv = null;   //previous

            if (p_ID == null)
                return;

            if (contains(p_ID) == false)
                return;

            _cnt = _first;
            while (_cnt != null)
            {
                if (_cnt.ID.CompareTo(p_ID) == 0)
                {
                    // 1 eliminazione
                    if (_cnt.prev == null)
                    {
                        // 1.1 eliminazione in testa
                        if (_first == _last)
                        {   // 1.1.1: il dictionary ha un solo elemento: si svuota
                            _first = _last = null;
                            return;
                        }
                        _first = _cnt.next; ;
                        _cnt.next.prev = null;
                        return;
                    }

                    // 1.2 Eliminazione in coda
                    if (_last == _cnt)
                    {
                        _cnt.prev.next = null;
                        return;
                    }

                    // 1.3 Eliminazione
                    _cnt.prev.next = _cnt.next;
                    _cnt.next.prev = _cnt.prev;
                    return;
                }
                _cnt = _cnt.next;
            }
        } //fine

        /// <summary>
        /// Rimuove con scorrimento a sinistra dall'oggetto IDictionary l'elemento in posizione specificata.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:14/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_idx">Posizione dell'elemento da rimuovere.</param>
        /// <exception cref="IndexOutOfRangeException">Nel caso si specifichi una posizione eccedente la lunghezza del Dictionary.</exception>
        public void remove(int p_idx)
        {
            SimplexFormEntry _en = getElementAt(p_idx);
            remove(_en.ID);
        }

        /// <summary>
        /// Aggiunge in testa al dictionary e rimuove l'eventuale elemento già presente, ovunque esso sia.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Chiave (obbl.)</param>
        /// <param name="p_Text">Valore.</param>
        /// <param name="p_Type">Tipo.</param>
        public void addOnTop(String p_ID, String p_Text, String p_Type)
        {
            // 0 controllo
            if (p_ID == null)
                return;

            // 1 eventuale rimozione dell'elemento pre-esistente
            if (contains(p_ID) == true)
                remove(p_ID);

            // 2 inserimento in testa
            SimplexFormEntry _new = new SimplexFormEntry(p_ID, p_Text, p_Type);
            if (_first == _last)
                _last = _new;
            _new.next = _first;
            _first.prev = _new;
            _first = _new;
        } // fine

        /// <summary>
        /// Ottiene una Entry data per chiave.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018 - Monterotondo
        /// Mdfd: 25/05/2018 - v. 1.2.0.3 Correzione
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Chiave (Obbl)</param>
        /// <returns>La entry richiesta, se esiste, ovvero null.</returns>
        public SimplexFormEntry getElement(String p_ID)
        {
            if (p_ID == null)
                return null;

            SimplexFormEntry _cnt = null;
            _cnt = _first;
            while (_cnt != null)
            {
                if (_cnt.ID.CompareTo(p_ID) == 0)
                    return _cnt;
                _cnt = _cnt.next;
            }
            return null;
        } // fine

        /// <summary>
        /// Restituisce un oggetto di classe SimplexFormEnumerator
        /// Implementazione dell'interfaccia IEnumerable.<p></p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <returns>Un enumeratore di classe SimplexFormEnumerator.</returns>
        public SimplexFormEnumerator GetEnumerator()
        {
            SimplexFormEnumerator _en = new SimplexFormEnumerator(_first);
            return _en;
        }

        /// <summary>
        /// Restituisce un oggetto conforme a IEnumerator.
        /// Implementazione esplicita del membro GetEnumerator() dell'interfaccia IEnumerable.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <returns>Un enumeratore di classe SimplexFormEnumerator.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (System.Collections.IEnumerator)GetEnumerator();
        }

        /// <summary>
        /// Inserisce con scorrimento a destra una nuova voce nella posizione specificata da un indice.<b></b>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/01/2018 - Monterotondo
        /// </pre>
        /// <remarks>
        /// La voce <b>NON DEVE</b> essere già presente nel Dictionary.
        /// </remarks>
        /// </summary>
        /// <param name="p_ID">Chiave (obbl.)</param>
        /// <param name="p_Text">Valore.</param>
        /// <param name="p_Type">Tipo.</param>
        /// <param name="p_idx">Posizione di inserimento.</param>
        /// <exception cref="InvalidOperationException">In caso di inserimento di chiave già esistente.</exception>
        public void insertAt(String p_ID, String p_Text, String p_Type, int p_idx)
        {
            if(p_ID==null)
                return;
            
            if (p_idx < 0)
                return;

            SimplexFormEntry _en = new SimplexFormEntry(p_ID, p_Text, p_Type);
            insertAt(_en, p_idx);            
        }

        #endregion

        #region METODI PROTETTI

        /// <summary>
        /// Conta il numero di elementi nella collection.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <returns>Numero di elementi nella collection</returns>
        protected int count()
        {
            SimplexFormEntry _current = null;
            _current = _first;
            int _toRet = 0;
            while (_current != null)
            {
                _current = _current.next;
                _toRet++;
            }
            return _toRet;
        }

        /// <summary>
        /// Restituisce l'oggetto di classe SimplexFormEntry situato nella posizione specificata (0 BASED). <p></p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/01/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_idx">Indice posizionale dell'elemento da prelevare.</param>
        /// <returns>Un oggetto di classe SimplexFormEntry</returns>
        /// <exception cref="IndexOutOfRangeException">Nel caso si specifichi una posizione eccedente la lunghezza del Dictionary.</exception>
        protected SimplexFormEntry getElementAt(int p_idx)
        {
            if (p_idx < 0)
                return null;

            SimplexFormEntry _cnt = null;
            _cnt = _first;
            int _idx = 0;

            while (_cnt != null && _idx<= p_idx)
            {
                if (_idx == p_idx)
                    return _cnt;
                _cnt = _cnt.next;
                _idx++;
            }
            throw new IndexOutOfRangeException("in SimplexFormDictionary.getElementAt(): indice troppo grande.");
        }

        /// <summary>
        /// Sostituisce l'elemento passato come parametro nel dictionary a quello presente nella posizione specificata.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 27/02/2018 Minterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Chiave (se nullo, non esegue).</param>
        /// <param name="p_Text">Valore (può essere nullo).</param>
        /// <param name="p_Type">Tipo (può essere nullo).</param>
        /// <param name="p_idx">Posizione di sostituzione.</param>
        /// <exception cref="IndexOutOfRangeException">Sforamento della dimensione del dictionary.</exception>
        public void putElementAt(String p_ID, String p_Text, String p_Type, int p_idx)
        {
            if(p_ID==null)
                return;

            SimplexFormEntry _e = new SimplexFormEntry(p_ID, p_Text, p_Type);
            putElementAt(_e, p_idx);
        }

        /// <summary>
        /// Sostituisce l'elemento passato come parametro nel dictionary a quello presente nella posizione specificata.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 26/02/2018 Minterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_sfe">Elemento che sostituisce (Se nullo o con chiave nulla, non esegue alcuna sostituzione).</param>
        /// <param name="p_idx">Posizione in cui sostituire (In base 0. Se negativo non esegue alcuna sostituzione).</param>
        /// <exception cref="IndexOutOfRangeException">Sforamento della dimensione del dictionary.</exception>
        protected void putElementAt(SimplexFormEntry p_sfe, int p_idx)
        {
            if (p_idx < 0)
                return;
            if (p_sfe == null)
                return;
            if (p_sfe.ID == null)
                return;

            SimplexFormEntry _cnt = null;
            _cnt = _first;
            int _idx = 0;

            while (_cnt != null && _idx<= p_idx)
            {
                if (_idx == p_idx)
                {
                    // sostituire _cnt con p_sfe                   
                    if(_cnt.prev!=null)
                    {
                        (_cnt.prev).next = p_sfe;
                        p_sfe.prev = _cnt.prev;
                    }

                    if(_cnt.next!=null)
                    {
                        (_cnt.next).prev = p_sfe;
                        p_sfe.next = _cnt.next;
                    }

                    if(_first==_cnt)
                        _first = p_sfe;

                    if(_last == _cnt)
                        _last = p_sfe;

                    return;
                }
                _cnt = _cnt.next;
                _idx++;
            }
            throw new IndexOutOfRangeException("in SimplexFormDictionary.getElementAt(): indice troppo grande.");
        }

        /// <summary>
        /// Inserisce con scorrimento a destra una Entry nella posizione specificata da un indice.<b></b>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/01/2018 - Monterotondo
        /// </pre>
        /// <remarks>
        /// La Entry <b>NON DEVE</b> avere un valore di chiave già presente nel Dictionary.
        /// </remarks>
        /// </summary>
        /// <param name="p_en">Entry</param>
        /// <param name="p_idx">Posizione di inserimento.</param>
        /// <exception cref="InvalidOperationException">In caso di inserimento di chiave già esistente.</exception>
        protected void insertAt(SimplexFormEntry p_en, int p_idx)
        {

            // * controlli
            if (p_idx < 0)
                return;

            if(p_en==null)
                return;

            // ** controlli
            if (contains(p_en.ID) == true)
                throw new InvalidOperationException("Chiave " + p_en.ID + " già presente. Rimuoverla prima di inserire");


            SimplexFormEntry _en = _first;
            int _idx=0;
            
            while (_en != null && _idx<=p_idx)
            {
                if (_idx < p_idx)
                {
                    _en = _en.next;
                    _idx++;
                    continue;
                }

                if (_idx == p_idx)
                {
                    // inserimento in testa
                    if (_en == _first)
                    {
                        p_en.next = _first;
                        _first.prev = p_en;
                        p_en.prev = null;
                        if (_first == _last)
                            _last = p_en;
                        _first = p_en;
                        return;
                    }

                    // inserimento con scorrimento a destra
                    // Link con il precedente
                    _en.prev.next = p_en;
                    p_en.prev = _en.prev;

                    // Link con il successivo
                    p_en.next = _en;
                    _en.prev = p_en;

                    return;
                }
            }
            throw new IndexOutOfRangeException("insertAt(): Impossibile inserire nella posizione specificata");
        }// fine

        #endregion

    }
}
