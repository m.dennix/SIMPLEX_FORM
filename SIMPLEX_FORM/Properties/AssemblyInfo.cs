﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// Le informazioni generali relative a un assembly sono controllate dal seguente 
// insieme di attributi. Per modificare le informazioni associate a un assembly
// occorre quindi modificare i valori di questi attributi.
[assembly: AssemblyTitle("SIMPLEX_FORM")]
[assembly: AssemblyDescription("v 1.1.0.0 - Più controlli in un stessa cella - v1.1.0.2 - aggiunto prefisso - v.1.2.0.0: nuovi controlli dinamici: v.1.2.0.2 introdotto SimplexFormDictionary + MaskState")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Michele de Nittis (@MdN)")]
[assembly: AssemblyProduct("SIMPLEX_FORM")]
[assembly: AssemblyCopyright("Copyright ©  2014 - 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Se si imposta ComVisible su false, i tipi in questo assembly non saranno visibili 
// ai componenti COM. Se è necessario accedere a un tipo in questo assembly da 
// COM, impostare su true l'attributo ComVisible per tale tipo.
[assembly: ComVisible(false)]

// Se il progetto viene esposto a COM, il GUID che segue verrà utilizzato per creare l'ID della libreria dei tipi
[assembly: Guid("19c9afc4-f645-412d-ac7a-f750f54cc214")]

// Le informazioni sulla versione di un assembly sono costituite dai seguenti quattro valori:
//
//      Numero di versione principale
//      Numero di versione secondario 
//      Numero build
//      Revisione
//
// È possibile specificare tutti i valori oppure impostare i valori predefiniti per i numeri relativi alla build e alla revisione 
// utilizzando l'asterisco (*) come descritto di seguito:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.0.3")]
[assembly: AssemblyFileVersion("1.2.0.3")]
[assembly: NeutralResourcesLanguageAttribute("it-IT")]
