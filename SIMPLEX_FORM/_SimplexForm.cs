﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using simplex_ORM;
using System.Configuration;
using SIMPLEX_Config;


namespace simplex_FORM
{
    /// <summary>
    /// <p>
    /// Delegato per il validatore generico semplice.
    /// </p>
    /// <p>
    /// Un validatore semplice agisce sul testo (propr. Text) del controllo passato per argomento
    /// e ne verifica alcune proprietà quali, a titolo indicativo,
    /// nullità, lunghezza, tipo, etc.
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 10/05/2015
    /// </pre>
    /// </summary>
    /// <param name="p_wc">Controllo web oggetto di validazione.</param>
    /// <returns><b>True</b> se la validazione ha esito positivo, <b>False</b> altrimenti.</returns>
    public delegate Boolean generalSimplexValidateHandler(System.Web.UI.WebControls.WebControl p_wc);

    /// <summary>
    /// <p>
    /// IT: E' una specializzazione dell'oggetto Page adatta
    /// ad operare con gli oggetti del framework simplex_ORM.
    /// <br>
    /// La classe comunica situazioni di anomalia o di errore mediante il 
    /// ricorso a messaggi di classe simplex_ORM.Spx_ORMMEssage.
    /// <see cref="simplex_ORM.Spx_ORMMessage"/>
    /// </br>
    /// </p>
    /// <p>
    /// EN: Page specialization for simplex_ORM.
    /// Form type: 0;
    /// </p>
    /// <pre>
    /// ----------------------------------------------
    /// Throws Spx_ORMMEssage Exceptions:
    /// 1000:       The form does not have a table name;
    /// 1001:       Wrong referenced table;
    /// 1002:       No controls found in the form;
    /// 1003:       No referenced table;
    /// 1004:       No control type or ID specified;
    /// 1005:       One of the objects used by the form has not been secified.
    /// 1006:       The form does not have a valid default connection.
    /// 1007:       not valid XML dynamic element.
    /// 1008:       not valid value for an XML element'argument.
    /// 1009:       Problems with connection strings.
    /// 1010:       Validation error: not a valid datetime value;
    /// 1011:       Validation error: not a valid integral value;
    /// 1012:       Validation error: not a valid decimal value;
    /// 1013:       Validation error: not a valid numeric value;
    /// 1014:       Validation error: a value must be specified;
    /// ----------------------------------------------
    /// @MdN
    /// Crtd: 23/10/2014
    /// Tstd:
    /// Mdfd: 01/05/2015 
    /// Mdfd: 19/09/2015 - attributo Continue.
    /// Mdfd: 28/09/2015 - attributo MaxLength in txt.
    /// Mdfd: 01/10/2015 - corretti BUGS vari.
    /// Mdfd: 23/10/2015 - corretto BUG --> Version 1.1.0.1
    /// Mdfd: 05/06/2017 - Modificato copyValues() in modo che non vengano annullati i "valori" delle colonne di un oggetto ORM in caso di DDL disabilitate. --> v.1.1.0.3
    /// Mdfd: 17/08/2017 -> 02/09/2017 - Version 1.2.0.0: nuovi controlli dinamici.
    ///                     22/08/2017 introdotta solveComplexControls()
    ///                     09/09/2017 introdotta innerDesignDynamicForm(); introdotto altro overload di designDynamicForm()
    ///                     17/09/2017 introdotto altro overload di designDynamicForm(). NON TESTATO l'overload: designDynamicForm(String p_XML, System.Web.UI.WebControls.PlaceHolder p_PH, int p_NumCols, Boolean p_withSections)
    /// Mdfd: 11/10/2017    Version 1.2.0.1: 
    ///                     11/10/2017 Modificato copyValues() per correggere un BUG introdotto con la modifica del 05/06/2017.
    /// Mdfd: 18/03/2018    Version 1.2.0.2:
    ///                     13/01/2018 - 27/02/2018: introdotta la classe SimplexFormDictionary e le classi ad essa collegate.
    ///                     18/03/2018: introdotti MyFormState, saveFormState(), clearFormState(), loadFormState().
    ///                     18/03/2018: nuovi overloads per copyValues(), show()
    /// Mdfd: 25/05/2018    Version 1.2.0.3:
    ///                     Modifiche a SimplexFormDictionary
    ///                     
    /// </pre>
    /// </summary>
    public class SimplexForm : System.Web.UI.Page
    {

        /// <summary>
        /// <p>
        /// Sottoclasse protetta di supporto alla lista di delegati di validazione
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/05/2015
        /// </pre> 
        /// </summary>
        public class SimplexValidateEntry
        {
            internal generalSimplexValidateHandler MyHandler = null;
            internal System.Web.UI.WebControls.WebControl MyWeb = null;
            internal SimplexValidateEntry MyNext = null;
            internal SimplexValidateEntry MyHead = null;

            /// <summary>
            /// <p>
            /// Restituisce il numero degli elementi presenti in lista.
            /// </p>
            /// <pre>
            /// ----
            /// @MdN
            /// Crtd: 10/05/2015
            /// </pre>
            /// </summary>
            public int Count
            {
                get
                {
                    int _toRet = 0;
                    SimplexValidateEntry _cur;
                    _cur = MyHead;
                    while (_cur != null)
                    {
                        _cur = _cur.MyNext;
                        _toRet++;
                    }
                    return _toRet;
                }
            }
        }

        /// <summary>
        /// [IT] Oggetto ORM referenziato dalla Maschera. <br></br>
        /// [EN] <i>ORM Object referenced by the form.</i><br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/10/2014
        /// </pre>
        /// </summary>
        protected simplex_ORM.SQLSrv.SQLTable TBL_UNDER_FORM=null;

        /// <summary>
        /// [IT] Nome della tabella referenziata<br></br>
        /// [EN] <i>Name of the referenced table</i>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/10/2014
        /// </pre>
        /// </summary>
        protected String TableName = null;
        /// <summary>        
        /// Lista di delegati (handler) di validazione di tipo 
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ?
        /// </pre>
        /// generalSimplexValidateHandler <see cref="generalSimplexValidateHandler"/>.
        /// </summary>
        protected SimplexValidateEntry MyValidateHandler = null;

        /// <summary>
        /// [IT]:<br></br>
        /// Lista degli errori.<br></br>
        /// RICORDA: LA LISTA DEVE ESSERE CANCELLATA DOPO OGNI OPERAZIONE UTENTE.<br></br>
        /// NON DIMENTICARSI DI CANCELLARE LA LISTA DOPO AVERLA USATA.<br></br><br></br>
        /// SI SUGGERISCE, AD ESEMPIO, DI USARE LA LISTA NEL METODO <b>show()</b> originario/virtual o sovrascritto/ovverride.<br></br>
        /// NOTA: <br></br>
        /// SE SI VOGLIONO MOSTRARE GLI ERRORI PRODOTTI IN UNA OPERAZIONE UTENTE, SI DEVE EFFETTUARE L'OVERRIDE DEL METODO <i>show()</i>,
        /// MOSTRARE GLI ERRORI E INFINE INVOCARE IL METODO <i>base.show()</i>.
        /// <br></br>
        /// <br></br>
        /// <br></br>
        /// [EN]:
        /// List of errors.<br></br>
        /// REMEMBER: THE LIST MUST BE CLEANED AFTER EVERY TRANSACTION.<br></br>
        /// DON'T FORGET TO CLEAR THE LIST AFTER YOU USE IT.<br></br><br></br>
        /// I.E. I SUGGEST TO USE THE LIST IN THE show() METHOD (NATIVE OR OVERRIDED).<br></br>
        /// REMARK:<br></br>
        /// IF YOU WANT TO SHOW THE ERRORS, YOU HAVE TO OVERRIDE THE show() METHOD, SHOW THE ERRORS,
        /// CLEAR THE LIST AND FINALLY INVOKE THE base.show() METHOD.<br></br>
        /// <pre>
        /// -----
        /// @MdN
        /// Crtd: 07/12/2014
        /// </pre>
        /// </summary>
        protected System.Collections.Generic.List<Spx_ORMMessage> MyErrors;

        /// <summary>
        /// [IT]: Vettore di stringhe che rappresentano i nomi degli elementi del file XML dei controlli
        /// dinamici.<br></br>
        /// [EN]: String array that contains XML elements' names corresponding to web controls in the XML dynamic controls file.
        /// </summary>
        
        protected static String[] Elements = { "main", "tab", "ddl", "sqlddl", "txt", "lbl", "chk", "calendar", "lnk", "btn", "hddn"};
        /// <summary>
        /// [IT]: Vettore di stringhe che rappresentano i nomi dei possibili attributi degli elementi del file XML dei controlli
        /// dinamici.<br></br>
        /// [EN]: String Array containing attributes' names of XML elements expected in the XML dynamic controls file.<br></br>
        /// </summary>
        protected static String[] Attributes = {    "ID"
                                                   , "Name"
                                                   , "Visible"
                                                   , "CssClass"
                                                   , "LableText"
                                                   , "Readonly"
                                                   , "Width"
                                                   , "DataValueField"
                                                   , "DataTextField"
                                                   , "Separator"
                                                   , "ConnectionString"
                                                   , "Onselectedindexchanged"
                                                   , "SelectedIndex"
                                                   , "TextMode"
                                                   , "Height"
                                                   , "Enabled"
                                                   , "Checked"
                                                   , "NumCols"
                                                   , "rows"
                                                   , "NullItem"
                                                   , "Continue"     // @MdN 19/09/2015
                                                   , "MaxLength"    // @MdN 28/09/2015
                                                   , "Calendar"     // @MdN 22/08/2017
                                                   , "OnClick"      // @MdN 22/08/2017
                                                   , "OnSelectionChanged"       // @MdN 22/08/2017
                                               };
        /// <summary>
        /// Simbolo di espansione da usare nei LinkButton quando inseriti nel controllo composto Txt 
        /// </summary>
        protected String MyLnkExpand = "(+)";
        protected String MyLnkCollapse = "(-)";

        /// <summary>
        /// [IT]: Collezione dei valori dei controlli presenti nella maschera;
        /// [EN]: Collection of controls' values.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/03/2018 (v 1.2.0.2)
        /// </pre>
        /// </summary>
        protected SimplexFormDictionary MyFormState = null;

        #region STRINGHE/STRINGS
#if EN
        String _notSpecifiedParameter  = "Parameter %param% has not been spcified.";
        String _noControlsFound = "SimplexForm.show(): Non controls found in the '%param%' container.";
#else
        String _notSpecifiedParameter = "Il parametro %param% non è stato specificato.";
        String _noControlsFound = "SimplexForm.show(): Nel container '%param%' non sono stati trovati controlli.";

#endif

        #endregion

        #region CONSTRUCTORS

        public SimplexForm():base()
        {
            MyErrors = new List<Spx_ORMMessage>();
        }

        /// <summary>
        /// @MdN
        /// Set the referenced table name. The empty simplex_ORM.
        /// Table corresponding object is not instantiated.
        /// Crtd: 23/10/2014
        /// </summary>
        /// <param name="p_TabName">Name of the referenced table.</param>
        public SimplexForm(String p_TabName):base()
        {
            TableName = p_TabName;
            TBL_UNDER_FORM = null;
        }

        /// <summary>
        /// @MdN
        /// Constructor with the object representing the referenced table.
        /// Crtd: 23/10/2014
        /// </summary>
        /// <param name="p_SqlTable"></param>
        public SimplexForm(simplex_ORM.SQLSrv.SQLTable p_SqlTable):base()
        {
            if (p_SqlTable == null)
                return;
            TableName = p_SqlTable.Name;
            TBL_UNDER_FORM = p_SqlTable;
        }
        #endregion

        #region VALIDATORI

        /// <summary>
        /// <p>
        /// Verifica se una TextBox è stata valorizzata o se è vuota.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/05/2015
        /// </pre></summary>
        /// <param name="p_wc">Controllo TextBox</param>
        /// <returns><b>True</b> se il controllo è valorizzato, <b>False</b> altrimenti.</returns>
        /// <remarks>Se il test fallisce, aggiunge un messaggio di dettaglio nella collezione di messaggi 'MyErrors'.</remarks>
        public Boolean isNotEmpty(System.Web.UI.WebControls.TextBox p_wc)
        {
            Boolean _toRet=true;
            if (p_wc == null)
                _toRet = false;

            if (p_wc.Text == null)
                _toRet = false;;

            if (p_wc.Text.Length == 0)
                _toRet = false;

            if(_toRet==false)
                MyErrors.Add(new Spx_ORMMessage(1014,p_wc.ID, "Il controllo " + p_wc.ID + " non contiene alcun valore"));

            return _toRet;
        }

        /// <summary>
        /// <p>
        /// Verifica se una TextBox è stata valorizzata con del testo che
        /// rappresenta un numero.
        /// <br>
        /// Invoca il metodo statico simplex_ORM.Column.isNumber() - vedi <see cref="simplex_ORM.Column.isNumber"/> - .
        /// </br>
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/05/2015
        /// </pre></summary>
        /// <param name="p_wc">Controllo TextBox</param>
        /// <returns><b>True</b> se il controllo è un numero, <b>False</b> altrimenti.</returns>
        /// <remarks>Se il test fallisce, aggiunge un messaggio di dettaglio nella collezione di messaggi 'MyErrors'.</remarks>
        public Boolean isGenericNumber(System.Web.UI.WebControls.TextBox p_wc)
        {
            //Controllo
            if (isNotEmpty(p_wc) == false)
                return false;

            Boolean _test=simplex_ORM.Column.isNumber(p_wc.Text);
            if(_test==false)
                MyErrors.Add(new Spx_ORMMessage(1013,p_wc.ID, "Il controllo " + p_wc.ID + " non contiene un valore numerico"));

            return _test;            
        }

        /// <summary>
        /// <p>
        /// Verifica se una TextBox è stata valorizzata con del testo che
        /// rappresenta un numero decimale (il formato è quello italiano, con
        /// la virgola come separatore decimale).
        /// <br>
        /// Invoca il metodo statico simplex_ORM.Column.isDecimal() - vedi <see cref="simplex_ORM.Column.isDecimal"/> - .
        /// </br>
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/05/2015
        /// </pre></summary>
        /// <param name="p_wc">Controllo TextBox</param>
        /// <returns><b>True</b> se il controllo è un numero decimale, <b>False</b> altrimenti.</returns>
        /// <remarks>Se il test fallisce, aggiunge un messaggio di dettaglio nella collezione di messaggi 'MyErrors'.</remarks>
        public Boolean isDecimalNumber(System.Web.UI.WebControls.TextBox p_wc)
        {
            //Controllo
            if (isNotEmpty(p_wc) == false)
                return false;

            Boolean _test=simplex_ORM.Column.isDecimal(p_wc.Text);
            if(_test==false)
                MyErrors.Add(new Spx_ORMMessage(1012,p_wc.ID, "Il controllo " + p_wc.ID + " non contiene un valore decimale"));

            return _test;
        }

        /// <summary>
        /// <p>
        /// Verifica se una TextBox è stata valorizzata con del testo che
        /// rappresenta un numero intero.
        /// <br>
        /// Invoca il metodo statico simplex_ORM.Column.isPureInteger() - vedi <see cref="simplex_ORM.Column.isPureInteger()"/> - .
        /// </br>
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/05/2015
        /// </pre></summary>
        /// <param name="p_wc">Controllo TextBox</param>
        /// <returns><b>True</b> se il controllo è un numero intero, <b>False</b> altrimenti.</returns>
        /// <remarks>Se il test fallisce, aggiunge un messaggio di dettaglio nella collezione di messaggi 'MyErrors'.</remarks>
        public Boolean isIntegerNumber(System.Web.UI.WebControls.TextBox p_wc)
        {
            
            //Controllo
            if (isNotEmpty(p_wc) == false)
                return false;
            
            Boolean _test=simplex_ORM.Column.isPureInteger(p_wc.Text);
            if(_test==false)
                MyErrors.Add(new Spx_ORMMessage(1011,p_wc.ID, "Il controllo " + p_wc.ID + " non contiene un valore intero"));

            return _test;
        }

        /// <summary>
        /// <p>
        /// Verifica se una TextBox è stata valorizzata con del testo che
        /// rappresenta una data in formato DD/MM/YYYY.
        /// <br>
        /// Invoca il metodo statico simplex_ORM.Column.isDateTime() - vedi <see cref="simplex_ORM.Column.isDateTime()"/> - .
        /// </br>
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/05/2015
        /// </pre></summary>
        /// <param name="p_wc">Controllo TextBox</param>
        /// <returns><b>True</b> se il controllo è un numero intero, <b>False</b> altrimenti.</returns>
        /// <remarks>Se il test fallisce, aggiunge un messaggio di dettaglio nella collezione di messaggi 'MyErrors'.</remarks>
        public Boolean isDateTime(System.Web.UI.WebControls.TextBox p_wc)
        {
            //Controllo
            if (isNotEmpty(p_wc) == false)
                return false;

            Boolean _test = simplex_ORM.Column.isDateTime(p_wc.Text,"DD/MM/YYYY");
            if(_test==false)
                MyErrors.Add(new Spx_ORMMessage(1010,p_wc.ID, "Il controllo " + p_wc.ID + " non contiene una data valida"));
            return _test;         
        }


        /// <summary>
        /// <p>
        /// Aggiunge un delegato di validazione alla lista di delegati da lanciare
        /// per validare la maschera. I controlli della maschera verranno validati
        /// nell'ordine di inserimento nella lista.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_H">Delegato (Handler) di validazione</param>
        /// <returns>Numero di delegati presenti.</returns>
        public virtual int addValidator(generalSimplexValidateHandler p_H, System.Web.UI.WebControls.WebControl p_C)
        {
            SimplexValidateEntry _cur;

            //Controlli
            if (MyValidateHandler == null)
                MyValidateHandler = new SimplexValidateEntry();
            if (p_C == null)
                return MyValidateHandler.Count;                        
            if (p_H == null)
                return MyValidateHandler.Count;

            _cur = MyValidateHandler;
            _cur.MyHandler = p_H;
            _cur.MyWeb = p_C;
            //Accodamento
            while (_cur.MyNext!=null)
                _cur = _cur.MyNext;
            _cur.MyNext = new SimplexValidateEntry();
            _cur.MyNext.MyHead = _cur.MyHead;
            _cur.MyNext.MyHandler = p_H;
            _cur.MyNext.MyWeb = p_C;
            return MyValidateHandler.Count;
        }

        /// <summary>
        /// <p>
        /// Esegue la validazione della maschera lanciando in sequenza tutti i 
        /// validatori inseriti nella lista dei '<i>validatori generici semplici</i>' (cioè quelli
        /// che prevedono un solo parametro) <i>MyValidateHandler</i> <see cref="MyValidateHandler"/>.
        /// </p>
        /// <p>
        /// In caso di validazione non riuscita il metodo, oltre a restiuire <b>False</b>,
        /// inserisce appositi messaggi di dettaglio nella lista dei messaggi (MyErrors).
        /// </p>
        /// </summary>
        /// <returns><b>True</b> in caso di validazione positva, <b>False</b> altrimenti.</returns>
        public virtual Boolean validateForm()
        {
            Boolean _toRet = true;
            Boolean _test = false;
            if (MyValidateHandler == null)
                return true;
            // Ciclo di validazione
            SimplexValidateEntry _cur = MyValidateHandler;
            while (_cur != null)
            {
                _test = _cur.MyHandler(_cur.MyWeb);
                _toRet = _toRet & _test;
                _cur = _cur.MyNext;
            }

            if (_toRet == false)
                MyErrors.Add(new Spx_ORMMessage(1015, "SimplexForm.validateForm():", "Errori di validazione. Vedi messaggi di dettaglio"));
            
            return _toRet;
        }

        #endregion

        #region GESTORI DI DEFAULT

        /// <summary>
        /// Gestore generico di evento di pressione di Button o di LinkButton.<br></br>
        /// Inserisce in <i>MyErrors</i> un messaggio ed esce.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/09/2017
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void generic_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.WebControl _tgt = null; 
            if (sender != null)
            {
                simplex_ORM.Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _tgt = (WebControl)sender;
                _ormm.Message = "Il controllo " + _tgt.ID + " ha gestito un evento click()";
                MyErrors.Add(_ormm);                
            }
        }

        /// <summary>
        /// Inserisce nel controllo TextBox associato al controllo Calendar (sender) la data selezionata.<br></br>
        /// <remarks>
        /// Il controllo LinkButton ha lo stesso nome del relativo controllo TextBox più "_LNK". <br></br>
        /// Il controlo Calendar ha lo stesso nome del relativo controllo TextBox più "_Calendar".<br></br>
        /// Dunque è facile trovare il nome dei controlli associati conoscendo il nome del controllo corrente.<br></br>
        /// </remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 11/08/2017 Livorno - 22/08/2017 Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TextBoxCalendar_DefaultSelectionChanged(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Calendar _calendar = (System.Web.UI.WebControls.Calendar)sender;
            System.Web.UI.WebControls.PlaceHolder _container = null;
            System.Web.UI.WebControls.TextBox _txt = null;

            String _RootName = null;
            String _id = null;
            String _textName = null;
            Int32 _idx = 0;
            DateTime _dt;

            // si cerca il nome RADICE del controllo, che è quello che precede la componente _Container
            _id = _calendar.ID;
            if (_id != null && _id.Contains("_Calendar"))
            {
                _idx = _id.LastIndexOf("_Calendar");
                _RootName = _id.Substring(0, _idx);
            }
            else
                return;

            if (_RootName == null)
                return;

            // Individuazione del controllo TextBox
            _textName = _RootName;
            _txt = (System.Web.UI.WebControls.TextBox)_calendar.Parent.FindControl(_textName);

            // Impostazione della data
            if (_txt != null)
            {
                _dt = ((System.Web.UI.WebControls.Calendar)sender).SelectedDate;
                _txt.Text = _dt.ToString("dd/MM/yyyy");
            }
        }

        /// <summary>
        /// Espande e colassa il calendario associato.
        /// <remarks>
        /// Il controllo LinkButton ha lo stesso nome del relativo controllo TextBox più "_LNK". <br></br>
        /// Il controlo Calendar ha lo stesso nome del relativo controllo TextBox più "_Calendar".<br></br>
        /// Dunque è facile trovare il nome dei controlli associati conoscendo il nome del controllo corrente.<br></br>
        /// </remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 11/08/2017 Livorno
        /// Mdfd: 16/08/2017 Ledro
        /// Mdfd: 22/08/2017 Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TextBoxCalendar_DefaultClick(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.LinkButton _lb = (System.Web.UI.WebControls.LinkButton)sender;
            System.Web.UI.WebControls.PlaceHolder _container = null;
            System.Web.UI.WebControls.Calendar _calendar = null;
            System.Web.UI.WebControls.TextBox _txt = null;
            String _RootName = null;
            String _id = null;
            String _calendarName = null;
            String _txtText = null;
            Int32 _idx = 0;
            DateTime _dt = DateTime.Now;

            //comprime ovvero fa sparire il calendario
            // si cerca il nome RADICE del controllo, che è quello che precede la componente _LNK
            _id = _lb.ID;
            if (_id != null && _id.Contains("_LNK"))
            {
                _idx = _id.LastIndexOf("_LNK");
                _RootName = _id.Substring(0, _idx);
            }
            else
                return;

            if (_RootName == null)
                return;

            // si prende il valore del controllo radice
            _txt = (System.Web.UI.WebControls.TextBox)_lb.Parent.FindControl(_RootName);
            if (_txt != null && _txt.Text != null && _txt.Text.Length > 0)
            {
                System.Globalization.DateTimeFormatInfo _dtInfo = new System.Globalization.DateTimeFormatInfo();
                _dtInfo.ShortDatePattern = "dd/MM/yyyy";
                _txtText = _txt.Text;
                try
                {
                    _dt = DateTime.Parse(_txtText, _dtInfo);
                }
                catch (Exception E)
                {
                    _dt = DateTime.Now;
                }
            }

            // Individuazione del controllo Calendario
            _calendarName = _RootName + "_Calendar";
            _calendar = (System.Web.UI.WebControls.Calendar)_lb.Parent.FindControl(_calendarName);

            // Espansione o compressione del calendario e cambio del testo del link button
            if (_lb.Text.CompareTo(MyLnkExpand) == 0)
            {
                _calendar.Visible = true;
                _lb.Text = MyLnkCollapse;
                _calendar.SelectedDate = _dt;
                _calendar.VisibleDate = _dt;
            }
            else
            {
                _calendar.Visible = false;
                _lb.Text = MyLnkExpand;
            }
        }

        #endregion

        #region METODI
        /// <summary>
        /// Salva nella sessione, con chiave "this.title_FORMSTATE", la collezione dei valori dei controlli della maschera.<br></br>
        /// I controlli considerati sono quelli della form e nel placeholder "R_Dynamic", se presente.<br></br>
        /// Se lo stato (variabile MyFormState) non è istanziato, la maschera lo istanzia.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:18/03/2018 (v 1.2.0.2)
        /// </pre>
        /// </summary>
        public void saveFormState()
        {
            Control _cntrl = null;

            if (MyFormState == null)
            {
                MyFormState = new SimplexFormDictionary();
            }

            // salvo i valori dei controlli della form principale (form1)
            copyValues(MyFormState, "form1", "System.Web.UI.HtmlControls.HtmlForm");
            
            // salvo i valori dell'eventuale controllo R_Dynamic
            foreach (Control C in this.Controls)
            {
                _cntrl = searchControl(C, "R_Dynamic","System.Web.UI.WebControls.PlaceHolder");
                if (_cntrl != null)
                {
                    copyValues(MyFormState, ((System.Web.UI.WebControls.PlaceHolder)_cntrl));
                    break;      //TROVATO!!!
                }
            }
            Session.Add(this.Title + "_FORMSTATE", MyFormState);
        }

        /// <summary>
        /// Pulisce la collezione dei valori dei controlli della maschera e libera la sessione.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:18/03/2018 (v.1.2.0.2)
        /// </pre>
        /// </summary>
        public void clearFormState()
        {
            MyFormState.clear();
            Session[this.Title + "_FORMSTATE"] = null;
        }

        /// <summary>
        /// Carica nel'attributo <b>MyFormState</b> la collezione dei valori dei controlli della maschera e libera la sessione.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:18/03/2018 (v 1.2.0.2)
        /// </pre>
        /// </summary>
        public void loadFormState()
        {
            if(MyFormState!=null)
                MyFormState.clear();
            MyFormState = (SimplexFormDictionary)Session[this.Title + "_FORMSTATE"];           
        }

        /// <summary>
        /// <p>IT: Copia i valori dei controlli presenti in un PlaceHolder nell'oggetto di classe SimplexFormDictionary specificato.</p>
        /// <p>EN: Copies the values of the controls contained in a PlaceHolder in the specified SimplexFormDictionary class object.</p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:18/03/2018 (v 1.2.0.2)
        /// </pre>
        /// </summary>
        /// <param name="p_sfd">Oggetto di classe PlaceHolder. Non può essere nullo.</param>
        /// <param name="p_ph">Riferimento al PlaceHolder che deve visualizzare lo stato. Non può essere nullo.</param>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">In caso di parametro nullo.</exception>
        public void copyValues(SimplexFormDictionary p_sfd, PlaceHolder p_ph)
        {

            #region Controlli

            // controlli
            if (p_sfd == null)
            {
                simplex_ORM.Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.Message = _notSpecifiedParameter.Replace("%param%", "p_sfd");
                _ormm.MessageCode = 1;
                _ormm.MessageType = 2;
                _ormm.Source = "copyValues(SimplexFormDictionary, String, String)";
                throw _ormm;
            }

            if (p_ph == null)
            {
                simplex_ORM.Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.Message = _notSpecifiedParameter.Replace("%param%", "p_sfd");
                _ormm.MessageCode = 1;
                _ormm.MessageType = 2;
                _ormm.Source = "copyValues(SimplexFormDictionary, String, String)";
                throw _ormm;
            }
            #endregion

            #region Acquisizione dei valori dei controlli della maschera
            // ciclo di lettura dei controlli
            foreach (Control _c in p_ph.Controls)
            {
                if (_c.GetType().Name.CompareTo("TextBox") == 0)
                    p_sfd.Append(_c.ID, ((TextBox)_c).Text, _c.GetType().Name);

                if (_c.GetType().Name.CompareTo("DropDownList") == 0)
                    p_sfd.Append(_c.ID, ((DropDownList)_c).Text, _c.GetType().Name);

                if (_c.GetType().Name.CompareTo("Label") == 0)
                    p_sfd.Append(_c.ID, ((Label)_c).Text, _c.GetType().Name);

                if (_c.GetType().Name.CompareTo("CheckBox") == 0)
                {
                    if (((CheckBox)_c).Checked == true)
                        p_sfd.Append(_c.ID, "1", _c.GetType().Name);
                    else
                        p_sfd.Append(_c.ID, "0", _c.GetType().Name);
                }

                if (_c.GetType().Name.CompareTo("Calendar") == 0)
                {
                    String _DateStr = null;
                    _DateStr = simplex_ORM.Column.DateToSQLDateString(((Calendar)_c).SelectedDate);
                    p_sfd.Append(_c.ID, _DateStr, _c.GetType().Name);
                }
            }//fine ciclo
            #endregion

        }//fine

        /// <summary>
        /// <p>IT: Copia i valori dei controlli presenti in un container nell'oggetto di classe SimplexFormDictionary specificato.</p>
        /// <p>EN: Copies the values of the controls contained in a container in the specified SimplexFormDictionary class object.</p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:18/03/2018 (v 1.2.0.2)
        /// </pre>
        /// </summary>
        /// <param name="p_sfd">Oggetto di classe SimplexFormDictionary. Non può essere nullo.</param>
        /// <param name="p_CPH_ID">ID del contenitore. Non può essere nullo.</param>
        /// <param name="p_CPH_Type">Tipo del contenitore. Non può essere nullo.</param>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">In caso di parametro nullo.</exception>
        public void copyValues(SimplexFormDictionary p_sfd, String p_CPH_ID, String p_CPH_Type)
        {
            Control _control=null;

            #region Controlli
            // controlli
            if (p_sfd == null)
            {
                simplex_ORM.Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.Message = _notSpecifiedParameter.Replace("%param%", "p_sfd");
                _ormm.MessageCode = 1;
                _ormm.MessageType = 2;
                _ormm.Source = "copyValues(SimplexFormDictionary, String, String)";
                throw _ormm;
            }

            if (p_CPH_ID == null)
            {
                simplex_ORM.Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.Message = _notSpecifiedParameter.Replace("%param%", "p_CPH_ID");
                _ormm.MessageCode = 1;
                _ormm.MessageType = 2;
                _ormm.Source = "copyValues(SimplexFormDictionary, String, String)";
                throw _ormm;
            }

            if (p_CPH_Type == null)
            {
                simplex_ORM.Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.Message = _notSpecifiedParameter.Replace("%param%", "p_CPH_Type");
                _ormm.MessageCode = 1;
                _ormm.MessageType = 2;
                _ormm.Source = "copyValues(SimplexFormDictionary, String, String)";
                throw _ormm;
            }

            // Cerca i controlli del container
            foreach (Control C in this.Controls)
            {
                _control = searchControl(C, p_CPH_ID, p_CPH_Type);
                if (_control != null)
                    break;      //TROVATO!!!
            }

            if (_control == null)
            {
                simplex_ORM.Spx_ORMMessage ORMME = new Spx_ORMMessage("copyValues(SimplexFormDictionary, String, String): No controls found in the form.", 1002);
                ORMME.Source = "copyValues(SimplexFormDictionary, String, String)";
                ORMME.MessageType = 2;
                ORMME.MessageCode = 1002;
                throw ORMME;
            }
            #endregion

            #region Acquisizione dei valori dei controlli della maschera
            // ciclo di lettura dei controlli
            foreach (Control _c in _control.Controls)
            {
                if (_c.GetType().Name.CompareTo("TextBox") == 0)
                    p_sfd.Append(_c.ID, ((TextBox)_c).Text, _c.GetType().Name);

                if (_c.GetType().Name.CompareTo("DropDownList") == 0)
                    p_sfd.Append(_c.ID, ((DropDownList)_c).Text, _c.GetType().Name);

                if (_c.GetType().Name.CompareTo("Label") == 0)
                    p_sfd.Append(_c.ID, ((Label)_c).Text, _c.GetType().Name);

                if (_c.GetType().Name.CompareTo("CheckBox") == 0)
                {
                    if (((CheckBox)_c).Checked == true)
                        p_sfd.Append(_c.ID, "1", _c.GetType().Name);
                    else
                        p_sfd.Append(_c.ID, "0", _c.GetType().Name);
                }

                if (_c.GetType().Name.CompareTo("Calendar") == 0)
                {
                    String _DateStr = null;
                    _DateStr = simplex_ORM.Column.DateToSQLDateString(((Calendar)_c).SelectedDate);
                    p_sfd.Append(_c.ID, _DateStr, _c.GetType().Name);
                }
            }//fine ciclo
            #endregion

        }//fine


        /// <summary>
        /// <p>IT: Copia i valori dei controlli presenti in un container nell'oggetto di classe SQLTable.</p>
        /// <p>Provvede alla conversione delle date e dei numeri nei formati accettati dal SQL Server.</p>
        /// <p>EN: Copies the values of the controls contained in a container in the SQLTable class object.</p>
        /// <p>Provides the conversion of dates and numbers in those formats then SQL Server does accept.</p>
        /// <remarks>
        /// IT: In caso di errori questo metodo non lancia un'eccezione ma aggiunge un messaggio di classe Spx_ORMMessage
        /// alla lista dei messaggi referenziata dall'attributo privato MyErrors.
        /// EN: On errors occurred, this method doesn't throw an exception but adds a Spx_ORMMMessage's class instance to
        /// the list of the messages referenced by MyErrors private attribute.
        /// </remarks>
        /// <pre>
        /// @MdN
        /// Crtd:20/11/2014
        /// Mdfd:   05/06/2017 - Modificato copyValues() in modo che non vengano annullati i "valori" delle colonne di un oggetto ORM in caso di DDL disabilitate. --> v.1.1.0.3
        /// ---------------------
        /// 
        /// </pre>
        /// </summary>
        /// <param name="p_table">Tabella di destinazione</param>
        /// <param name="p_CPH_ID">ID del contenitore</param>
        /// <param name="p_CPH_Type">Tipo del contenitore</param>
        public void copyValues(simplex_ORM.SQLSrv.SQLTable p_table, String p_CPH_ID, String p_CPH_Type)
        {
            Control myControl = null;
            String l_CtrlName = null;
            String ValueStr = null;

            // Controllo preliminare
            if (p_table == null)
                throw new simplex_ORM.Spx_ORMMessage("copyValues(): no referenced table.", 1003);
            if ((p_CPH_Type == null) || (p_CPH_ID == null))
                throw new simplex_ORM.Spx_ORMMessage("copyValues(): no control type or ID specified.", 1004);
            
            // Cerca i controlli del container
            foreach (Control C in this.Controls)
            {
                myControl = searchControl(C, p_CPH_ID, p_CPH_Type);
                if (myControl != null)
                    break;      //TROVATO!!!
            }

            //doppio ciclo di acquisizione dei valori
            // Ciclo Esterno: cicla sui controlli della maschera
            if (myControl == null)
            {
                simplex_ORM.Spx_ORMMessage ORMME = new Spx_ORMMessage("SimplexForm.show(): No controls found in the form.", 1002);
                throw ORMME;
            }
            // Esame dei controlli appartenenti al contenitore.
            foreach (Control C in myControl.Controls)
            {
                if (C == null)
                    continue;
                l_CtrlName = C.ID;
                if (l_CtrlName == null)
                    continue;
                // Ciclo interno: ricerca della colonna corrispondente
                foreach (String l_FieldName in p_table.SQLColumns)
                {
                    if (l_CtrlName.CompareTo(l_FieldName) == 0)
                    {
                        try
                        {
                            // TROVATO IL CONTROLLO CHE HA LO STESSO NOME DELLA COLONNA
                            // Prelevo il valore dalla Request
                            // Se il nome del campo nella request contiene un carattare '_' siamo fritti
                            if (l_FieldName.Contains('_'))
                            {
                                //Tolgo da Control.ClientID il nome del campo
                                int l_startsFrom = C.ClientID.IndexOf(l_FieldName);
                                String l_tmpName = C.ClientID.Remove(l_startsFrom);
                                l_tmpName = l_tmpName.Replace('_', '$');
                                l_tmpName = String.Concat(l_tmpName, l_FieldName);
                                ValueStr = Request.Form[l_tmpName];
                            }
                            else 
                                ValueStr = Request.Form[C.ClientID.Replace('_', '$')];
                            if (C.GetType().Name.CompareTo("TextBox") == 0)
                            {
                                // TEXTBOX --> DATA ESTESA
                                if (Column.isDateTime(ValueStr, "DD/MM/YYYY hh.mm.ss") == true)
                                {
                                    DateTime TmpDt;
                                    TmpDt = Column.parseDateTimeString(ValueStr, "DD/MM/YYYY hh.mm.ss");
                                    p_table.setValue(l_FieldName, Column.DateToSQLDateString(TmpDt));
                                    continue;
                                }
                                // TEXTBOX --> DATA SEMPLICE
                                if (Column.isDateTime(ValueStr, "DD/MM/YYYY") == true)
                                {
                                    DateTime TmpDt;
                                    TmpDt = Column.parseDateTimeString(ValueStr, "DD/MM/YYYY");
                                    p_table.setValue(l_FieldName, Column.DateToSQLDateString(TmpDt));
                                    continue;
                                }
                                // TEXTBOX --> NUMERO DECIMALE
                                if (Column.isDecimal(ValueStr) == true)
                                {
                                    p_table.setValue(l_FieldName, Column.clearString(ValueStr, '.').Replace(',', '.'));
                                    continue;
                                }

                                // TEXTBOX --> NUMERO INTERO
                                if (Column.isDecimal(ValueStr) == true)
                                {
                                    p_table.setValue(l_FieldName, Column.clearString(ValueStr, '.'));
                                    continue;
                                }

                                // TEXTBOX <DEFAULT>
                                // @MdN 31/01/2015 - gestione del caso ValueStr di lunghezza 0
                                if (ValueStr != null && ValueStr.Length == 0)
                                    ValueStr = null;
                                p_table.setValue(l_FieldName, ValueStr);

                            } // fine TEXTBOX

                            //LABLE: non ha molto senso 
                            if (C.GetType().Name.CompareTo("Label") == 0 && ((Label)C).Text != null)
                            {
                                //p_table.setValue(l_FieldName,((Label)C).Text);
                                p_table.setValue(l_FieldName, ValueStr);
                                continue;
                            }

                            // HYPERLINK: non ha molto senso
                            if (C.GetType().Name.CompareTo("HyperLink") == 0 && ((HyperLink)C).Text != null)
                            {
                                p_table.setValue(l_FieldName, ((HyperLink)C).Text);
                                p_table.setValue(l_FieldName, ValueStr);
                                continue;
                            }

                            // CHECKBOX
                            if (C.GetType().Name.CompareTo("CheckBox") == 0)
                            {
                                if (((CheckBox)C).Checked == true)
                                    p_table.setValue(l_FieldName, "1");
                                else
                                    p_table.setValue(l_FieldName, "0");
                                continue;
                            } // FINE CHECKBOX

                            //CALENDAR
                            if (C.GetType().Name.CompareTo("Calendar") == 0)
                            {
                                //p_table.setValue(l_FieldName,Column.DateToSQLDateString(((Calendar)C).VisibleDate));
                                if (Column.isDateTime(ValueStr, "DD/MM/YYYY") == true)
                                    p_table.setValue(l_FieldName, Column.DateToSQLDateString(Column.parseDateTimeString(ValueStr, "DD/MM/YYYY")));
                                continue;
                            }//fine CALENDAR

                            //DROPDOWNLIST
                            if (C.GetType().Name.CompareTo("DropDownList") == 0)
                            {
                                //p_table.setValue(l_FieldName, ((DropDownList)C).SelectedValue);
                                //p_table.setValue(l_FieldName, ValueStr);  //<deleted @MdN 05/06/2017>
                                if (((DropDownList)C).Enabled == false)      //<added @MdN 05/06/2017><mdfd @MdN 11/10/2017>
                                    continue;
                                else
                                    p_table.setValue(l_FieldName, ValueStr);  //<mdfd @MdN 11/10/2017>
                            }//FINE DROPDOWNLIST

                            //LISTBOX
                            if (C.GetType().Name.CompareTo("ListBox") == 0)
                            {
                                //p_table.setValue(l_FieldName, ((ListBox)C).SelectedValue);
                                p_table.setValue(l_FieldName, ValueStr);
                                continue;
                            }//FINE LISTBOX
                        }
                        catch (simplex_ORM.Spx_ORMMessage ORMM)
                        {
                            MyErrors.Add(ORMM);
                        }
                    }
                }
            }

        } // the end

        /// <summary>
        /// <p>IT: Salva il contenuto dell'oggetto di riferimento (TBL_UNDER_FORM) in un record della corrispondente tabella.</p>
        /// <p>EN: Saves the referenced object's content (TBL_UNDER_FORM) in the corresponding table.</p>
        /// ----
        /// @MdN
        /// Crtd: 07/12/2014
        /// Mdfd: @MdN 08/12/2014;
        /// </summary>
        protected virtual bool save()
        {
            if (TBL_UNDER_FORM == null)
                return false;
            try
            {
                TBL_UNDER_FORM.save(false);
                return true;
            }
            catch (simplex_ORM.Spx_ORMMessage SORMM)
            {
                MyErrors.Add(SORMM);
                return false;
            }
        }

        /// <summary>
        /// Consente di caricare tutte le variabili necessarie al funzionamento di una data 
        /// SimplexForm (ad esempio dalla Sessione o dalla Request).
        /// Ogni implementazione DEVE effettuare l'override del metodo.
        /// -----
        /// @MdN
        /// Crtd 05/12/2014
        /// </summary>
        protected virtual void preloadEnvironment()
        {
            //NOP
        }
        

        /// <summary>
        /// <p>
        /// EN: Modifies the content of the referenced object with the 
        /// actual values of the form's controls a saves the object.
        /// Calls copyValues
        /// </p>
        /// <pre>
        /// @MdN
        /// Crtd: 22/11/2014 - 23/10/2014
        /// Tstd:
        /// Mdfd: @MdN 26/11/2014;  @MdN 10/02/2015
        /// </pre>
        /// </summary>
        /// <param name="p_Table">Referenced SQLTable class object.</param>
        /// <param name="p_CPH_ID">ID of the container.</param>
        /// <param name="p_CPH_Type">Type of the container.</param>
        /// <remarks>The default value of p_nocheck is 'false'. See SimplexForm.save().</remarks>
        /// 
        public virtual void modify(simplex_ORM.SQLSrv.SQLTable p_Table, String p_CPH_ID, String p_CPH_Type)
        {
            this.copyValues(p_Table, p_CPH_ID, p_CPH_Type);
            if (p_Table.CurrentConnection.State == System.Data.ConnectionState.Closed)
                p_Table.CurrentConnection.Open();                                       // @MdN 10/02/2015
            try
            {
                p_Table.save(false);
            }
            catch (Spx_ORMMessage ORMM)
            {
                this.MyErrors.Add(ORMM);
                throw ORMM;
            }
            p_Table.CurrentConnection.Close();                                          //@MdN 10/02/2015
            show(p_Table, p_CPH_ID, p_CPH_Type);
        }

        /// <summary>
        /// <p> Modifies the content of the referenced object with the 
        /// actual values of the form's controls.</p>
        /// <br>Calls copyValues.</br>
        /// Uses:<br/>
        /// <ul style="list-style-type:square">
        ///  <li>p_Table=TBL_UNDER_FORM</li>
        ///  <li>p_CPH_ID="form1"</li>
        ///  <li>p_CPH_Type="System.Web.UI.HtmlControls.HtmlForm"</li>
        ///  <li>p_nocheck=false</li>
        ///</ul> 
        /// 
        /// <pre>
        /// @MdN
        /// Crtd: 23/11/2014 - 23/10/2014
        /// Tstd:
        /// Mdfd: @MdN 26/11/2014; @MdN 10/02/2015
        /// </pre>
        /// </summary>
        /// <remarks>Check if the referenced object TBL_UNDER_FORM is not null.</remarks>
        public virtual void modify()
        {
            if (TBL_UNDER_FORM == null)
                return;
            this.copyValues(this.TBL_UNDER_FORM, "form1", "System.Web.UI.HtmlControls.HtmlForm");
            if (TBL_UNDER_FORM.CurrentConnection.State == System.Data.ConnectionState.Closed)   //@MdN 10/02/2015
                TBL_UNDER_FORM.CurrentConnection.Open();
            this.TBL_UNDER_FORM.save(false);
            TBL_UNDER_FORM.CurrentConnection.Close();                                           // @MdN 10/02/2015
            show(this.TBL_UNDER_FORM, "form1", "System.Web.UI.HtmlControls.HtmlForm");
        }

        /// <summary>
        /// Modifies the content of the referenced object with the 
        /// actual values of the form's controls.
        /// Calls copyValues
        /// @MdN
        /// Crtd: 23/10/2014
        /// Tstd:
        /// Mdfd: @MdN 26/11/2014; @MdN 10/02/2015
        /// </summary>
        public virtual void modify(simplex_ORM.SQLSrv.SQLTable p_Table, String p_CPH_ID, String p_CPH_Type, Boolean p_nocheck)
        {
            this.copyValues(p_Table, p_CPH_ID, p_CPH_Type);
            if (p_Table.CurrentConnection.State == System.Data.ConnectionState.Closed)   //@MdN 10/02/2015
                p_Table.CurrentConnection.Open();
            p_Table.save(p_nocheck);
            p_Table.CurrentConnection.Close();                                           // @MdN 10/02/2015
            show(p_Table, p_CPH_ID, p_CPH_Type);
        }

        /// <summary>
        /// @MdN - 27/10/2014
        /// Effettua una vista in profondità della struttura Controls alla ricerca
        /// del particolare ContentPlaceHolder passato per argomento.
        /// </summary>
        /// <param name="p_C">Nodo System.Web.UI.Control</param>
        /// <param name="p_CPH_ID">ID del ContentPlaceHolder.</param>
        /// <returns>il nodo System.Web.UI.Control contenente i controlli della form.</returns>
        private Control searchContentPlaceHolder(Control p_C, String p_CPH_ID)
        {
            Control l_C=null;
            foreach (Control C in p_C.Controls)
            {
                //System.Console.Out.WriteLine(C.ClientID); //DEBUG
                if (C != null)
                {
                    if (C.ToString().CompareTo("System.Web.UI.WebControls.ContentPlaceHolder") == 0 && C.ClientID.CompareTo(p_CPH_ID) == 0)
                    {
                        return C;                        
                    }
                    else
                    {
                        l_C = searchContentPlaceHolder(C, p_CPH_ID);
                        if (l_C != null)
                            return l_C;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// @MdN - 15/11/2014
        /// Effettua una vista in profondità della struttura Controls alla ricerca
        /// del particolare tipo di controllo passato per argomento.
        /// Il controllo viene identificato per nome e per tipo.
        /// Il tipo del controllo è rappresentanto mediante il nome completo della sua classe.
        /// Esempi:
        /// System.Web.UI.HtmlControls.HtmlForm
        /// System.Web.UI.WebControls.ContentPlaceHolder
        /// etc.
        /// </summary>
        /// <param name="p_C">Nodo System.Web.UI.Control di partenza della ricerca</param>
        /// <param name="p_CPH_ID">ID della form.</param>
        /// <param name="p_Type">Nome completo della classe del controllo da cercare.</param>
        /// <returns>il nodo System.Web.UI.Control contenente i controlli della form.</returns>
        protected Control searchControl(Control p_C, String p_CPH_ID, String p_Type)
        {
            Control l_C = null;
            foreach (Control C in p_C.Controls)
            {
                //System.Console.Out.WriteLine(C.ClientID); //DEBUG
                if (C != null)
                {
                    if (C.ToString().CompareTo(p_Type) == 0 && C.ID.CompareTo(p_CPH_ID) == 0)
                    {
                        return C;
                    }
                    else
                    {
                        //ricerca in profondità
                        l_C = searchControl(C, p_CPH_ID,p_Type);
                        if (l_C != null)
                            return l_C;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// @MdN - 02/11/2014
        /// Effettua una vista in profondità della struttura Controls alla ricerca
        /// del particolare System.Web.UI.HtmlControls.HtmlForm passato per argomento.
        /// </summary>
        /// <param name="p_C">Nodo System.Web.UI.Control</param>
        /// <param name="p_CPH_ID">ID della form.</param>
        /// <returns>il nodo System.Web.UI.Control contenente i controlli della form.</returns>
        private Control searchContentHTMLForm(Control p_C, String p_CPH_ID)
        {
            Control l_C = null;
            foreach (Control C in p_C.Controls)
            {
                //System.Console.Out.WriteLine(C.ClientID); //DEBUG
                if (C != null)
                {
                    if (C.ToString().CompareTo("System.Web.UI.HtmlControls.HtmlForm") == 0 && C.ID.CompareTo(p_CPH_ID) == 0)
                    {
                        return C;
                    }
                    else
                    {
                        //ricerca in profondità
                        l_C = searchContentHTMLForm(C, p_CPH_ID);
                        if (l_C != null)
                            return l_C;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// <p>EN: Shows the content of the 'MaskState' in the controls contained in then specified container.</p>
        /// 
        /// <p>IT: Mostra il contenuto dell'oggetto 'MaskState' nei controlli presenti nel container specificato.</p>
        /// <p>Questa versione del metodo consente di anteporre ai nomi delle chiavi della collection <i>MyMaskState</i> 
        /// il prefisso <i>p_Prefix</i> in modo da poter mostrare (<b>SOLO IN VISUALIZZAZIONE</b>) i valori dei campi 
        /// di un secondo oggetto (ad esempio una tabella).<br></br>
        /// E' il caso, ad esempio, di una maschera che gestisce le informazioni di un oggetto Componente: 
        /// è possibile mostrare i campi del suo oggetto Contenitore inserendo in maschera i corrispondenti campi con un prefisso anteposto.<br></br> 
        /// Esempio: per mostrare l'attributo Contenitore.ID si inserirà il campo  PRFX_ID che non confliggerà con il campo ID corrispondente all'attributo Componente.ID.<br></br>
        /// <pre>
        /// @MdN
        /// Crtd: 18/03/2018 (v 1.2.0.2)
        /// </pre>
        /// <example>
        /// Sono contenitori i seguenti:
        /// <ul>
        /// <li>System.Web.UI.HtmlControls.HtmlForm</li>
        /// <li>System.Web.UI.WebControls.ContentPlaceHolder</li>
        /// </ul>
        /// </example>
        /// </summary>
        /// <param name="p_CPH_ID">Id del container</param>
        /// <param name="p_sfd">Riferimento ad un oggetto di classe SimplexFormDictionary.</param>
        /// <param name="p_Type">Tipo del contaner</param>
        /// <param name="p_Prefix">Prefisso da inserire in testa al nome del campo</param>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">
        /// 1002: None controls found in the form/ Nessun controllo trovato nella maschera
        /// </exception>
        public void show(SimplexFormDictionary p_sfd, String p_CPH_ID, String p_Type, String p_Prefix)
        {
            // Ricerca del ContentPlaceHolder1
            Control myControl = null;
            foreach (Control C in this.Controls)
            {
                myControl = searchControl(C, p_CPH_ID, p_Type);
                if (myControl != null)
                    break;      //TROVATO!!!
            }
            //Doppio ciclo
            // Ciclo Esterno: cicla sui controlli della maschera
            String l_CtrlName = null;
            if (myControl == null)
            {
                simplex_ORM.Spx_ORMMessage ORMME = new Spx_ORMMessage();
                ORMME.Message = _noControlsFound.Replace("%param%", p_CPH_ID);
                ORMME.MessageCode = 1002;
                ORMME.MessageType = 2;
                throw ORMME;
            }
            foreach (Control C in myControl.Controls)
            {
                if (C == null)
                    continue;
                l_CtrlName = C.ID;
                if (l_CtrlName == null)
                    continue;
                // Ciclo interno: ricerca della colonna corrispondente
                foreach (String l_FieldName in p_sfd.Keys)
                {
                    String _FieldName = null;

                    if (p_Prefix != null)
                        _FieldName = p_Prefix + l_FieldName;
                    else
                        _FieldName = l_FieldName;

                    if ((l_CtrlName.CompareTo(_FieldName) == 0) && (p_sfd[l_FieldName]!=null))
                    {
                        if ((C.GetType().Name.CompareTo("TextBox") == 0))
                        {
                            try
                            {
                                if (Column.isSQLDateString(p_sfd[l_FieldName]) == true)
                                    ((TextBox)C).Text = (Column.SQLDateStringToDate(p_sfd[l_FieldName])).ToString("dd/MM/yyyy HH.mm.ss");
                                else if (Column.isDecimal(p_sfd[l_FieldName]) == true)
                                    ((TextBox)C).Text = p_sfd[l_FieldName].Replace('.', ',');
                                else
                                    ((TextBox)C).Text = p_sfd[l_FieldName];
                            }
                            catch (Spx_ORMMessage E)
                            {
                                // salta il campo
                                ((TextBox)C).Text = p_sfd[l_FieldName];
                                continue;
                            }
                        }
                        if (C.GetType().Name.CompareTo("Label") == 0)
                            ((Label)C).Text = p_sfd[l_FieldName];

                        /* AL MOMENTO SI ESCLUDONO GLI HYPERLINKS */
                        //if (C.GetType().Name.CompareTo("HyperLink") == 0)
                        //{
                        //    ((HyperLink)C).Text = p_sfd[l_FieldName].Text;
                        //    ((HyperLink)C).NavigateUrl = p_sfd[l_FieldName].Text;
                        //}

                        if (C.GetType().Name.CompareTo("CheckBox") == 0)
                        {
                            //Il checkbox di norma è associato ad un Booleano/Bit o ad un intero
                            //Si deve discriminare questo caso: @MdN - 31/01/2015
                            if (Column.isNumber(p_sfd[l_FieldName]) == true)
                            {
                                if (p_sfd[l_FieldName]!= null && Int32.Parse(p_sfd[l_FieldName]) == 1)
                                    ((CheckBox)C).Checked = true;
                                else
                                    ((CheckBox)C).Checked = false;
                            }
                            else
                            {
                                //Siamo in presenza di un booleano
                                String _bool = p_sfd[l_FieldName].ToLower();
                                if ((_bool.CompareTo("true") == 0) || _bool.CompareTo("t") == 0 || _bool.CompareTo("vero") == 0 || _bool.CompareTo("v") == 0)
                                    ((CheckBox)C).Checked = true;
                                else
                                    ((CheckBox)C).Checked = false;
                            }
                        }

                        if (C.GetType().Name.CompareTo("Calendar") == 0)
                        {
                            if (p_sfd[l_FieldName] != null && Column.isSQLDateString(p_sfd[l_FieldName]) == true)
                            {
                                ((Calendar)C).VisibleDate = Column.SQLDateStringToDate(p_sfd[l_FieldName]);
                                ((Calendar)C).SelectedDate = Column.SQLDateStringToDate(p_sfd[l_FieldName]);
                            }

                        }
                        if (C.GetType().Name.CompareTo("DropDownList") == 0)
                        {
                            int myx;
                            // @MdN: per evitare [HttpException (0x80004005): Impossibile selezionare più elementi in un DropDownList.]
                            // deselezionare la voce corrente
                            ListItem DummyItem = ((DropDownList)C).SelectedItem;
                            if (DummyItem != null)
                                DummyItem.Selected = false;
                            foreach (ListItem MyItem in ((DropDownList)C).Items)
                            {
                                if (Int32.TryParse(p_sfd[l_FieldName], out myx) == true)
                                {
                                    // la chiave è un numero
                                    if (MyItem.Value.ToString().CompareTo(p_sfd[l_FieldName]) == 0)
                                    {
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (MyItem.Value.CompareTo(p_sfd[l_FieldName]) == 0)
                                    {
                                        //la chiave è una stringa
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                            }
                        } // FINE DROPDOWN LIST

                        if (C.GetType().Name.CompareTo("ListBox") == 0)
                        {
                            int myx;
                            foreach (ListItem MyItem in ((ListBox)C).Items)
                            {
                                if (Int32.TryParse(p_sfd[l_FieldName], out myx) == true)
                                {
                                    // la chiave è un numero
                                    if (MyItem.Value.ToString().CompareTo(p_sfd[l_FieldName]) == 0)
                                    {
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (MyItem.Value.CompareTo(p_sfd[l_FieldName]) == 0)
                                    {
                                        //la chiave è una stringa
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                            }
                        } // FINE LISTBOX
                    }
                }
            } // Per ogni controllo            
        } // fine

        /// <summary>
        /// <p>EN: Shows the content of the referencing object in the controls contained in then specified form.</p>
        /// <p>IT: Mostra il contenuto dell'oggetto di riferimento nei controlli presenti nel modulo specificato.</p>
        /// 
        /// @MdN
        /// Crtd: 21/11/2014
        /// Tstd:
        /// Mdfd: 
        /// </summary>
        /// <param name="p_table">Name of the table</param>
        /// <param name="p_CPH_ID">Identifier of the form</param>
        public void show(simplex_ORM.SQLSrv.SQLTable p_table, String p_CPH_ID)
        {
            show(p_table, p_CPH_ID, "System.Web.UI.HtmlControls.HtmlForm");
        }

        /// <summary>
        /// <p>EN: Shows the content of the referencing object in the controls contained in then specified container.</p>
        /// <p>IT: Mostra il contenuto dell'oggetto di riferimento nei controlli presenti nel container specificato.</p>
        /// 
        /// @MdN
        /// Crtd: 04/11/2014
        /// Tstd:
        /// Mdfd: @MdN 21/11/2014 - Refactorized
        /// Mdfd: @MdN 31/01/2015 - Checkbox improovement.
        /// </summary>
        /// <param name="p_CPH_ID">Identifier of the container</param>
        /// <param name="p_table">Name of the table</param>
        /// <param name="p_Type">Tipo del contaner</param>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">
        /// 1002: Non controls found in the form
        /// </exception>
        /// <example>
        /// Sono contenitori i seguenti:
        /// <ul>
        /// <li>System.Web.UI.HtmlControls.HtmlForm</li>
        /// <li>System.Web.UI.WebControls.ContentPlaceHolder</li>
        /// </ul>
        /// </example>
        public void show(simplex_ORM.SQLSrv.SQLTable p_table, String p_CPH_ID, String p_Type)
        {
            //System.Web.UI.ControlCollection l_Controls = null;
            // Ricerca del ContentPlaceHolder1
            Control myControl = null;
            foreach (Control C in this.Controls)
            {
                //myControl = serchContentPlaceHolder(C, "ctl00_ContentPlaceHolder1");
                //myControl = searchContentHTMLForm(C, p_formName);
                myControl = searchControl(C, p_CPH_ID, p_Type);
                if (myControl != null)
                    break;      //TROVATO!!!

            }
            //Doppio ciclo
            // Ciclo Esterno: cicla sui controlli della maschera
            String l_CtrlName = null;
            if (myControl == null)
            {
                simplex_ORM.Spx_ORMMessage ORMME = new Spx_ORMMessage("SimplexForm.show(): Non controls found in the container.", 1002);
                throw ORMME;
            }
            foreach (Control C in myControl.Controls)
            {
                if (C == null)
                    continue;
                l_CtrlName = C.ID;
                if (l_CtrlName == null)
                    continue;
                // Ciclo interno: ricerca della colonna corrispondente
                foreach (String l_FieldName in p_table.SQLColumns)
                {
                    if ((l_CtrlName.CompareTo(l_FieldName) == 0) && (p_table.getValue(l_FieldName)!=null))
                    {
                        if ((C.GetType().Name.CompareTo("TextBox") == 0))
                        {
                            try
                            {
                                if (Column.isSQLDateString(p_table.getValue(l_FieldName)) == true)
                                    ((TextBox)C).Text = (Column.SQLDateStringToDate(p_table.getValue(l_FieldName))).ToString("dd/MM/yyyy HH.mm.ss");
                                else if (Column.isDecimal(p_table.getValue(l_FieldName)) == true)
                                    ((TextBox)C).Text = p_table.getValue(l_FieldName).Replace('.', ',');
                                else
                                    ((TextBox)C).Text = p_table.getValue(l_FieldName);
                            }
                            catch (Spx_ORMMessage E)
                            {
                                // salta il campo
                                ((TextBox)C).Text = p_table.getValue(l_FieldName);
                                continue;
                            }
                        }
                        if (C.GetType().Name.CompareTo("Label") == 0)
                            ((Label)C).Text = p_table.getValue(l_FieldName);
                        if (C.GetType().Name.CompareTo("HyperLink") == 0)
                        {
                            ((HyperLink)C).Text = p_table.getValue(l_FieldName);
                            ((HyperLink)C).NavigateUrl = p_table.getValue(l_FieldName);
                        }
                        if (C.GetType().Name.CompareTo("CheckBox") == 0)
                        {
                            //Il checkbox di norma è associato ad un Booleano/Bit o ad un intero
                            //Si deve discriminare questo caso: @MdN - 31/01/2015
                            if (Column.isNumber(p_table.getValue(l_FieldName)) == true)
                            {
                                if (p_table.getValue(l_FieldName) != null && Int32.Parse(p_table.getValue(l_FieldName)) == 1)
                                    ((CheckBox)C).Checked = true;
                                else
                                    ((CheckBox)C).Checked = false;
                            }
                            else
                            {
                                //Siamo in presenza di un booleano: @MdN - 31/01/2015
                                String _bool = p_table.getValue(l_FieldName).ToLower();
                                if((_bool.CompareTo("true")==0)||_bool.CompareTo("t")==0 || _bool.CompareTo("vero")==0|| _bool.CompareTo("v")==0)
                                    ((CheckBox)C).Checked = true;
                                else
                                    ((CheckBox)C).Checked = false;
                            }
                        }

                        if (C.GetType().Name.CompareTo("Calendar") == 0)
                        {
                            if (p_table.getValue(l_FieldName) != null && Column.isSQLDateString(p_table.getValue(l_FieldName)) == true)
                            {
                                ((Calendar)C).VisibleDate = Column.SQLDateStringToDate(p_table.getValue(l_FieldName));
                                ((Calendar)C).SelectedDate = Column.SQLDateStringToDate(p_table.getValue(l_FieldName));
                            }

                        }
                        if (C.GetType().Name.CompareTo("DropDownList") == 0)
                        {
                            int myx;
                            // @MdN 13/12/2014: per evitare [HttpException (0x80004005): Impossibile selezionare più elementi in un DropDownList.]
                            // deselezionare la voce corrente
                            ListItem DummyItem=((DropDownList)C).SelectedItem;
                            if (DummyItem != null)
                                DummyItem.Selected = false;
                            foreach (ListItem MyItem in ((DropDownList)C).Items)
                            {
                                if (Int32.TryParse(p_table.getValue(l_FieldName), out myx) == true)
                                {
                                    // la chiave è un numero
                                    if (MyItem.Value.ToString().CompareTo(p_table.getValue(l_FieldName)) == 0)
                                    {
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (MyItem.Value.CompareTo(p_table.getValue(l_FieldName)) == 0)
                                    {
                                        //la chiave è una stringa
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                            }
                        } // FINE DROPDOWN LIST

                        if (C.GetType().Name.CompareTo("ListBox") == 0)
                        {
                            int myx;
                            foreach (ListItem MyItem in ((ListBox)C).Items)
                            {
                                if (Int32.TryParse(p_table.getValue(l_FieldName), out myx) == true)
                                {
                                    // la chiave è un numero
                                    if (MyItem.Value.ToString().CompareTo(p_table.getValue(l_FieldName)) == 0)
                                    {
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (MyItem.Value.CompareTo(p_table.getValue(l_FieldName)) == 0)
                                    {
                                        //la chiave è una stringa
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                            }
                        } // FINE LISTBOX
                    }
                }
            } // Per ogni controllo            

        }

        /// <summary>
        /// <p>EN: Shows the content of the referencing object in the controls contained in then specified container.</p>
        /// 
        /// <p>IT: Mostra il contenuto dell'oggetto di riferimento nei controlli presenti nel container specificato.</p>
        /// <p>Questa versione del metodo consente di anteporre ai nomi delle colonne della tabella <i>p_table</i> il prefisso <i>p_Prefix</i> in modo da poter mostrare (<b>SOLO IN VISUALIZZAZIONE</b>) i valori dei campi di un secondo oggetto.<br></br>
        /// E' il caso, ad esempio, di una maschera che gestisce le informazioni di un oggetto Componente: 
        /// è possibile mostrare i campi del suo oggetto Contenitore inserendo in maschera i corrispondenti campi con un prefisso anteposto.<br></br> 
        /// Esempio: per mostrare l'attributo Contenitore.ID si inserirà il campo  PRFX_ID che non confliggerà con il campo ID corrispondente all'attributo Componente.ID.<br></br>
        /// Se il parametro p_Prefix=null, il metodo si comporta come l'overload con tra parametri: <i>show(simplex_ORM.SQLSrv.SQLTable p_table, String p_CPH_ID, String p_Type)</i>.</p>
        /// 
        /// @MdN
        /// Crtd: 04/11/2014
        /// Tstd:
        /// Mdfd: @MdN 21/11/2014 - Refactorized
        /// Mdfd: @MdN 31/01/2015 - Checkbox improovement.
        /// Mdfd: @MdN 22/08/2016 - Prefix -
        /// </summary>
        /// <param name="p_CPH_ID">Identifier of the container</param>
        /// <param name="p_table">Name of the table</param>
        /// <param name="p_Type">Tipo del contaner</param>
        /// <param name="p_Prefix">Prefisso da inserire in testa al nome del campo</param>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">
        /// 1002: None controls found in the form
        /// </exception>
        /// <example>
        /// Sono contenitori i seguenti:
        /// <ul>
        /// <li>System.Web.UI.HtmlControls.HtmlForm</li>
        /// <li>System.Web.UI.WebControls.ContentPlaceHolder</li>
        /// </ul>
        /// </example>
        public void show(simplex_ORM.SQLSrv.SQLTable p_table, String p_CPH_ID, String p_Type, String p_Prefix)
        {
            //System.Web.UI.ControlCollection l_Controls = null;
            // Ricerca del ContentPlaceHolder1
            Control myControl = null;
            foreach (Control C in this.Controls)
            {
                //myControl = serchContentPlaceHolder(C, "ctl00_ContentPlaceHolder1");
                //myControl = searchContentHTMLForm(C, p_formName);
                myControl = searchControl(C, p_CPH_ID, p_Type);
                if (myControl != null)
                    break;      //TROVATO!!!

            }
            //Doppio ciclo
            // Ciclo Esterno: cicla sui controlli della maschera
            String l_CtrlName = null;
            if (myControl == null)
            {
                simplex_ORM.Spx_ORMMessage ORMME = new Spx_ORMMessage("SimplexForm.show(): Non controls found in the container.", 1002);
                throw ORMME;
            }
            foreach (Control C in myControl.Controls)
            {
                if (C == null)
                    continue;
                l_CtrlName = C.ID;
                if (l_CtrlName == null)
                    continue;
                // Ciclo interno: ricerca della colonna corrispondente
                foreach (String l_FieldName in p_table.SQLColumns)
                {
                    String _FieldName = null; //<added @MdN: 22/08/2016>

                    //<added @MdN: 22/08/2016>
                    if (p_Prefix != null)
                        _FieldName = p_Prefix + l_FieldName;
                    else
                        _FieldName = l_FieldName;
                    //</added @MdN: 22/08/2016>

                    if ((l_CtrlName.CompareTo(_FieldName) == 0) && (p_table.getValue(l_FieldName) != null))
                    {
                        if ((C.GetType().Name.CompareTo("TextBox") == 0))
                        {
                            try
                            {
                                if (Column.isSQLDateString(p_table.getValue(l_FieldName)) == true)
                                    ((TextBox)C).Text = (Column.SQLDateStringToDate(p_table.getValue(l_FieldName))).ToString("dd/MM/yyyy HH.mm.ss");
                                else if (Column.isDecimal(p_table.getValue(l_FieldName)) == true)
                                    ((TextBox)C).Text = p_table.getValue(l_FieldName).Replace('.', ',');
                                else
                                    ((TextBox)C).Text = p_table.getValue(l_FieldName);
                            }
                            catch (Spx_ORMMessage E)
                            {
                                // salta il campo
                                ((TextBox)C).Text = p_table.getValue(l_FieldName);
                                continue;
                            }
                        }
                        if (C.GetType().Name.CompareTo("Label") == 0)
                            ((Label)C).Text = p_table.getValue(l_FieldName);
                        if (C.GetType().Name.CompareTo("HyperLink") == 0)
                        {
                            ((HyperLink)C).Text = p_table.getValue(l_FieldName);
                            ((HyperLink)C).NavigateUrl = p_table.getValue(l_FieldName);
                        }
                        if (C.GetType().Name.CompareTo("CheckBox") == 0)
                        {
                            //Il checkbox di norma è associato ad un Booleano/Bit o ad un intero
                            //Si deve discriminare questo caso: @MdN - 31/01/2015
                            if (Column.isNumber(p_table.getValue(l_FieldName)) == true)
                            {
                                if (p_table.getValue(l_FieldName) != null && Int32.Parse(p_table.getValue(l_FieldName)) == 1)
                                    ((CheckBox)C).Checked = true;
                                else
                                    ((CheckBox)C).Checked = false;
                            }
                            else
                            {
                                //Siamo in presenza di un booleano: @MdN - 31/01/2015
                                String _bool = p_table.getValue(l_FieldName).ToLower();
                                if ((_bool.CompareTo("true") == 0) || _bool.CompareTo("t") == 0 || _bool.CompareTo("vero") == 0 || _bool.CompareTo("v") == 0)
                                    ((CheckBox)C).Checked = true;
                                else
                                    ((CheckBox)C).Checked = false;
                            }
                        }

                        if (C.GetType().Name.CompareTo("Calendar") == 0)
                        {
                            if (p_table.getValue(l_FieldName) != null && Column.isSQLDateString(p_table.getValue(l_FieldName)) == true)
                            {
                                ((Calendar)C).VisibleDate = Column.SQLDateStringToDate(p_table.getValue(l_FieldName));
                                ((Calendar)C).SelectedDate = Column.SQLDateStringToDate(p_table.getValue(l_FieldName));
                            }

                        }
                        if (C.GetType().Name.CompareTo("DropDownList") == 0)
                        {
                            int myx;
                            // @MdN 13/12/2014: per evitare [HttpException (0x80004005): Impossibile selezionare più elementi in un DropDownList.]
                            // deselezionare la voce corrente
                            ListItem DummyItem = ((DropDownList)C).SelectedItem;
                            if (DummyItem != null)
                                DummyItem.Selected = false;
                            foreach (ListItem MyItem in ((DropDownList)C).Items)
                            {
                                if (Int32.TryParse(p_table.getValue(l_FieldName), out myx) == true)
                                {
                                    // la chiave è un numero
                                    if (MyItem.Value.ToString().CompareTo(p_table.getValue(l_FieldName)) == 0)
                                    {
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (MyItem.Value.CompareTo(p_table.getValue(l_FieldName)) == 0)
                                    {
                                        //la chiave è una stringa
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                            }
                        } // FINE DROPDOWN LIST

                        if (C.GetType().Name.CompareTo("ListBox") == 0)
                        {
                            int myx;
                            foreach (ListItem MyItem in ((ListBox)C).Items)
                            {
                                if (Int32.TryParse(p_table.getValue(l_FieldName), out myx) == true)
                                {
                                    // la chiave è un numero
                                    if (MyItem.Value.ToString().CompareTo(p_table.getValue(l_FieldName)) == 0)
                                    {
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (MyItem.Value.CompareTo(p_table.getValue(l_FieldName)) == 0)
                                    {
                                        //la chiave è una stringa
                                        MyItem.Selected = true;
                                        break;
                                    }
                                }
                            }
                        } // FINE LISTBOX
                    }
                }
            } // Per ogni controllo            
        }// fine show()

        /// <summary>
        /// Shows the content of the referenced object.
        /// @MdN
        /// Crtd: 23/10/2014
        /// Tstd:
        /// Mdfd:
        /// </summary>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">
        /// 1002: Non controls found in the form
        /// </exception>
        public void show()
        {
            show(TBL_UNDER_FORM, "form1");
        }

        public virtual void reset()
        {
            reset("System.Web.UI.HtmlControls.HtmlForm", "form1");
        }

        /// <summary>
        /// <p>
        /// Scrive un messaggio utente semplice (MessageType=0) nella tabella dei log.
        /// Se non riesce a scrivere il messaggio, solleva un'eccezione del tipo Spx_ORMMessage. Tale
        /// eccezione porta in se i seguenti parametri di invocazione:
        /// </p>
        /// <ul>
        /// <li>p_message</li>
        /// <li>p_SourceName</li>
        /// </ul>
        /// <p>
        /// L'eccezione può essere sollevata per i seguenti motivi:
        /// </p>
        /// <ul>
        /// <li>L'oggetto TBL_UNDER_FORM non è istanziato</li>
        /// <li>L'oggetto TBL_UNDER_FORM è istanziato ma non ha una connessione associata.</li>
        /// </ul>
        /// In questi casi è ancora possibile scrivere il messaggio invocando, dunque, l'overload del metodo che consente di
        /// specificare anche la connessione da impiegare.
        /// <pre>
        /// ----
        /// @MdN
        /// Mdfd: 12/07/2015 - impostazione del tracetype al valore 0.
        /// </pre>
        /// </summary>
        /// <param name="p_message">Testo del messaggio</param>
        /// <param name="p_SourceName">Nome del metodo/oggetto/form/breadcrumb originatore del messaggio.</param>
        /// <param name="p_UserID">Id dell'utente (ATTENZIONE: convertire in stringa).</param>
        /// <param name="p_SessionID">Id della sessione utente (ATTENZIONE: convertire in stringa).</param>
        public virtual void writeMessageToLog(String p_message, String p_SourceName, String p_UserID, String p_SessionID)
        {
            Spx_ORMMessage _msg = new Spx_ORMMessage(0);
            _msg.MessageField = p_SourceName;
            _msg.MessageCode = 0;
            _msg.Message = p_message;
            _msg.MessageType = 0;       //@MdN: 12/07/2015 - impostazione del tracetype al valore 0.
            
            if(TBL_UNDER_FORM==null)
                throw _msg;
            if (TBL_UNDER_FORM.CurrentConnection == null)
                throw _msg;
            if (TBL_UNDER_FORM.CurrentConnection.State == System.Data.ConnectionState.Closed)
                TBL_UNDER_FORM.CurrentConnection.Open();
            if(_msg.traceLog(TBL_UNDER_FORM.CurrentConnection, p_UserID, p_SessionID)==false)
                throw _msg;
            TBL_UNDER_FORM.CurrentConnection.Close();
        }

        /// <summary>
        /// <p>
        /// Scrive un messaggio utente semplice (MessageType=0) nella tabella dei log.
        /// Se non riesce a scrivere il messaggio, solleva un'eccezione del tipo Spx_ORMMessage. Tale
        /// eccezione porta in se i seguenti parametri di invocazione:
        /// </p>
        /// <ul>
        /// <li>p_message</li>
        /// <li>p_SourceName</li>
        /// </ul>
        /// <p>
        /// L'eccezione può essere sollevata se la connessione non è istanziata.
        /// </p>
        /// </summary>
        /// <param name="p_message">Testo del messaggio</param>
        /// <param name="p_SourceName">Nome del metodo/oggetto/form/breadcrumb originatore del messaggio.</param>
        /// <param name="p_UserID">Id dell'utente (ATTENZIONE: convertire in stringa).</param>
        /// <param name="p_SessionID">Id della sessione utente (ATTENZIONE: convertire in stringa).</param>
        public virtual void writeMessageToLog(System.Data.Odbc.OdbcConnection p_Conn, String p_message, String p_SourceName, String p_UserID, String p_SessionID)
        {
            Spx_ORMMessage _msg = new Spx_ORMMessage(0);
            _msg.MessageField = p_SourceName;
            _msg.MessageCode = 0;
            _msg.Message = p_message.Replace("'", "''");
            _msg.MessageType = 0;       //@MdN: 12/07/2015 - impostazione del tracetype al valore 0 - 07/09/2015.

            if (p_Conn == null)
                throw _msg;

            if (p_Conn.State == System.Data.ConnectionState.Closed)
                p_Conn.Open();

            if (_msg.traceLog(p_Conn, p_UserID, p_SessionID) == false)
                throw _msg;

            p_Conn.Close();
        }

        /// <summary>
        /// Resets all the controls in the container named as the passed parameter.
        /// @MdN
        /// Crtd: 15/11/2014
        /// Tstd:
        /// Mdfd:
        /// </summary>
        /// <param name="p_container">Name of the cointainer</param>
        /// <param name="p_containerType">Fully specified container type name.</param>
        public void reset(String p_container, String p_containerType)
        {
            Boolean found = false;
            String l_CtrlName = null;
            Control myControl = null;

            // new SQLTable Object
            TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(TBL_UNDER_FORM.CurrentConnection, TBL_UNDER_FORM.Name);
            // clear all controls
            foreach (Control C in this.Controls)
            {
                myControl = this.searchControl(C, p_container, p_containerType);
                if (myControl != null)
                {
                    found = true;
                    break;      //TROVATO
                }
            }
            if(found==false)
                return;

            foreach (Control MyC in myControl.Controls)
            {
                if (MyC == null)
                    continue;
                l_CtrlName = MyC.ID;
                if (l_CtrlName == null)
                    continue;
                        if (MyC.GetType().Name.CompareTo("TextBox") == 0)
                        {
                            ((TextBox)MyC).Text=null;
                        }
                        if (MyC.GetType().Name.CompareTo("Label") == 0)
                        {
                            continue;
                        }
                        if (MyC.GetType().Name.CompareTo("HyperLink") == 0)
                        {
                            ((HyperLink)MyC).Text = null;
                            ((HyperLink)MyC).NavigateUrl = null;
                        }
                        if (MyC.GetType().Name.CompareTo("CheckBox") == 0)
                        {
                            ((CheckBox)MyC).Checked=false;
                        }

                        if (MyC.GetType().Name.CompareTo("Calendar") == 0)
                        {                            
                                ((Calendar)MyC).VisibleDate = DateTime.Now;
                                ((Calendar)MyC).SelectedDate = DateTime.Now;
                        }
                        if (MyC.GetType().Name.CompareTo("DropDownList") == 0)
                        {
                            int myx;
                            foreach (ListItem MyItem in ((DropDownList)MyC).Items)
                            {
                                MyItem.Selected = false;
                            }
                        }
                }// Per ogni controllo            
            }

        /// <summary>
        /// @MdN
        /// Set the name of the table associated with the current form.
        /// Crtd: 24/10/2014
        /// </summary>
        /// <param name="p_TableName">The name of the table.</param>
        public void setTableName(String p_TableName)
        {
            if (p_TableName == null)
            {   // Raise error 1000 if p_TableName is not specified
                simplex_ORM.Spx_ORMMessage ME = new simplex_ORM.Spx_ORMMessage("SimplexForm.setReferencedTable(): the form does not have a table name. " + TableName + " needed.", 1000);
                throw ME;
            }
            TableName = p_TableName;
            
        }

        /// <summary>
        /// @MdN
        /// Set the referenced table.
        /// Crtd: 23/10/2014
        /// </summary>
        /// <param name="p_Table">Table Object that is the referenced table.</param>
        /// <remarks>
        /// Throws Spx_ORMMEssage Exceptions:
        /// 1000:       the form does not have a table name;
        /// 1001:       wrong referenced table
        /// </remarks>
        public void setReferencedTable(simplex_ORM.SQLSrv.SQLTable p_Table)
        {

            if (TableName == null)
            {
                simplex_ORM.Spx_ORMMessage ME = new simplex_ORM.Spx_ORMMessage("SimplexForm.setReferencedTable(): the form does not have a table name. " + TableName + " needed.", 1000);
                throw ME;
            }

            if (p_Table.Name.CompareTo(TableName) != 0)
            {
                simplex_ORM.Spx_ORMMessage ME = new simplex_ORM.Spx_ORMMessage("SimplexForm.setReferencedTable(): wrong referenced table. " + TableName + " needed.", 1001);
                throw ME;
            }
            TBL_UNDER_FORM = p_Table;
        }

        /// <summary>
        /// <p>
        /// Disegna i controlli della maschera in modo dinamico prelevando le informazioni da un 
        /// file xml il cui path viene passato quale parametro. I controlli vengono disegnati in
        /// un placeholder specificato in uno dei parametri passati al metodo.
        /// </p>
        /// <remarks>
        /// Il metodo disegna solo la sezione <b>'main'</b>. Per le altre sezioni (ad esempio i TAB)
        /// si idevono invocare gli opportuni override del metodo implementati nelle classi derivate.
        /// </remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:
        /// Mdfd: 22/08/2017 - Montrotondo - Espansione dei controlli composti
        /// </pre>
        /// </summary>
        /// <param name="p_XML">path del file xml contenente le definizioni dei controlli.</param>
        /// <param name="p_PH">placeholder dove verranno disegnati i controlli.</param>
        /// <param name="p_NumCols">Numero di colonne su cui distribuire i controlli. Se 0, il numero di colonno le prende dall'attributo 'NumCols'
        /// del file di configurazione.</param>
        public virtual void designDynamicForm(String p_XML, System.Web.UI.WebControls.PlaceHolder p_PH, int p_NumCols)
        {

            SIMPLEX_Config.Spx_XmlElement _config = null;
            SIMPLEX_Config.Spx_XmlElement _head = null;
            System.Web.UI.WebControls.Literal _ltrl = null;
            System.Web.UI.WebControls.PlaceHolder _plh = p_PH;

            // per il calcolo della larghezza delle colonne
            // _col3 DEVE essere sempre a 0
            // _col1 DEVE sempre essere il 20% di _col2
            // _col1 + _col2 = 100/p_NumCols
            int _col1 = 0;
            int _col2 = 0;
            int _col3 = 0;

            String _strNumCols = null;
            int _numCols = p_NumCols;            
            int _countCols = 0;                                                             // contatore di colonne
            
            if (p_PH == null)
                return;
            if (p_XML == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML assente!!!</b>.";
                return;
            }
            _config = SIMPLEX_Config.Spx_XmlConfigFile.load(p_XML);
            if (_config == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML non valido!!!</b>.";
                return;
            }
            /* **** **** **** **** **** 
             * Il primo elemento deve essere di tipo 'dynamic'
             * **** **** **** **** **** */
            SIMPLEX_Config.Spx_XmlElement _elem = _config;
            if(_elem.Name.ToLower().CompareTo("dynamic")!=0)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: il primo elemento del file XML NON è 'dynamic'</b>.";
                return;
            }
            _elem = _elem.Inner;

            /* **** **** **** **** **** 
             * Gli elementi del livello immediatamente inferiore alla radice 'dynamic'
             * rappresentano le sezioni di una maschera che sono:
             * Main               - sezione principale (solo una) 
             * Tab                - eventuali Tab
             **** **** **** **** **** */
            #region BLOCCO MAIN
            /* RICERCA DELLA SEZIONE 'main' */
            while (_elem != null)
                if (_elem.Name.ToLower().CompareTo("main") != 0)
                    _elem = _elem.Next;
                else
                    break;
            if (_elem == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: Nel file XML manca l'elemento obbligatorio 'main'.</b>.";
                return;
            }
            
            /* ESPLORAZIONE DEGLI ELEMENTI CONTENUTI IN 'main' */
            _head = _elem;

            /* determinazione del numero di colonne: variabile  _numCols */
            _strNumCols = _head.getValue("NumCols");
            if (_strNumCols == null)
                _numCols = 1;
            else
            {
                if (simplex_ORM.Column.isPureInteger(_strNumCols) == true)
                    _numCols = int.Parse(_strNumCols);
                else
                    _numCols = 1;
            }
            // ... ancora
            if (_numCols == 0)
                _numCols = 1;
            /* OK Determinato _numCols */
            
            /* DETERMINAZIONE DELLA LARGHEZZA DELLE COLONNE */
            int _ratio = (int)Math.Floor((decimal)(100 / _numCols));
            _col2 = (int)Math.Floor((decimal)(_ratio / 5)) * 4;
            _col1 = (int)Math.Floor((decimal)(_ratio / 5));


            /* 
             * LOGICA PER INSERIRE PIU' CONTROLLI IN UNA SOLA CELLA
             * @MdN 19/09/2015
             * ------
             * Si introducono le variabili booleane _continue ed _opened
             * Il valore di _continue è determinato da un attributo dell'elemento XML corrispondente al controllo da posizionare.
             * Il valore true di _opened indica che la cella <TD> è aperta. Il valore di default di _opened è 'false' e commuta in 
             * 'true' una volta che è stato scritto il tag <TD> nel controllo Literal.
             * 
             * 1. Se _opened=false AND _continue=false -> scrivi il <TD> -> commuta _opened=true -> posiziona il controllo.
             * 2. Se _opened=false AND _continue=true -> scrivi il <TD> -> commuta _opened=true -> posiziona il controllo.
             * 3. Se _opened=true AND _continue=true -> posiziona il controllo.
             * 4. Se _opened=true AND _continue=flase -> posiziona il controllo -> scrivi </TD> -> commuta _opened=false.
             * 
             * Il funzionamento di default attiva solo i comportamenti 1. e 4. in uno stesso ciclo.
             * La scrittura di più controlli in una stessa cella non comporta alcun incremento del contatore '_countCols'!!!
             * 
             * La variabile intera _onTheSameCell conta quanti controlli vengono inseriti nella medesima cella.
             */
            Boolean _opened = false;
            Boolean _continue = false;
            int _onTheSameCell = 0;

            /* INIZIO DEL DRAFT DELLA MASCHERA */

            if (_head.InnerElementsCount > 0)
            {
                System.Web.UI.WebControls.Label _lbl = null; ;
                System.Web.UI.WebControls.Literal _ltr = null;
                System.Web.UI.WebControls.WebControl _test = null;
                _ltr = new Literal();
                _ltr.Text = "<table class=\"DYN\">"; //;
                _plh.Controls.Add(_ltr);
                _elem = _head.Inner;                                                //Primo degli elementi di livello inferiore
                while (_elem != null)
                {

                    // Espansione dei controlli composti
                    // @MdN 22/08/2017
                    solveComplexControls(_elem);

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_countCols == 0 && _continue==false)
                    {
                        _ltr = new Literal();
                        _ltr.Text = "<tr class=\"DYN_ROW\">";
                        _plh.Controls.Add(_ltr);
                    }
                    //@MdN 19/09/2015
                    #endregion

                    // colonna 1 -- Label
                    _ltr = new Literal();

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_continue == false && _opened == false)
                    {
                        _ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >";
                        _opened = true;
                    }
                    else
                        _ltr.Text = "&nbsp;";
                    //@MdN 19/09/2015
                    #endregion

                    _plh.Controls.Add(_ltr);
                    // etichetta associata: SOLO SE IL CONTROLLO NON E' A SUA VOLTA UN'ETICHETTA
                    if (_elem.Name.ToLower().CompareTo("lbl") != 0)
                    {
                        _lbl = getAssociatedLabel(_elem);
                        if (_lbl != null)
                            _plh.Controls.Add(_lbl); //JUMP!!!
                    }

                    // colonna 2 -- INSERIMENTO CONTROLLO WEB
                    _ltr = new Literal();
                    
                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    //if ((_continue == false && _opened==true) || _onTheSameCell==0)
                    if ((_continue == false && _opened == true))
                    {
                        _ltr.Text = "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">";
                        //_opened = true;
                    }
                    else
                        _ltr.Text = "&nbsp;";
                    //@MdN 19/09/2015
                    #endregion
                                        
                    _plh.Controls.Add(_ltr);
                    try
                    {
                        System.Web.UI.WebControls.WebControl _debugwb = null;
                        _debugwb = getWebControl(_elem);
                        _plh.Controls.Add(_debugwb);
                        // - eventuale associazione con data source
                        if(_elem.Name.ToLower().CompareTo("sqlddl")==0)
                        {
                            System.Web.UI.WebControls.SqlDataSource _sqlds = getAssociatedSqlDataSource(_elem);
                            if (_sqlds != null)
                                _plh.Controls.Add(_sqlds);
                        }
                        //_plh.Controls.Add(getWebControl(_elem));
                    } catch(simplex_ORM.Spx_ORMMessage ORMM)
                    {
                        MyErrors.Add(ORMM);
                    }
                    // clonna 3 -- chiusura
                    _ltr = new Literal();
                    //_ltr.Text = "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015

                    // Determina se si deve continuare o meno
                    if (_elem.getValue("Continue") != null && _elem.getValue("Continue").ToLower().CompareTo("true") == 0)
                        _continue = true;
                    else
                        _continue = false;

                    if (_continue == false && _opened == true)
                    {
                        _ltr.Text = "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";
                        _opened = false;
                        _onTheSameCell = 0;
                    }
                    else
                    {
                        _onTheSameCell++;
                        _ltr.Text = "&nbsp;";
                    }

                    //@MdN 19/09/2015
                    #endregion
                    _plh.Controls.Add(_ltr);

                    // nuova riga, Sì/No?       //_countCols++;
                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_continue == false)
                        _countCols++;
                    //@MdN 19/09/2015
                    #endregion

                    if (_countCols == _numCols)
                    {
                        //Sì
                        _countCols = 0;
                        _ltr = new Literal();
                        //_ltr.Text = "</tr>";

                        #region @MdN 19/09/2015
                        //@MdN 19/09/2015
                        if (_continue == false)
                            _ltr.Text = "</tr>";
                        else
                            _ltr.Text = "&nbsp;";
                        //@MdN 19/09/2015
                        #endregion
                        
                        _plh.Controls.Add(_ltr);
                        _countCols = 0;
                    }
                    _elem = _elem.Next;
                }// fine ciclo while

                // Si deve gestire la chiusura dell' ultima riga nel caso in cui _countCols < _NumCols
                if (_countCols>0 && _countCols < _numCols)
                {
                    while (_countCols < _numCols)
                    {
                        _ltr = new Literal();
                        _ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >" + "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">" + "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";
                        _plh.Controls.Add(_ltr);
                        _countCols++;
                    }
                    _ltr = new Literal();
                    _ltr.Text = "</tr>";
                    _plh.Controls.Add(_ltr);
                }

                // -- chiusura tabella
                _ltr = new Literal();
                _ltr.Text = "</table>";
                _plh.Controls.Add(_ltr);

            } // blocco principale per 'main'
            #endregion

        }//fine

        /// <summary>
        /// Disegna i controlli della maschera in modo dinamico prelevando le informazioni da un 
        /// file xml il cui path viene passato quale parametro. I controlli vengono disegnati nel
        /// PlaceHolder specificato dal riferimento. Se non viene specificato alcun riferimento 
        /// (null) il PlaceHolder viene individuato nella maschera sulla base dell'ID dell'elemento 
        /// xml &lt;section&gt; passato come parametro (p_SectionId). <br></br>
        /// Ne consegue che in questo caso nella maschera vi deve essere un PlaceHolder che abbia 
        /// nel file XML un corrispondente elemento xml &lt;section&gt;. <br></br>
        /// La corrispondenza è data dall'attributo ID del tag xml con l'ID del controllo PlaceHolder. 
        /// Se la corrispondenza non viene trovata, la sezione non viene disegnata.
        /// <br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/09/2017 - Monterotondo - v.1.0.0.0.
        /// </pre>
        /// </summary>
        /// <param name="p_XML">File XML</param>
        /// <param name="p_PH">Riferimento al PlaceHolder Target (nullable)</param>
        /// <param name="p_NumCols">Numero di incolonnamenti</param>
        /// <param name="p_SectionId">ID della sezione del file XML da rappresentare.</param>
        public virtual void designDynamicForm(String p_XML, System.Web.UI.WebControls.PlaceHolder p_PH, int p_NumCols, String p_SectionId)
        {
            // nuova funzione
            SIMPLEX_Config.Spx_XmlElement _config = null;
            SIMPLEX_Config.Spx_XmlElement _head = null;
            System.Web.UI.WebControls.Literal _ltrl = null;
            System.Web.UI.WebControls.PlaceHolder _plh = p_PH;
            System.Web.UI.WebControls.PlaceHolder _ph = p_PH;
            String _sectionID = null;

            // per il calcolo della larghezza delle colonne
            // _col3 DEVE essere sempre a 0
            // _col1 DEVE sempre essere il 20% di _col2
            // _col1 + _col2 = 100/p_NumCols
            int _col1 = 0;
            int _col2 = 0;
            int _col3 = 0;

            String _strNumCols = null;
            int _numCols = p_NumCols;
            int _countCols = 0;                                                             // contatore di colonne

            //  * Controlli
            // se non viene specificata la sezione, esce.
            if (p_SectionId == null)
                return;

            // se non viene specificato il file XML, esce.
            if (p_XML == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML assente!!!</b>.";
                return;
            }

            // se il file XML non è valido, Esce
            _config = SIMPLEX_Config.Spx_XmlConfigFile.load(p_XML);
            if (_config == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML non valido!!!</b>.";
                return;
            }
            /* **** **** **** **** **** 
             * Il primo elemento deve essere di tipo 'dynamic', se non lo è esce.
             * **** **** **** **** **** */
            SIMPLEX_Config.Spx_XmlElement _elem = _config;
            if (_elem.Name.ToLower().CompareTo("dynamic") != 0)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: il primo elemento del file XML NON è 'dynamic'</b>.";
                return;
            }

            // ** Preparazione
            // Posizionato sul primo elemento
            _elem = _elem.Inner;

            /*
             * La novità introdotta dal presente codice è quella che la procedura NON effettua la ricerta della sezione main
             * ma di una specifica sezione.
             * Le sezioni 'section' hanno i seguenti attributi:
             *  - ID:   caratterizza UNIVOCAMENTE la sezione e DEVE corrispondere con l'ID di un controllo PlaceHolder presente nella maschera.
             *          Nel caso più sezioni abbiano uguale ID, i relativi controlli verranno aggiunti nel medesimo PlaceHolder.
             *          Se il PlaceHolder corrispondente all'ID non esiste, la procedura procede regolarmente senza disegnare la sezione e senza
             *          segnalare errori.
             *  - NumCols: come in 'main'.
             */

            // Se il placeholder NON è specificato, cerca il placeholder che ha l'ID dell'elemento XML specificato
            if (_ph == null)
            {
                //_ph = (System.Web.UI.WebControls.PlaceHolder)this.FindControl(p_SectionId);
                _ph = (System.Web.UI.WebControls.PlaceHolder)this.searchControl(this, p_SectionId, "System.Web.UI.WebControls.PlaceHolder");
                if (_ph == null)
                {
                    /* Il PlaceHolder non esiste? TORNA INDIETRO!!!! */
                    return;
                }
            }

            // Cercare la sezione che ha l'ID specificato
            while (_elem != null)
            {
                if (_elem.Name.ToLower().CompareTo("section") != 0)
                {
                    _elem = _elem.Next;
                    continue;
                }
                // Trovata una sezione!
                _sectionID = _elem.getValue("ID");
                if (_sectionID == null)
                {
                    // NON HA ID? SALTA!
                    _elem = _elem.Next;
                    continue;
                }
                if (_sectionID.CompareTo(p_SectionId) == 0)
                    break;  // trovata la sezione target
                else
                {
                    // continua la ricerca della sezione target
                    _elem = _elem.Next;
                    continue;
                }
            }
            // se la sezione target non è stata trovata, torna indietro.
            if (_elem == null)
                return;

            // *** Esecuzione

            /* il PlaceHolder esiste! --> calcolo il numero di colonne */
            /* determinazione del numero di colonne: variabile  _numCols */
            _strNumCols = _elem.getValue("NumCols");
            if (_strNumCols == null)
                _numCols = 1;
            else
            {
                if (simplex_ORM.Column.isPureInteger(_strNumCols) == true)
                    _numCols = int.Parse(_strNumCols);
                else
                    _numCols = 1;
            }
            // ... ancora
            if (_numCols == 0)
                _numCols = 1;
            /* OK Determinato _numCols */
            /* Discegno la maschera*/
            innerDesignDynamicForm(_elem, _ph, _numCols);
        }

        /// <summary>
        /// Disegna i controlli della maschera in modo dinamico prelevando le informazioni da un 
        /// file xml il cui path viene passato quale parametro. I controlli vengono disegnati nei
        /// diversi PlaceHolder posizionati nella pagina. Il PlaceHolder di default, quello associato 
        /// al tag xml &lt;main&gt;, deve essere specificato nel parametro p_PH.<br></br>
        /// I PlaceHolder associati ai tag xml &lt;section&gt;, invece, vengono individuati mediante la 
        /// corrispondenza dell'atributo ID del tag con l'ID del controllo PlaceHolder. Se la corrispondenza 
        /// per un tag  &lt;section&gt; non viene trovata, la corrispondente sezione non viene disegnata.
        /// <br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 09/09/2017 - Monterotondo - v.1.0.0.0.
        /// Mdfd: 17/09/2017 - Monterotondo - v.1.0.0.0. - corretta la parte di codice che cerca il PlaceHolder corrispondente ad una sezione.
        /// </pre>
        /// </summary>
        /// <param name="p_XML">Path del file XML</param>
        /// <param name="p_PH">Riferimento al PlaceHolder di default</param>
        /// <param name="p_NumCols">Numero diincolonnamenti per i controlli nel placeholder</param>
        /// <param name="p_withSections">Abilita le sezioni: impostare di norma a true. Impostato a False equivale ad invocare l'overload senza questo parametro.</param>
        public virtual void designDynamicForm(String p_XML, System.Web.UI.WebControls.PlaceHolder p_PH, int p_NumCols, Boolean p_withSections)
        {
            // compatibilità all'indietro.
            if (p_withSections == false)
            {
                designDynamicForm(p_XML, p_PH, p_NumCols);
                return;
            }

            // nuova funzione
            SIMPLEX_Config.Spx_XmlElement _config = null;
            SIMPLEX_Config.Spx_XmlElement _head = null;
            System.Web.UI.WebControls.Literal _ltrl = null;
            System.Web.UI.WebControls.PlaceHolder _plh = p_PH;

            // per il calcolo della larghezza delle colonne
            // _col3 DEVE essere sempre a 0
            // _col1 DEVE sempre essere il 20% di _col2
            // _col1 + _col2 = 100/p_NumCols
            int _col1 = 0;
            int _col2 = 0;
            int _col3 = 0;

            String _strNumCols = null;
            int _numCols = p_NumCols;
            int _countCols = 0;                                                             // contatore di colonne

            if (p_PH == null)
                return;
            if (p_XML == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML assente!!!</b>.";
                return;
            }
            _config = SIMPLEX_Config.Spx_XmlConfigFile.load(p_XML);
            if (_config == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML non valido!!!</b>.";
                return;
            }
            /* **** **** **** **** **** 
             * Il primo elemento deve essere di tipo 'dynamic'
             * **** **** **** **** **** */
            SIMPLEX_Config.Spx_XmlElement _elem = _config;
            if (_elem.Name.ToLower().CompareTo("dynamic") != 0)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: il primo elemento del file XML NON è 'dynamic'</b>.";
                return;
            }
            _elem = _elem.Inner;

            /*
             * La novità introdotta dal presente codice è quella che la procedura NON si limita alla ricerta della sezione main
             * ma anche ad altre sezioni.
             * Le sezioni 'section' hanno i seguenti attributi:
             *  - ID:   caratterizza UNIVOCAMENTE la sezione e DEVE corrispondere con l'ID di un controllo PlaceHolder presente nella maschera.
             *          Nel caso più sezioni abbiano uguale ID, i relativi controlli verranno aggiunti nel medesimo PlaceHolder.
             *          Se il PlaceHolder corrispondente all'ID non esiste, la procedura procede regolarmente senza disegnare la sezione e senza
             *          segnalare errori.
             *  - NumCols: come in 'main'.
             */

            #region BLOCCO MAIN

            // RICERCA DEL BLOCCO OBBLIGATORIO MAIN
            while (_elem != null)
                if (_elem.Name.ToLower().CompareTo("main") != 0)
                    _elem = _elem.Next;
                else
                    break;
            if (_elem == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: Nel file XML manca l'elemento obbligatorio 'main'.</b>.";
                return;
            }

            /* ESPLORAZIONE DEGLI ELEMENTI CONTENUTI IN 'main' */
            _head = _elem;

            /* determinazione del numero di colonne: variabile  _numCols */
            _strNumCols = _head.getValue("NumCols");
            if (_strNumCols == null)
                _numCols = 1;
            else
            {
                if (simplex_ORM.Column.isPureInteger(_strNumCols) == true)
                    _numCols = int.Parse(_strNumCols);
                else
                    _numCols = 1;
            }
            // ... ancora
            if (_numCols == 0)
                _numCols = 1;
            /* OK Determinato _numCols */

            // disegno la maschera nella sezione 'main' usando il metodo fattorizzato 'innerDesignDynamicForm()'
            innerDesignDynamicForm(_head, p_PH, _numCols);

            #endregion

            #region BLOCCO ALTRE SEZIONI
            /* ******************************************** */
            // NUOVO: inserimento nelle altre sezioni
            
            String _sectionID = null;
            System.Web.UI.WebControls.PlaceHolder _ph = null;

            _elem = _config.Inner; // ripartiamo dall'inizio
            while (_elem != null)
            {
                if (_elem.Name.ToLower().CompareTo("section") != 0)
                {
                    _elem = _elem.Next;
                    continue;
                }

                // TROVATO!!!
                _sectionID = _elem.getValue("ID");
                if (_sectionID == null)
                {
                    // NON HA ID? SALTA!
                    _elem = _elem.Next;
                    continue;
                }

                // Cercare il placeholder che ha quell'ID
                //_ph = (System.Web.UI.WebControls.PlaceHolder)this.FindControl(_sectionID);
                _ph = (System.Web.UI.WebControls.PlaceHolder)this.searchControl(this, _sectionID, "System.Web.UI.WebControls.PlaceHolder"); //<added @MdN: 17/02/2017>
                if (_ph == null)
                {
                    /* Il PlaceHolder non esiste? SALTA!!!! */
                    _elem = _elem.Next;
                    continue;
                }
                
                /* il PlaceHolder esiste! --> calcolo il numero di colonne */
                /* determinazione del numero di colonne: variabile  _numCols */
                _strNumCols = _elem.getValue("NumCols");
                if (_strNumCols == null)
                    _numCols = 1;
                else
                {
                    if (simplex_ORM.Column.isPureInteger(_strNumCols) == true)
                        _numCols = int.Parse(_strNumCols);
                    else
                        _numCols = 1;
                }
                // ... ancora
                if (_numCols == 0)
                    _numCols = 1;
                /* OK Determinato _numCols */
                /* Discegno la maschera*/
                innerDesignDynamicForm(_elem, _ph, _numCols);

                /* Vado avanti */
                _elem = _elem.Next;

            }// fine ciclo while principale
            #endregion
        }        

        /// <summary>
        /// Riempie di controlli una "sezione" di una maschera.<br></br>
        /// La sezione principale e di default è 'main', le altre sono contenute nel tag "section" file XML.<br></br>
        /// Il metodo nasce per fattorizzazione dal metodo designDynamicForm().<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 09/09/2017 v.1.0.0.0
        /// Tstd: 09-17/09/2017
        /// </pre>
        /// </summary>
        /// <param name="p_XML">elemento del file xml contenente le definizioni dei controlli da inserire nella sezione</param>
        /// <param name="p_PH">riferimento al controllo WebControls.PlaceHolder</param>
        /// <param name="p_NumCols">numero di colonne in cui allineare i controlli web.</param>
        protected virtual void innerDesignDynamicForm(SIMPLEX_Config.Spx_XmlElement p_XML, System.Web.UI.WebControls.PlaceHolder p_PH, int p_NumCols)
        {
            SIMPLEX_Config.Spx_XmlElement _config = null;
            SIMPLEX_Config.Spx_XmlElement _head = null;
            System.Web.UI.WebControls.Literal _ltrl = null;
            System.Web.UI.WebControls.PlaceHolder _plh = p_PH;

            // per il calcolo della larghezza delle colonne
            // _col3 DEVE essere sempre a 0
            // _col1 DEVE sempre essere il 20% di _col2
            // _col1 + _col2 = 100/p_NumCols
            int _col1 = 0;
            int _col2 = 0;
            int _col3 = 0;

            String _strNumCols = null;
            int _numCols = p_NumCols;
            int _countCols = 0;                                                             // contatore di colonne

            if (p_PH == null)
                return;
            if (p_XML == null)
            {
                return;
            }
            _config = p_XML;

            /* **** **** **** **** **** 
             * Il primo elemento deve essere di tipo 'main' o 'section' 
             * **** **** **** **** **** */
            SIMPLEX_Config.Spx_XmlElement _elem = _config;
            if (_elem.Name.ToLower().CompareTo("main") != 0 && _elem.Name.ToLower().CompareTo("section")!=0)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: il primo elemento del file XML NON è 'main' o 'section'</b>.";
                return;
            }

            /* ESPLORAZIONE DEGLI ELEMENTI CONTENUTI IN 'main' o in 'section' */
            _head = _elem;

            /* determinazione del numero di colonne: variabile  _numCols */
            _strNumCols = _head.getValue("NumCols");
            if (_strNumCols == null)
                _numCols = 1;
            else
            {
                if (simplex_ORM.Column.isPureInteger(_strNumCols) == true)
                    _numCols = int.Parse(_strNumCols);
                else
                    _numCols = 1;
            }
            // ... ancora
            if (_numCols == 0)
                _numCols = 1;
            /* OK Determinato _numCols */

            /* DETERMINAZIONE DELLA LARGHEZZA DELLE COLONNE */
            int _ratio = (int)Math.Floor((decimal)(100 / _numCols));
            _col2 = (int)Math.Floor((decimal)(_ratio / 5)) * 4;
            _col1 = (int)Math.Floor((decimal)(_ratio / 5));


            /* 
             * LOGICA PER INSERIRE PIU' CONTROLLI IN UNA SOLA CELLA
             * @MdN 19/09/2015
             * ------
             * Si introducono le variabili booleane _continue ed _opened
             * Il valore di _continue è determinato da un attributo dell'elemento XML corrispondente al controllo da posizionare.
             * Il valore true di _opened indica che la cella <TD> è aperta. Il valore di default di _opened è 'false' e commuta in 
             * 'true' una volta che è stato scritto il tag <TD> nel controllo Literal.
             * 
             * 1. Se _opened=false AND _continue=false -> scrivi il <TD> -> commuta _opened=true -> posiziona il controllo.
             * 2. Se _opened=false AND _continue=true -> scrivi il <TD> -> commuta _opened=true -> posiziona il controllo.
             * 3. Se _opened=true AND _continue=true -> posiziona il controllo.
             * 4. Se _opened=true AND _continue=flase -> posiziona il controllo -> scrivi </TD> -> commuta _opened=false.
             * 
             * Il funzionamento di default attiva solo i comportamenti 1. e 4. in uno stesso ciclo.
             * La scrittura di più controlli in una stessa cella non comporta alcun incremento del contatore '_countCols'!!!
             * 
             * La variabile intera _onTheSameCell conta quanti controlli vengono inseriti nella medesima cella.
             */
            Boolean _opened = false;
            Boolean _continue = false;
            int _onTheSameCell = 0;

            /* INIZIO DEL DRAFT DELLA MASCHERA */

            if (_head.InnerElementsCount > 0)
            {
                System.Web.UI.WebControls.Label _lbl = null; ;
                System.Web.UI.WebControls.Literal _ltr = null;
                System.Web.UI.WebControls.WebControl _test = null;
                _ltr = new Literal();
                _ltr.Text = "<table class=\"DYN\">"; //;
                _plh.Controls.Add(_ltr);
                _elem = _head.Inner;                                                //Primo degli elementi di livello inferiore
                while (_elem != null)
                {

                    // Espansione dei controlli composti
                    // @MdN 22/08/2017
                    solveComplexControls(_elem);

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_countCols == 0 && _continue == false)
                    {
                        _ltr = new Literal();
                        _ltr.Text = "<tr class=\"DYN_ROW\">";
                        _plh.Controls.Add(_ltr);
                    }
                    //@MdN 19/09/2015
                    #endregion

                    // colonna 1 -- Label
                    _ltr = new Literal();

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_continue == false && _opened == false)
                    {
                        _ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >";
                        _opened = true;
                    }
                    else
                        _ltr.Text = "&nbsp;";
                    //@MdN 19/09/2015
                    #endregion

                    _plh.Controls.Add(_ltr);
                    // etichetta associata: SOLO SE IL CONTROLLO NON E' A SUA VOLTA UN'ETICHETTA
                    if (_elem.Name.ToLower().CompareTo("lbl") != 0)
                    {
                        _lbl = getAssociatedLabel(_elem);
                        if (_lbl != null)
                            _plh.Controls.Add(_lbl); //JUMP!!!
                    }

                    // colonna 2 -- INSERIMENTO CONTROLLO WEB
                    _ltr = new Literal();

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    //if ((_continue == false && _opened==true) || _onTheSameCell==0)
                    if ((_continue == false && _opened == true))
                    {
                        _ltr.Text = "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">";
                        //_opened = true;
                    }
                    else
                        _ltr.Text = "&nbsp;";
                    //@MdN 19/09/2015
                    #endregion

                    _plh.Controls.Add(_ltr);
                    try
                    {
                        System.Web.UI.WebControls.WebControl _debugwb = null;
                        _debugwb = getWebControl(_elem);
                        _plh.Controls.Add(_debugwb);
                        // - eventuale associazione con data source
                        if (_elem.Name.ToLower().CompareTo("sqlddl") == 0)
                        {
                            System.Web.UI.WebControls.SqlDataSource _sqlds = getAssociatedSqlDataSource(_elem);
                            if (_sqlds != null)
                                _plh.Controls.Add(_sqlds);
                        }
                        //_plh.Controls.Add(getWebControl(_elem));
                    }
                    catch (simplex_ORM.Spx_ORMMessage ORMM)
                    {
                        MyErrors.Add(ORMM);
                    }
                    // clonna 3 -- chiusura
                    _ltr = new Literal();
                    //_ltr.Text = "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015

                    // Determina se si deve continuare o meno
                    if (_elem.getValue("Continue") != null && _elem.getValue("Continue").ToLower().CompareTo("true") == 0)
                        _continue = true;
                    else
                        _continue = false;

                    if (_continue == false && _opened == true)
                    {
                        _ltr.Text = "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";
                        _opened = false;
                        _onTheSameCell = 0;
                    }
                    else
                    {
                        _onTheSameCell++;
                        _ltr.Text = "&nbsp;";
                    }

                    //@MdN 19/09/2015
                    #endregion

                    _plh.Controls.Add(_ltr);

                    // nuova riga, Sì/No?       //_countCols++;
                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_continue == false)
                        _countCols++;
                    //@MdN 19/09/2015
                    #endregion

                    if (_countCols == _numCols)
                    {
                        //Sì
                        _countCols = 0;
                        _ltr = new Literal();
                        //_ltr.Text = "</tr>";

                        #region @MdN 19/09/2015
                        //@MdN 19/09/2015
                        if (_continue == false)
                            _ltr.Text = "</tr>";
                        else
                            _ltr.Text = "&nbsp;";
                        //@MdN 19/09/2015
                        #endregion

                        _plh.Controls.Add(_ltr);
                        _countCols = 0;
                    }
                    _elem = _elem.Next;
                }// fine ciclo while

                // Si deve gestire la chiusura dell' ultima riga nel caso in cui _countCols < _NumCols
                if (_countCols > 0 && _countCols < _numCols)
                {
                    while (_countCols < _numCols)
                    {
                        _ltr = new Literal();
                        _ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >" + "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">" + "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";
                        _plh.Controls.Add(_ltr);
                        _countCols++;
                    }
                    _ltr = new Literal();
                    _ltr.Text = "</tr>";
                    _plh.Controls.Add(_ltr);
                }

                // -- chiusura tabella
                _ltr = new Literal();
                _ltr.Text = "</table>";
                _plh.Controls.Add(_ltr);

            } // blocco principale per 'main'
        } //fine


        /// <p>
        /// Disegna i controlli della maschera in modo dinamico prelevando le informazioni da un 
        /// file xml il cui path viene passato quale parametro. I controlli vengono disegnati in
        /// un placeholder specificato in uno dei parametri passati al metodo.
        /// </p>
        /// <remarks>
        /// Il metodo disegna solo la sezione <b>'main'</b>. Per le altre sezioni (ad esempio i TAB)
        /// si idevono invocare gli opportuni override del metodo implementati nelle classi derivate.
        /// </remarks>
        /// <p>
        /// Il valore del numero di colonne viene prelevato dal file di configurazione, se esiste
        /// l'attributo <b>'NumCols'</b>, altrimenti il valore di default è '1'.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:
        /// 
        /// </pre>
        /// </summary>
        /// <param name="p_XML">path del file xml contenente le definizioni dei controlli.</param>
        /// <param name="p_PH">placeholder dove verranno disegnati i controlli.</param>
        public virtual void designDynamicForm(String p_XML, System.Web.UI.WebControls.PlaceHolder p_PH)
        {
            this.designDynamicForm(p_XML, p_PH, 0);
        }

        /// <summary>
        /// <p>
        /// Dato un elemento Spx_XmlElement che rappresenta una "sqlddl", restituisce 
        /// il controllo "System.Web.UI.WebControls.SqlDataSource" da aggiungere alla form
        /// e da associare al controllo DropDownList corrispondente all'elemento Spx_XmlElement
        /// passato come parametro. 
        /// </p>
        /// <p>
        /// L'associazione viene fatta automaticamente dal sistema.
        /// </p>
        /// 
        /// <pre>
        /// @MdN
        /// Crtd: 27/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_elem">Elemento Spx_XmlElement di tipo "sqlddl".</param>
        /// <returns>Il controllo SqlDataSource associato alla DropDownList corrispondente all'elemento passato come parametro 
        /// ovvero null nei seguenti due casi:
        /// <ul>
        /// <li>Riferimento all'elemento nullo</li>
        /// <li>L'elemento non è di tipo "sqlddl"</li>
        /// </ul>,
        /// </returns>
        protected System.Web.UI.WebControls.SqlDataSource getAssociatedSqlDataSource(SIMPLEX_Config.Spx_XmlElement p_elem)
        {
            //controlli
            if (p_elem == null)
                return null;
            if (p_elem.Name.ToLower().CompareTo("sqlddl") != 0)
                return null;
            
            //Creazione
            String _xmlConnString=null;
            String _webConnString;
            String _webProviderName;
            System.Configuration.Configuration _webConfig;
            // - CONFIGURAZIONE DELL'ORIGNE DATI
            // - prendo i parametri di configurazione dal file web.config
            _xmlConnString = p_elem.getValue("ConnectionString");
            if (_xmlConnString != null)
            {
                _webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.config");
                if (_webConfig.ConnectionStrings.ConnectionStrings.Count > 0)
                {
                    _webConnString = _webConfig.ConnectionStrings.ConnectionStrings[_xmlConnString].ConnectionString;
                    if (_webConnString == null)
                        throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getAssociatedSqlDataSource(): nel file web.conf non è definita la stringa di connessione '" + _xmlConnString + "'", 1009);
                    _webProviderName = _webConfig.ConnectionStrings.ConnectionStrings[_xmlConnString].ProviderName;
                    if (_webProviderName == null)
                        _webProviderName = "System.Data.SqlClient";
                }
                else
                {
                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getAssociatedSqlDataSource(): nel file web.conf non è definita alcuna stringa di connessione.", 1009);
                }
                /**** VREAZIONE DELL'ORIGINE DATI   ****/
                /**** ATTENZIONE:                       ****
                 * L'ORIGINE DATI DEVE ESSERE CREATA ED AGGIUNTA AL PlaceHolder 
                 * ESTERNAMENTE!!!
                 * ****                                 ****/
                System.Web.UI.WebControls.SqlDataSource _sqlDS = new SqlDataSource(_webProviderName, _webConnString, p_elem.Text);
                _sqlDS.ID = "SQL_" + p_elem.getValue("ID");
                return _sqlDS;
            }
            else
                return null;
            
        }

        /// <summary>
        /// <p>
        /// Restituisce l'etichetta associata al controllo passato attraverso il l'elelento xml
        /// passato per argomento.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 06/04/2015
        /// Mdfd: 25/04/2015
        /// Mdfd: 01/10/2015 BUG: manca controllo sull'esistenza dell'attributo Viisible. Risolto
        /// Mdfd: 02/09/2017 Se l'attributo LableText è diverso da null, anche il tag XML "lbl" introduce una lable in prima colonna.
        /// </pre>
        /// </summary>
        /// <param name="p_xml">Elemento xml corrispondente ad un controllo Web</param>
        /// <returns>un controllo di tipo System.Web.UI.WebControls.Label ovvero null 
        /// se il parametro è un elemento label.
        /// </returns>
        protected virtual System.Web.UI.WebControls.Label getAssociatedLabel(SIMPLEX_Config.Spx_XmlElement p_xml)
        {
            String _val = null;
            // Controlli
            if (p_xml == null)
            {
                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getAssociatedLabel(): Omesso elemento XML", 1007);
            }
            if (getElementCode(p_xml) < 2)
            {

                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getAssociatedLabel(): Elemento XML '" + p_xml.Name + "' ID=\"" + p_xml.getValue("ID") + "\" non valido o in posizione non valida", 1007);
            }
            System.Web.UI.WebControls.Label _toRet = new Label();

            //@MdN 25/04/2015: se il controllo è una lable, si restituisce null
            //@MdN 02/09/2017:  rivedo la precedente decisione. Si restituisce null solo se "non è definito" l'attributo LableText.
            //                  Utile per mettere un'etichetta in prima colonna (ovviamente il corpo dell'elemento XML deve essere vuoto).
            //if (p_xml.Name.ToLower().CompareTo("lbl") == 0)
            if (p_xml.Name.ToLower().CompareTo("lbl") == 0 && p_xml.getValue("LableText")==null) //02/09/2017
                return null;

            _val = p_xml.getValue("LableText");
            if (_val == null)
                _toRet.Text = p_xml.Name;
            else
                _toRet.Text = _val;

            _toRet.ID = "L_" + p_xml.getValue("ID");
            _toRet.AssociatedControlID = p_xml.getValue("ID");
            _toRet.CssClass = "lbl_" + p_xml.getValue("CssClass");
            //@MdN 01/10/2015 BUG: manca controllo sull'esistenza dell'attributo Viisible. Risolto
            if (p_xml.getValue("Visible") != null && p_xml.getValue("Visible").Length > 0)
                _toRet.Visible = bool.Parse(p_xml.getValue("Visible"));
            else
                _toRet.Visible = true;
            return _toRet;
        }

        /// <summary>
        /// Questo metodo protetto consente di trasformare un "controllo complesso" in una opportuna sequenza di controlli web.<br></br>
        /// Un <b>controllo complesso</b> è un controllo il cui funzionamento necessita di altri controlli. Ne è un esempio il controllo <b>TextBox</b> quando è attiva l'opzione "calendar=true". In 
        /// questo caso il metodo accoda al controllo TextBox un controllo LinkButton chiamato NomeTextBox_LNK ed un controllo Calendar chiamato NomeTextBox_Calendar.
        /// <br></br>
        /// <strong>MODIFICA STRUTTURA DI ELEMENTI XML.</strong>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/08/2017 - Monterotondo - txt con attributo calendar
        /// Mdfd: 02/09/2017 - Monterotondo - corrzione BUG
        /// 
        /// </pre>
        /// 
        /// </summary>
        /// <param name="p_xml">elemento del file xml o struttura di elementi XML</param>
        protected virtual void solveComplexControls(SIMPLEX_Config.Spx_XmlElement p_xml)
        {

            String _ID = null;

            // soliti controlli
            if (p_xml == null)
            {
                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.resolveComplexControls(): Omesso elemento XML", 1007);
            }
            int _code = getElementCode(p_xml);
            // controlli preliminari 
            if (_code == -1)
                return;
            if (_code == -2)
            {
                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.resolveComplexControls(): " + p_xml.Name + " non riconosciuto.", 1007);
            }

            if(p_xml.getValue("ID")==null)
                return;
            else
                _ID = p_xml.getValue("ID");

            switch (_code)
            {
                case 4:
                    {/* TXT */
                        /*
                         * Inserimento dell'attributo calendar o Calendar.
                         * Trucchettino: se nelle proprietà del tag XML si trova "calendar" o "Calendar" su aggiungono un LinkButton ed un Calendar nella struttura XML di supporto.
                         */
                        if ((p_xml.getValue("Calendar") != null && p_xml.getValue("Calendar").ToLower().CompareTo("true")==0)|| (p_xml.getValue("calendar") != null && p_xml.getValue("calendar").ToLower().CompareTo("true")==0))
                        {
                            // Impostiamo l'attributo Continue
                            String _oldval = null;
                            _oldval = p_xml.getValue("Continue");
                            //<mdfd @MdN: 02/09/2017>
                            if (p_xml.getValue("Continue") != null)     
                                p_xml.setAttribute("Continue", "true");
                            else
                                p_xml.addAttribute("Continue", "true");
                            //</mdfd>

                            // creazione dell'elemento LinkButton
                            SIMPLEX_Config.Spx_XmlElement _xml = null;
                            _xml = p_xml.appendElementAfter("lnk", MyLnkExpand, 0);
                            _xml.addAttribute("ID", _ID + "_LNK");
                            _xml.addAttribute("OnClick", "TextBoxCalendar_DefaultClick");
                            _xml.addAttribute("Continue", "true");
                            _xml.addAttribute("LableText", ""); //Importante, altrimenti inserisce un'etichetta.


                            // Creazione dell'elemento Calendar
                            _xml = p_xml.appendElementAfter("calendar", "", 1);
                            _xml.addAttribute("OnSelectionChanged", "TextBoxCalendar_DefaultSelectionChanged");
                            _xml.addAttribute("ID", _ID + "_Calendar");
                            _xml.addAttribute("Visible", "false");
                            _xml.addAttribute("LableText", ""); //Importante, altrimenti inserisce un'etichetta.
                            // TEST IF Continue: si guarda che valore aveva la proprietà Continue del tag "txt"
                            //if(p_xml.getValue("Continue")!= null && p_xml.getValue("Continue")!= "true")
                            if (_oldval != null && _oldval.CompareTo("true")==0)
                            {
                                p_xml.setAttribute("Continue","false");
                                _xml.addAttribute("Continue", "true");
                            }                            
                        }
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        /// <summary>
        /// <p>
        /// Dato un elemento del file XML, restituisce il corrispondente controllo.
        /// <strong>ATTENZIONE: se l'elemento non viene riconosciuto come un controllo, lancia un
        /// un errore di tipo Spx_ORMMessage con codice 1007.</strong>
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Mdfd: 01/10/2015 BUG: sostituito questo test 'if (_chk != null)'
        /// Mdfd: 23/10/2015 BUG: sostituito questo test 'if (_chk != null)' --> 1.1.0.1
        /// </pre>
        /// </summary>
        /// <param name="p_xml">elemento del file xml</param>
        /// <returns>il controllo corrispondente all'elemento oppure null se l'elemento 
        /// viene riconosciuto ma non corrisponde ad un controllo.
        /// </returns>
        protected virtual System.Web.UI.WebControls.WebControl getWebControl(SIMPLEX_Config.Spx_XmlElement p_xml)
        {
            if(p_xml==null)
            {
                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): Omesso elemento XML", 1007);
            }
            int _code = getElementCode(p_xml);
            // controlli preliminari 
            if (_code == -1)
                return null;
            if (_code == -2)
            {
                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): " + p_xml.Name + " non riconosciuto.", 1007);
            }
            switch (_code)
            {
                case 0:
                case 1:
                    {
                        return null;
                    }
                case 2:
                    { /* DDL */
                        System.Web.UI.WebControls.DropDownList _ddl = new System.Web.UI.WebControls.DropDownList();
                        String _val = null;
                        int _idxvalue = 0;
                        int _idxtext = 0;
                        String[] _separator = {","};
                        String _indexChangedEvent = null;
                        String _text = null;
                        String[] _splittedText = null;

                        // ID
                        _val = p_xml.getValue("ID");
                        if (_val != null)
                            _ddl.ID = _val;
                        // Visible
                        _val = p_xml.getValue("Visible");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _ddl.Visible = true;
                            else if(_val.ToLower().CompareTo("false") == 0)
                                _ddl.Visible = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Visible' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008); 
                        }
                        //CssClass
                        _val = p_xml.getValue("CssClass");
                        if (_val != null)
                            _ddl.CssClass = _val;
                        //Enabled
                        _val = p_xml.getValue("Enabled");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _ddl.Enabled  = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _ddl.Enabled = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Enabled' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //width
                        _val = p_xml.getValue("width");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _ddl.Width = Unit.Pixel(Int32.Parse(_val));
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'width' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        //DataValueField
                        _val = p_xml.getValue("DataValueField");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _idxvalue = Int32.Parse(_val);
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'DataValueField' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        else
                            _idxvalue = 0;
                        //DataTextField
                        _val = p_xml.getValue("DataTextField");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _idxtext = Int32.Parse(_val);
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'DataTextField' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        else
                            _idxtext = 1;
                        //Separator
                        _val = p_xml.getValue("Separator");
                        if (_val != null)
                            _separator[0] = _val;
                        //onselectedindexchanged
                        _val = p_xml.getValue("onselectedindexchanged");
                        if (_val != null)
                        {
                            // USO 'REFLECTION' PER ASSOCIARE IL DELEGATO (CHE DEVE ESISTERE)
                            // Prima di tutto si deve verificare che il metodo da invocare esista!!!
                            if (this.GetType().GetMethod(_val, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic) != null)
                            {
                                // OK esiste!
                                    _ddl.AutoPostBack = true;
                                    _ddl.SelectedIndexChanged += (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), this, _val, false, true);
                               
                            }// fine reflection
                        }
                        
                        _text = p_xml.Text;
                        
                        /* ***** ***** ***** ***** ***** ***** */
                        /* CREAZIONE DELLE VOCI DEL CONTROLLO  */
                        /* ***** ***** ***** ***** ***** ***** */
                        int _t = 0;
                        int _s = 0;
                        int _base = 0;
                        ListItem _li = null;
                        _splittedText = _text.Split(_separator, StringSplitOptions.None);
                        while(_base<_splittedText.Count())
                        {
                            _li = new ListItem();
                            _li.Text = _splittedText[_base + _idxtext];
                            _li.Value = _splittedText[_base + _idxvalue];
                            _base = _base + _idxtext + 1;
                            _ddl.Items.Add(_li);
                        }

                        //SelectedIndex
                        //@MdN: 26/04/2015
                        _val = p_xml.getValue("SelectedIndex");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _ddl.SelectedIndex = Int32.Parse(_val);
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'widthSelectedIndex' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);


                        if (_indexChangedEvent != null)
                            _ddl.SelectedIndexChanged += new EventHandler(DynamicIndexChanged);
                        
                        return _ddl;                       
                    } // fine DDL
                case 3:
                    {
                        //SQLDDL
                        System.Web.UI.WebControls.DropDownList _ddl = new System.Web.UI.WebControls.DropDownList();
                        String _val = null;                        
                        String _indexChangedEvent = null;
                        String _text = null;
                        String _ConnStringName = null;
                        
                        // ID
                        _val = p_xml.getValue("ID");
                        if (_val != null)
                            _ddl.ID = _val;
                        // Visible
                        _val = p_xml.getValue("Visible");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _ddl.Visible = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _ddl.Visible = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Visible' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //CssClass
                        _val = p_xml.getValue("CssClass");
                        if (_val != null)
                            _ddl.CssClass = _val;
                        //Enabled
                        _val = p_xml.getValue("Enabled");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _ddl.Enabled = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _ddl.Enabled = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Enabled' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //width
                        _val = p_xml.getValue("width");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _ddl.Width = Unit.Pixel(Int32.Parse(_val));
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'width' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        //DataValueField
                        _val = p_xml.getValue("DataValueField");
                        if (_val != null)
                            _ddl.DataValueField = _val;
                        else
                            _ddl.DataTextField = "0";
                        //DataTextField
                        _val = p_xml.getValue("DataTextField");
                        if (_val != null)
                            _ddl.DataTextField = _val;
                        else
                            _ddl.DataTextField = "1";
                        //onselectedindexchanged
                        _val = p_xml.getValue("onselectedindexchanged");
                        if (_val != null)
                        {
                            // USO 'REFLECTION' PER ASSOCIARE IL DELEGATO (CHE DEVE ESISTERE)
                            // Prima di tutto si deve verificare che il metodo da invocare esista!!!
                            if (this.GetType().GetMethod(_val, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic) != null)
                            {
                                // OK esiste!
                                    _ddl.AutoPostBack = true;
                                    _ddl.SelectedIndexChanged += (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), this, _val, false, true);
                               
                            }// fine reflection
                        }
                        //SelectedIndex
                        _val = p_xml.getValue("SelectedIndex");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _ddl.Width = Int32.Parse(_val);
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'widthSelectedIndex' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);

                        //ConnectionString
                        _val = p_xml.getValue("ConnectionString");
                        if (_val != null)
                            _ConnStringName = _val;
                        else
                            throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): per i controlli sqlddl è necessario il parametro ConnectioinString.", 1009);

                        //NullItem
                        _val = p_xml.getValue("NullItem");
                        if (_val != null)
                        {
                            ListItem _li=null;
                            if(_val.Length==0)
                                _li = new ListItem(null, null);
                            else
                                _li = new ListItem(_val, null);

                            _ddl.Items.Clear();
                            _ddl.Items.Add(_li);
                            _ddl.AppendDataBoundItems = true;
                        }
                                                
                        //Query SQL.
                        _text = p_xml.Text;
                        _ddl.DataSourceID = "SQL_" + _ddl.ID;
                        return _ddl;

                    } //fine SQLDDL
                case 4:
                    {/* TXT */
                        /*
                         * Inserimento dell'attributo calendar o Calendar.
                         * Trucchettino: se nelle proprietà del tag XML si trova "calendar" o "Calendar"
                         */
                        // TODO:

                        System.Web.UI.WebControls.TextBox _txt = new TextBox();
                        String _val = null;

                        // ID
                        _val = p_xml.getValue("ID");
                        if (_val != null)
                            _txt.ID = _val;
                        // Visible
                        _val = p_xml.getValue("Visible");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _txt.Visible = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _txt.Visible = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Visible' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //CssClass
                        _val = p_xml.getValue("CssClass");
                        if (_val != null)
                            _txt.CssClass = _val;
                        //Readonly
                        _val = p_xml.getValue("Readonly");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _txt.ReadOnly= true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _txt.ReadOnly= false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Enabled' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);                            
                        }
                        //Enabled
                        _val = p_xml.getValue("Enabled");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _txt.Enabled = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _txt.Enabled = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Enabled' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //width
                        _val = p_xml.getValue("width");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _txt.Width = Unit.Pixel(Int32.Parse(_val));
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'width' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        //heigh
                        _val = p_xml.getValue("height");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _txt.Height  = Unit.Pixel(Int32.Parse(_val));
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'height' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);

                        //TextMode
                        _val = p_xml.getValue("TextMode");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("Password".ToLower()) == 0)
                                _txt.TextMode = TextBoxMode.Password;
                            else if (_val.ToLower().CompareTo("MultiLine".ToLower()) == 0)
                                _txt.TextMode = TextBoxMode.MultiLine;
                            else
                                _txt.TextMode = TextBoxMode.SingleLine;
                        }
                        else
                            _txt.TextMode = TextBoxMode.SingleLine;

                        //rows
                        _val = p_xml.getValue("rows");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                if (_txt.TextMode == TextBoxMode.MultiLine)
                                    _txt.Rows = Int32.Parse(_val);
                                else
                                    _txt.Rows = 1;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'rows' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        
                        //MaxLength - @MdN 28/09/2015
                        _val = p_xml.getValue("MaxLength");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _txt.MaxLength = Int32.Parse(_val);
                            else
                                _txt.MaxLength = 32;
                        //else
                        //    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'MaxLength' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);

                        if(p_xml.Text != null)
                            _txt.Text = p_xml.Text; // valore di default

                        return _txt;
                    } //fine txt
                case 5:
                    {
                        /* lbl */
                        System.Web.UI.WebControls.Label _lbl = new Label();
                        String _val = null;

                        _val = p_xml.getValue("ID");
                        if (_val != null)
                            _lbl.ID = _val;
                        // Visible
                        _val = p_xml.getValue("Visible");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _lbl.Visible = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _lbl.Visible = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Visible' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //CssClass
                        _val = p_xml.getValue("CssClass");
                        if (_val != null)
                            _lbl.CssClass = _val;
                        //Enabled
                        _val = p_xml.getValue("Enabled");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _lbl.Enabled = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _lbl.Enabled = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Enabled' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //width
                        _val = p_xml.getValue("width");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _lbl.Width = Unit.Pixel(Int32.Parse(_val));
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'width' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        //heigh
                        _val = p_xml.getValue("height");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _lbl.Height = Unit.Pixel(Int32.Parse(_val));
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'height' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);

                        _lbl.Text = p_xml.Text;

                        return _lbl;
                    }
                case 6:
                    {
                        /* chk */
                        System.Web.UI.WebControls.CheckBox _chk = new CheckBox();
                        String _val = null;

                        _val = p_xml.getValue("ID");
                        if (_val != null)
                            _chk.ID = _val;
                        // Visible
                        _val = p_xml.getValue("Visible");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _chk.Visible = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _chk.Visible = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Visible' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //CssClass
                        _val = p_xml.getValue("CssClass");
                        if (_val != null)
                            _chk.CssClass = _val;
                        //Enabled
                        _val = p_xml.getValue("Enabled");
                        if (_val != null) //@MdN 01/10/2015 BUG: sostituito questo test if (_chk != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _chk.Enabled = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _chk.Enabled = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Enabled' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        //width
                        _val = p_xml.getValue("width");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _chk.Width = Unit.Pixel(Int32.Parse(_val));
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'width' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        //heigh
                        _val = p_xml.getValue("height");
                        if (_val != null)
                            if (simplex_ORM.Column.isPureInteger(_val) == true)
                                _chk.Height= Unit.Pixel(Int32.Parse(_val));
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'height' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);

                        //Checked //@MdN 28/04/2015 - @MdN corretto bug 23/10/2015 sostituito if(_chk!=null)
                        _val = p_xml.getValue("Checked");
                        if (_val != null)
                        {
                            if (_val.ToLower().CompareTo("true") == 0)
                                _chk.Checked = true;
                            else if (_val.ToLower().CompareTo("false") == 0)
                                _chk.Checked = false;
                            else
                                throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Checked' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                        }
                        
                        _chk.Text = p_xml.Text;
 
                        return _chk; 
                    }
                case 7:
                    {
                        // calendar
                        if (p_xml.getValue("ID") != null)
                        {
                            String _val = null;

                            System.Web.UI.WebControls.Calendar _clndr = new Calendar();
                            _clndr.ID = p_xml.getValue("ID");

                            // Visible
                            _val = p_xml.getValue("Visible");
                            if (_val != null && _val.ToLower().CompareTo("false") == 0)
                                _clndr.Visible = false;
                            else
                                _clndr.Visible = true;

                            // Enabled
                            _val = p_xml.getValue("Enabled");
                            if (_val != null && _val.ToLower().CompareTo("false") == 0)
                                _clndr.Enabled = false;
                            else
                                _clndr.Enabled = true;

                            // associazione dell'evento SelectionChanged
                            _val = p_xml.getValue("OnSelectionChanged");
                            if (_val != null)
                            {
                                // USO 'REFLECTION' PER ASSOCIARE IL DELEGATO (CHE DEVE ESISTERE)
                                // Prima di tutto si deve verificare che il metodo da invocare esista!!!
                                if (this.GetType().GetMethod(_val, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic) != null)
                                {
                                    // OK esiste!
                                    _clndr.SelectionChanged += (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), this, _val, false, true);
                                    //_ddl.SelectedIndexChanged += (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), this, _val, false, true);

                                }// fine reflection
                            }

                            // CssClass
                            _val = p_xml.getValue("CssClass");
                            if (_val != null)
                                _clndr.CssClass = _val;

                            //width
                            _val = p_xml.getValue("width");
                            if (_val != null)
                                if (simplex_ORM.Column.isPureInteger(_val) == true)
                                    _clndr.Width = Unit.Pixel(Int32.Parse(_val));
                                else
                                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'width' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                            //heigh
                            _val = p_xml.getValue("height");
                            if (_val != null)
                                if (simplex_ORM.Column.isPureInteger(_val) == true)
                                    _clndr.Height = Unit.Pixel(Int32.Parse(_val));
                                else
                                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'height' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);

                            return _clndr;
                        }
                        break;
                    }
                case 8:
                    {
                        //lnk
                        if (p_xml.getValue("ID") != null)
                        {
                            String _val = null;
                            System.Web.UI.WebControls.LinkButton _lnk = new LinkButton();
                            _lnk.ID = p_xml.getValue("ID");

                            // *** Analisi degli attributi ***
                            // Visible
                            _val = p_xml.getValue("Visible");
                            if (_val != null)
                            {
                                if (_val.ToLower().CompareTo("true") == 0)
                                    _lnk.Visible = true;
                                else if (_val.ToLower().CompareTo("false") == 0)
                                    _lnk.Visible = false;
                                else
                                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Visible' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                            }
                            
                            //CssClass
                            _val = p_xml.getValue("CssClass");
                            if (_val != null)
                                _lnk.CssClass = _val;

                            //Enabled
                            _val = p_xml.getValue("Enabled");
                            if (_val != null)
                            {
                                if (_val.ToLower().CompareTo("true") == 0)
                                    _lnk.Enabled = true;
                                else if (_val.ToLower().CompareTo("false") == 0)
                                    _lnk.Enabled = false;
                                else
                                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Enabled' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                            }

                            // associazione dell'evento OnClick
                            _val = p_xml.getValue("OnClick");
                            if (_val != null)
                            {
                                // USO 'REFLECTION' PER ASSOCIARE IL DELEGATO (CHE DEVE ESISTERE)
                                // Prima di tutto si deve verificare che il metodo da invocare esista!!!
                                if (this.GetType().GetMethod(_val, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic) != null)
                                {
                                    // OK esiste!
                                    _lnk.Click += (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), this, _val, false, true);
                                }// fine reflection
                            } else
                            {
                                // associazione dell'evento di default;
                                _lnk.Click += new EventHandler(generic_Click);
                            }
                            _lnk.Text = p_xml.Text;
                            return _lnk;
                        }
                        break;
                    }
                case 9:
                    {
                        //btn
                        if (p_xml.getValue("ID") != null)
                        {
                            String _val = null;
                            System.Web.UI.WebControls.Button _btn = new Button();
                            _btn.ID = p_xml.getValue("ID");

                            // *** Analisi degli attributi ***
                            //Text
                            if (p_xml.Text != null)
                                _btn.Text = p_xml.Text;
                            else
                                _btn.Text = " ... ";

                            // Visible
                            _val = p_xml.getValue("Visible");
                            if (_val != null)
                            {
                                if (_val.ToLower().CompareTo("true") == 0)
                                    _btn.Visible = true;
                                else if (_val.ToLower().CompareTo("false") == 0)
                                    _btn.Visible = false;
                                else
                                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Visible' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                            }

                            //CssClass
                            _val = p_xml.getValue("CssClass");
                            if (_val != null)
                                _btn.CssClass = _val;

                            //Enabled
                            _val = p_xml.getValue("Enabled");
                            if (_val != null)
                            {
                                if (_val.ToLower().CompareTo("true") == 0)
                                    _btn.Enabled = true;
                                else if (_val.ToLower().CompareTo("false") == 0)
                                    _btn.Enabled = false;
                                else
                                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'Enabled' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                            }

                            // associazione dell'evento OnClick
                            _val = p_xml.getValue("OnClick");
                            if (_val != null)
                            {
                                // USO 'REFLECTION' PER ASSOCIARE IL DELEGATO (CHE DEVE ESISTERE)
                                // Prima di tutto si deve verificare che il metodo da invocare esista!!!
                                if (this.GetType().GetMethod(_val, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic) != null)
                                {
                                    // OK esiste!
                                    _btn.Click += (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), this, _val, false, true);
                                }// fine reflection
                            }
                            else
                            {
                                // associazione dell'evento di default;
                                _btn.Click += new EventHandler(generic_Click);
                            }

                            //width
                            _val = p_xml.getValue("width");
                            if (_val != null)
                                if (simplex_ORM.Column.isPureInteger(_val) == true)
                                    _btn.Width = Unit.Pixel(Int32.Parse(_val));
                                else
                                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'width' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);
                            //heigh
                            _val = p_xml.getValue("height");
                            if (_val != null)
                                if (simplex_ORM.Column.isPureInteger(_val) == true)
                                    _btn.Height = Unit.Pixel(Int32.Parse(_val));
                                else
                                    throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'attributo 'height' ha il valore '" + _val + "' che non è valido per l'elemento " + p_xml.Name, 1008);

                            _btn.Text = p_xml.Text;
                            return _btn;

                        }
                        break;
                    }
                default:
                    {
                        throw new simplex_ORM.Spx_ORMMessage("SimplexForm.getWebControl(): l'elemento '" + p_xml.Name + " non è valido." , 1007);
                    }

            }//fine switch
            return null;
        } //fine

        protected virtual void DynamicIndexChanged(Object sender, EventArgs e)
        {
            //NOP
        }

        /// <summary>
        /// <p>
        /// Restituisce il codice dell'indice dell'elemento p_elem all'interno del vettore statico
        /// Elements.
        /// </p>
        /// </summary>
        /// <param name="p_elem"></param>
        /// <returns>
        /// Il codice del controllo come indice (base 0) del vettore Elements ovvero:
        /// <ul>
        /// <li>-1: se p_elem è vuoto</li>
        /// <li>-2: se p_elem non viene riconosciuto.</li>
        /// </ul>
        /// </returns>
        protected virtual int getElementCode(SIMPLEX_Config.Spx_XmlElement p_elem)
        {
            int t=0;
            //controlli
            if (p_elem == null)
                return -1;
            for (t = 0; t < SimplexForm.Elements.Count(); t++)
            {
                if (Elements[t].ToLower().CompareTo(p_elem.Name) == 0)
                    return t;
            }
            return -2;
        }

        /// <summary>
        /// <p>
        /// Imposta una voce di default in testa alle Drop Down List (DDL) ed esegue il DataBinding.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/03/2015
        /// Mdfd: 11/04/2015 - 12/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_DDL">Riferimento al controllo DropDownList.</param>
        /// <param name="p_VoceNulla">Valore da visualizzare come voce nulla. 
        /// Se ha valore 'null' allora la voce nulla è la stringa "Seleziona". 
        /// </param>
        /// <param name="p_SessionVarName">Conserva il valore di default in una variabile di sessione il cui nome è passato  
        /// come stringa.
        /// </param>
        /// <param name="p_ValoreNullo">E' il valore di default per la voce nulla. Può essere null.</param>
        protected void impostaVoceNullaDDL(System.Web.UI.WebControls.DropDownList p_DDL,String p_VoceNulla, String p_SessionVarName, String p_ValoreNullo)
        {
            String _VoceNulla=null;
            // Controllo
            if (p_DDL == null)
                return;
            if (p_VoceNulla != null)
                _VoceNulla = p_VoceNulla;

            ListItem _li = new ListItem(_VoceNulla, p_ValoreNullo);
            p_DDL.Items.Clear();
            p_DDL.Items.Add(_li);
            p_DDL.AppendDataBoundItems = true;
            p_DDL.DataBind();
            p_DDL.SelectedIndex = 0;
            if(p_SessionVarName!=null)
                Session[p_SessionVarName] = p_ValoreNullo;
        }//fine

        /// <summary>
        /// <p>
        /// Imposta una voce di default in testa alle Drop Down List (DDL) ed esegue il DataBinding.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/03/2015
        /// Mdfd: 11/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_DDL">Riferimento al controllo DropDownList.</param>
        /// <param name="p_VoceNulla">Valore da visualizzare come voce nulla. Se null la voce nulla è la stringa "Seleziona". 
        /// Il valore di default è null.
        /// </param>
        protected void impostaVoceNullaDDL(System.Web.UI.WebControls.DropDownList p_DDL, String p_VoceNulla)
        {
            impostaVoceNullaDDL(p_DDL, p_VoceNulla, null, null);
        }//fine

        /// <summary>
        /// <p>
        /// Imposta una voce di default 'seleziona' in testa alle Drop Down List (DDL) ed esegue il DataBinding.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/03/2015
        /// Mdfd: 11/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_DDL">Riferimento al controllo DropDownList.</param>
        protected void impostaVoceNullaDDL(System.Web.UI.WebControls.DropDownList p_DDL)
        {
            impostaVoceNullaDDL(p_DDL, null, null, null);
        }//fine
        
        public SIMPLEX_Config.Spx_XmlElement getControlsFromTable(simplex_ORM.SQLSrv.SQLTable p_Table, String p_ConnStrName)
        {
            Spx_XmlElement _toRet = null;
            Spx_XmlElement _current = null;
            Spx_XmlElement _elem = null;
            String _connName = p_ConnStrName;
            

            //controlli
            if (p_Table == null)
                return null;

            // Creazione della root
            _toRet = Spx_XmlElement.createRoot("dynamic");
            if (_toRet == null)
                return null;

            // Creazione di main
            _current = _toRet.insertElement("main", null);
            if (_current == null)
                return null;
            _current.addAttribute("numcols", "1");

            
            // Stringa di connessione
            if (_connName == null)
                // Se non specificata cerco di ricavarla
                _connName = guessOLEDBConnectionStringName(p_Table.CurrentConnection);
            
            // se dopo tutto ancora non ho la stringa di connessione, ogni 
            // controllo verrà trasformato in una TextBox

            /* **** PARTE PRINCIPALE *************************************
             * 
             * Si controlla se ogni colonna della tabella è soggetta a
             * vincolo di integrità referenziale.
             * In caso affermativo, si aggiunge un elemento sqlddl.
             * In case negativo, se il controllo è di tipo BIT allora si 
             * aggiunge un elemento chk, altrimenti un elemento txt.
             * *********************************************************** */
            simplex_ORM.SQLSrv.SQLForeignKey _fkey = null;
            simplex_ORM.SQLSrv.SQLTable _tempTbl = null;
            System.Text.StringBuilder _sb = null;

            foreach (String _column in p_Table.SQLColumns)
            {
                // 1 - SE esiste un vincolo di integrità referenziale
                _fkey = p_Table.getForeignKey(_column);
                
                // **** SQLDDL ****
                if (_fkey != null && _fkey.ColumnsCount==1)
                {
                    // ---- ABBIAMO UNA LOOKUP --> aggiungo un elemento sqlddl ---

                    //sqlddl: costruzione della query per il contenuto
                    Boolean _concat = false;
                    Boolean _zeroFields = true;
                    _tempTbl = _fkey.ReferencedTable;
                    if (_tempTbl != null)
                    {
                        _sb = new StringBuilder("SELECT ");
                        _sb.Append(_fkey.ReferencedColumns[0]).Append(", ");
                        
                        //sqlddl: costruzione della query per il contenuto --> concatena tutti i campi 'testuali' della tabella referenziata.
                        foreach (String _rcolumn in _tempTbl.SQLColumns)
                        {
                            if (_tempTbl.isTextField(_rcolumn) == true)
                            {
                                _zeroFields = false;
                                if (_concat == true)
                                    _sb.Append(" + ' - ' + ");

                                _sb.Append(_rcolumn);
                                _concat = true;
                            }                               
                        }
                        if (_zeroFields == true)
                        {
                            // Non ho trovato na mazza!!!
                            _sb.Append("' - vuoto - '");
                        }
                        _sb.Append(" AS Descr FROM ");
                        _sb.Append(_tempTbl.Name);
                    }
                    _elem = _current.insertElement("sqlddl", _sb.ToString());
                    _elem.addAttribute("ID", _column);
                    _elem.addAttribute("visible", "true");
                    _elem.addAttribute("width", "300");                         // 300 è il valore di default
                    _elem.addAttribute("DataValueField", _fkey.ReferencedColumns[0]);
                    _elem.addAttribute("DataTextField", "Descr");
                    _elem.addAttribute("LableText", _column);
                    _elem.addAttribute("ConnectionString", _connName);
                    continue;
                } // FINE SQLDDL
                
                /* CHECKBOX */
                if (p_Table.isBitField(_column) == true)
                {
                    _elem = _current.insertElement("chk", null);
                    _elem.addAttribute("ID", _column);
                    _elem.addAttribute("visible", "true");
                    _elem.addAttribute("LableText", _column);
                    _elem.addAttribute("checked", "false");
                    continue;
                }
                /* TEXTBOX - CASO DI DEFAULT */
                _elem = _current.insertElement("txt", null);
                _elem.addAttribute("ID", _column);
                _elem.addAttribute("visible", "true");
                _elem.addAttribute("DataTextField", "Descr");
                _elem.addAttribute("LableText", _column);
                _elem.addAttribute("width", "300");                         // 300 è il valore di default                
            }//FINE PARTE PRINCIPALE    
            return _toRet;
        }//fine

        /// <summary>
        /// <p>Restituisce il <b>nome</b> di una stringa di connessione corrispondente ad una connessione esistente.</p>
        /// <b>IT</b>: 
        /// <p>
        /// Questa funzione cerca di <b>scoprire</b>, se esiste nel file <i>web.config</i>, 
        /// una stringa di connessione (<i>Connection String</i>) che permetta di connettersi 
        /// alla medesima istanza ed al medesimo database che caratterizzano la connessione ODBC
        /// passata come parametro.
        /// </p>
        /// <p>Nel caso la scopra, la funzione restituisce il <b>nome</b> della stringa di connessione.</p>
        /// 
        /// <remarks>
        /// Questa funzione deve essere usata con estrema cautela perchè:
        /// <ol>
        /// <li>Potrebbe non restituire alcun risultato, sebbene la stringa di connessione esista nel file <i>web.config</i>.</li>
        /// <li>Potrebbe restituire il nome di una stringa di connessione diversa da quella desiderata 
        /// nel caso in cui nel file <i>web.config</i> esistessero più stringhe di connessione con la medesima istanza e 
        /// con il medesimo database.</li>
        /// </ol>
        /// </remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 03/05/2015
        /// Mdfd: 01/09/2015 - BUG SULL'INDIVIDUAZIONE DELLA STRINGA DI CONNESSIONE
        /// </pre>
        /// </summary>
        /// <param name="p_conn">Connessione ODBC di riferimento.</param>
        /// <returns>Il nome di una stringa di connessione presente nel file <i>web.config</i>, ovvero null.</returns>
        protected virtual String guessOLEDBConnectionStringName(System.Data.Odbc.OdbcConnection p_conn)
        {

            System.Configuration.Configuration _webConfig;
            String  _cs;
            Boolean _csIS=false;                                    //integrated security
            String  _csName=null;  
            String  _csUID=null;
            String _csDatabase = null;
            String _csInstance = null;            
            String[] _csComponents=null;
            char[] _separators = { ';', '=' };

            /*
             @MdN: 01/09/2015
             */
            Boolean _IS = false;
            String _Database = null;
            String _Instance = null;
            String _UID = null;
            String[] _Components = null;

            //controllo
            if(p_conn==null)
                return null;

            // Apertura della connessione
            if (p_conn.State == System.Data.ConnectionState.Closed)
                p_conn.Open();
            
            //Aperura file di configurazione
            _webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.config");

            foreach (System.Configuration.ConnectionStringSettings _strColl in _webConfig.ConnectionStrings.ConnectionStrings)
            {
                //Ciclare nella collezione connectionstrings
                _cs = _strColl.ConnectionString;
                _csName = _strColl.Name;
                
                //Analisi della connection string
                if (_cs == null)
                    continue;
                _csComponents = _cs.Split(_separators);
                if (_csComponents.Length > 0)
                {
                    // 0, 2, 4, 6 ... pari: i nomi dei campi
                    // 1, 3, 5, 7 ... dispari: i corrispondenti valori
                    // Cerco 'Data Source'
                    for (int _t = 0; _t < _csComponents.Length; _t++)
                    {
                        if (_csComponents[_t].Trim().ToLower().CompareTo("data source") == 0)
                        {
                            if (_t + 1 < _csComponents.Length)
                                _csInstance = _csComponents[_t + 1];
                        }
                    }
                    // Cerco 'initial catalog'
                    for (int _t = 0; _t < _csComponents.Length; _t++)
                    {
                        if (_csComponents[_t].Trim().ToLower().CompareTo("initial catalog") == 0)
                        {
                            if (_t + 1 < _csComponents.Length)
                                _csDatabase = _csComponents[_t + 1];
                        }
                    }
                    // Cerco 'integrated security'
                    for (int _t = 0; _t < _csComponents.Length; _t++)
                    {
                        if (_csComponents[_t].Trim().ToLower().CompareTo("integrated security") == 0)
                        {
                            if (_t + 1 < _csComponents.Length)
                            {
                                if (_csComponents[_t + 1].Trim().ToLower().CompareTo("true") == 0)
                                    _csIS = true;
                            }
                        }
                    }
                    // Cerco 'uid'
                    if (_csIS == false)
                    {
                        for (int _t = 0; _t < _csComponents.Length; _t++)
                        {
                            if (_csComponents[_t].Trim().ToLower().CompareTo("uid") == 0)
                            {
                                if (_t + 1 < _csComponents.Length)
                                {
                                    _csUID = _csComponents[_t + 1];
                                }
                            }
                        }
                    }

                }// FINE ANALISI DELLA CONNECTIONSTRING
                // Confronto con i parametri odbc

                /* ***
                 * @MdN: 01/09/2015
                 * BUG SCOPERTO: non è corretto usare la proprietà DataSource in quanto il codice può 
                 * risultare debole.
                 *  ***/

                if (p_conn.ConnectionString != null)
                {
                    _Components = p_conn.ConnectionString.Split(_separators);
                    if (_Components.Length > 0)
                    {
                        // 0, 2, 4, 6 ... pari: i nomi dei campi
                        // 1, 3, 5, 7 ... dispari: i corrispondenti valori
                        // Cerco 'server <-> instance'
                        for (int _t = 0; _t < _Components.Length; _t++)
                        {
                            if (_Components[_t].Trim().ToLower().CompareTo("server") == 0)
                            {
                                if (_t + 1 < _Components.Length)
                                    _Instance = _Components[_t + 1];
                            }
                        }
                        // Cerco 'Database <-> initial catalog'
                        for (int _t = 0; _t < _Components.Length; _t++)
                        {
                            if (_Components[_t].Trim().ToLower().CompareTo("database") == 0)
                            {
                                if (_t + 1 < _Components.Length)
                                    _Database = _Components[_t + 1];
                            }
                        }
                        // Cerco 'integrated security'
                        for (int _t = 0; _t < _Components.Length; _t++)
                        {
                            if (_Components[_t].Trim().ToLower().CompareTo("trusted_connection") == 0)
                            {
                                if (_t + 1 < _Components.Length)
                                {
                                    if (_Components[_t + 1].Trim().ToLower().CompareTo("yes") == 0)
                                        _IS = true;
                                }
                            }
                        }
                        // Cerco 'uid'
                        if (_IS == false)
                        {
                            for (int _t = 0; _t < _Components.Length; _t++)
                            {
                                if (_Components[_t].Trim().ToLower().CompareTo("uid") == 0)
                                {
                                    if (_t + 1 < _Components.Length)
                                    {
                                        _UID = _Components[_t + 1];
                                    }
                                }
                            }
                        }
                    }
                    Boolean _test = false;

                    // TODEL: 01/09/2015
                    //if(_csInstance!=null && p_conn.DataSource.ToLower().CompareTo(_csInstance.ToLower())==0)
                    //    _test = true;
                    //if(_csDatabase!=null && p_conn.Database.ToLower().CompareTo(_csDatabase.ToLower())==0)
                    //    _test=_test & true;
                    //if (_test == true)
                    //{
                    //    p_conn.Close();
                    //    return _csName;
                    //}
                    // TODEL: 01/09/2015

                    // @MdN 01/09/2015
                    if (_csInstance != null && _Instance.ToLower().CompareTo(_csInstance.ToLower()) == 0)
                        _test = true;
                    if (_csDatabase != null && _Database.ToLower().CompareTo(_csDatabase.ToLower()) == 0)
                        _test = _test & true;
                    if (_test == true)
                    {
                        p_conn.Close();
                        return _csName;
                    }
                }

            }// fine foreach
            p_conn.Close();
            return null;
        }
        #endregion
    }
}
