﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using simplex_ORM;

namespace simplex_FORM
{
    /// <summary>
    /// Elemento che rappresenta una briciola di pane
    /// @MdN
    /// -----
    /// Crtd: 11/01/2015
    /// </summary>
    class SimplexBreadCrumb
    {

        /// <summary>
        /// Nome dell'elemento BreadCrumb. Verrà visualizzato.
        /// DEVE ESSERE SEMPRE VALORIZZATA
        /// </summary>
        protected String MyName;

        /// <summary>
        /// Nome della variabile di sessione dove viene conservata la tabella.
        /// </summary>
        protected String MySessionTabName;
        
        /// <summary>
        /// Collegamento alla form di destinazione.
        /// Nel caso di chiamate GET, deve contenere i parametri in formato urlencoded
        /// DEVE ESSERE SEMPRE VALORIZZATA
        /// </summary>
        protected String MyLink;

        /// <summary>
        /// Riferimento all'oggetto SQLTable corrente.
        /// Nel caso sia nullo, l'oggetto DEVE trovarsi in sessione.
        /// </summary>
        protected simplex_ORM.SQLSrv.SQLTable MyTBL_UNDER_FORM;

        /// <summary>
        /// Riferimento all'oggetto precedentemente immesso
        /// </summary>
        protected SimplexBreadCrumb MyNext;

        /// <summary>
        /// Oggetto generico di supporto.
        /// </summary>
        protected object MyObject;
        
        #region COSTRUTTORI
        protected SimplexBreadCrumb(String p_Name, String p_Lnk)
        {
            MyName = p_Name;
            MyLink = p_Lnk;
        }
        #endregion

        #region CREATORI
        /// <summary>
        /// Creazione di un nuovo elemento.
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento</param>
        /// <param name="p_Lnk">Link dell'elemento</param>
        /// <returns>L'elemento appena creato.</returns>
        internal static SimplexBreadCrumb createNewBreadCrumb(String p_Name, String p_Lnk)
        {
            SimplexBreadCrumb _sbc = new SimplexBreadCrumb(p_Name, p_Lnk);
            return _sbc;
        }

        /// <summary>
        /// Creazione di un nuovo elemento.
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento</param>
        /// <param name="p_Lnk">Link dell'elemento</param>
        /// <param name="p_TBL">Oggetto SQLTable di riferimento</param>
        /// <returns>L'elemento appena creato.</returns>
        internal static SimplexBreadCrumb createNewBreadCrumb(String p_Name, String p_Lnk, simplex_ORM.SQLSrv.SQLTable p_TBL)
        {
            SimplexBreadCrumb _sbc = SimplexBreadCrumb.createNewBreadCrumb(p_Name, p_Lnk);
            _sbc.MyTBL_UNDER_FORM = p_TBL;
            return _sbc;
        }

        /// <summary>
        /// Creazione di un nuovo elemento.
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento</param>
        /// <param name="p_Lnk">Link dell'elemento</param>
        /// <param name="p_TBL">Oggetto SQLTable di riferimento</param>
        /// <param name="p_SessionName">Nome di sessione della tabella</param>
        /// <returns>L'elemento appena creato.</returns>
        internal static SimplexBreadCrumb createNewBreadCrumb(String p_Name, String p_Lnk, simplex_ORM.SQLSrv.SQLTable p_TBL, String p_SessionName)
        {
            SimplexBreadCrumb _sbc = SimplexBreadCrumb.createNewBreadCrumb(p_Name, p_Lnk, p_TBL);
            _sbc.MySessionTabName = p_SessionName;
            return _sbc;
        }
        #endregion

        /// <summary>
        /// Creazione di un nuovo elemento.
        /// @MdN
        /// ----
        /// Crtd: 25/01/2015
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento</param>
        /// <param name="p_Lnk">Link dell'elemento</param>
        /// <param name="p_TBL">Oggetto SQLTable di riferimento</param>
        /// <param name="p_SessionName">Nome di sessione della tabella</param>
        /// <param name="p_Obj">Eventuale oggetto di supporto</param>
        /// <returns>L'elemento appena creato.</returns>
        internal static SimplexBreadCrumb createNewBreadCrumb(String p_Name, String p_Lnk, simplex_ORM.SQLSrv.SQLTable p_TBL, String p_SessionName, object p_Obj)
        {
            SimplexBreadCrumb _sbc = SimplexBreadCrumb.createNewBreadCrumb(p_Name, p_Lnk, p_TBL);
            _sbc.MySessionTabName = p_SessionName;
            _sbc.MyObject = p_Obj;
            return _sbc;
        }

        #region READ ONLY PROPERTIES
        /// <summary>
        /// Restituisce il nome dell'elemento che viene visuializzato 
        /// all'interno del controllo
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015
        /// </summary>
        public String Name
        {
            get
            {
                return MyName;
            }
        }

        /// <summary>
        /// Restituisce l'indirizzo della pagina/form.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015
        /// </summary>
        public String Link
        {
            get
            {
                return MyLink;
            }
        }

        /// <summary>
        /// Restituisce l'oggetto SQLTable (tabella) di riferimento. Se null, prendere l'oggetto
        /// direttamente dalla sessione.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015
        /// </summary>
        public simplex_ORM.SQLSrv.SQLTable TBL_UNDER_FORM
        {
            get
            {
                return MyTBL_UNDER_FORM;
            }
        }

        /// <summary>
        /// Restituisce l'oggetto di supporto.
        /// @MdN
        /// ----
        /// Crtd: 25/01/2015
        /// </summary>
        public object SupportObject
        {
            get
            {
                return MyObject;
            }
        }

        /// <summary>
        /// Restituisce il nome dell'oggetto SQLTable nella sessione corrente
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015 
        /// </summary>
        public String TableSessionName
        {
            get
            {
                return MySessionTabName;
            }
        }

        /// <summary>
        /// Restituisce/Imposta l'oggetto di classe SimplexBreadCrumb successivo al corrente.
        /// </summary>
        public SimplexBreadCrumb Next
        {
            get
            {
                return MyNext;
            }
        }
        #endregion

        #region metodi pubblici
        /// <summary>
        /// Confronta l'oggetto di classe SimplexBreadCrumb corrente
        /// con un oggetto della stessa classe passatpo quale parametro.
        /// Il confronto viene eseguito solo sui campi Nome e Link.
        /// Restituisce un intero.
        /// > 0: i due oggetti sono SUPERFICIALMENTE uguali
        /// > 1: i due oggetti differiscono per Link
        /// > 2: i due oggetti differiscono per Nome
        /// > 3: i due oggetti differiscono per Nome e per Link
        /// > 4: l'oggetto passato come parametro non è istanziato.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015
        /// </summary>
        /// <param name="p_toCompare">Oggetto di classe SimplexBreadCrumb da confrontare.</param>
        /// <returns>un intero compreso tra -1 e +3.</returns>
        /// <example>Se due oggetti devono essere considerati uguali anche se hanno l'attributo Link diverso
        /// è sufficiente impostare risultato del confronto su 0 e (AND) 1.</example>
        public int CompareTo(SimplexBreadCrumb p_toCompare)
        {
            if (p_toCompare == null)
                return 4;

            int _toRet = 0;
            if (this.MyName.CompareTo(p_toCompare.MyName) != 0)
                _toRet = _toRet | 2;
            if (this.MyLink.CompareTo(p_toCompare.MyLink) != 0)
                _toRet = _toRet | 1;
            return _toRet;
        }

        /// <summary>
        /// Interrompe la catena di oggetti di classe SimplecBreadCrumb.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015
        /// </summary>
        public void breakChain()
        {
            MyNext = null;
        }
        #endregion

        #region ALTRI METODI

        /// <summary>
        /// Aggiunge in coda (valorizzando l'attributo MyNext) l'oggetto passato come parametro
        /// anche se nullo. In quest'ultimo caso il metodo si comporta come breakChain.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015
        /// </summary>
        /// <param name="p_sbc">Oggetto di classe SimplexBreadCrumb da accodare.</param>
        internal void addNext(SimplexBreadCrumb p_sbc)
        {
            MyNext = p_sbc;
        } // fine


        #endregion






    }
}
