﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace simplex_FORM
{
    /// <summary>
    /// Classe che rappresenta una collezione di oggetti di classe SimplexBreadCrumb.
    /// @MdN
    /// ----
    /// Crtd: 11/01/2015
    /// </summary>
    public class SimplexBreadCrumbs
    {
        /// <summary>
        /// Testa della strttura
        /// </summary>
        private SimplexBreadCrumb MyHead = null;

        /// <summary>
        /// Stringa di separazione delle voci.
        /// </summary>
        protected String MySeparator = " -> ";

        #region COSTRUTTORI
        /// <summary>
        /// Nessuna OPerazione
        /// </summary>
        public SimplexBreadCrumbs()
        {
            //NOP
        }

        /// <summary>
        /// Costruisce un nuovo oggetto specificando il separatore.
        /// Se il parametro è null, allora viene creato un novo oggetto con il separatore di default;
        /// Se il parametro ha lunghezza nulla allora viene creato un nuovo oggetto con il separatore [CLR SPACE]
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015
        /// </summary>
        /// <param name="p_separator">Stringa che rappresenta il separatore.</param>
        public SimplexBreadCrumbs(String p_separator)
        {
            if (p_separator != null)
                if (p_separator.Length == 0)
                    MySeparator = " ";
                else
                    MySeparator = p_separator;
            
        }

        #endregion


        #region METODI PUBBLICI

        /// <summary>
        /// Inserisce un elemento SimplexBreadCrumb nella posizione specificata ed 
        /// interrompe la catena (cioè l'elemento aggiunto diventa l'ultime elemento).
        /// La posizione è 0-based.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        /// <param name="p_sbc">Elemento da inserire nella catena.</param>
        /// <param name="p_position">Posizone 0-based di inserimento.</param>
        /// <returns>Il numero di elementi presenti nella catena.</returns>
        private int InsertAndBreak(SimplexBreadCrumb p_sbc, int p_position)
        {
            SimplexBreadCrumb temp, prev;
            int counter = 0;

            if (p_sbc == null)
                return 0;

            // Nel caso di catena vuota, si inserisce in testa e si esce.
            if (MyHead == null)
            {
                MyHead = p_sbc;
                return 1;
            }

            // scorrere la catena fino alla posizione indicata o fino al termine
            temp = MyHead;
            prev = null; ;
            while (counter < p_position && temp != null)
            {
                prev = temp;
                temp = temp.Next;
                counter += 1;
            }
            // punto di inserimento raggiunto.
            if (prev != null)
                prev.addNext(p_sbc);
            else
                MyHead = p_sbc;
            return counter+1;
        }

        /// <summary>
        /// Inserisce un elemento SimplexBreadCrumb nella posizione specificata ed 
        /// interrompe la catena (cioè l'elemento aggiunto diventa l'ultime elemento).
        /// La posizione è 0-based.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        /// <param name="p_Link">URL relativo della pagina/form da inserire nella catena.</param>
        /// <param name="p_Name">Nome dell'elemento della catena.</param>
        /// <param name="p_position">Posizone 0-based di inserimento.</param>
        /// <returns>Il numero di elementi presenti nella catena o [-2] in caso di errore.</returns>
        public int InsertAndBreak(String p_Name, String p_Link, int p_position)
        {
            SimplexBreadCrumb _sbc = SimplexBreadCrumb.createNewBreadCrumb(p_Name, p_Link);
            if (_sbc == null)
                return -2;
            return InsertAndBreak(_sbc, p_position);
        }

        /// <summary>
        /// Accoda un elemento SimplexBreadCrumb. Se nell'accodamento viene trovato un elemento che 
        /// ha lo stesso nome, quell'elemento viene sostituito con quello nuovo che diventa, dunque,
        /// l'ultimo anello della catena.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015 
        /// </summary>
        /// <param name="p_sbc">Elemento da inserire.</param>
        /// <returns>In numero di elemenrti che compongono la catena.</returns>
        private int Add(SimplexBreadCrumb p_sbc)
        {
            SimplexBreadCrumb temp, prev;
            int counter = 0;

            if (p_sbc == null)
                return 0;
            //inserimento in testa
            if (MyHead == null)
            {
                MyHead = p_sbc;
                return 1;
            }
            temp = MyHead;
            prev = null;
            // Ciclo di esplorazione della lista
            while (temp != null)
            {
                if (temp.CompareTo(p_sbc) <= 1)
                {
                    // TROVATO UN ELEMENTO CON LO STESSO NOME!!!
                    // --> SOSTITUZIONE ED INTERRUZIONE DELLA TRACCIA BREADCRUMBS
                    if (prev == null)
                    {
                        MyHead = p_sbc;
                        return 1;
                    }
                    else
                    {
                        prev.addNext(p_sbc); // INTERROMPE!!!
                        return counter;
                    }
                }
                prev = temp;
                temp = temp.Next;
                counter++;
            }// fine while
            prev.addNext(p_sbc);
            return counter;
        }//fine

        /// <summary>
        /// Crea un nuovo elemento SimplexBreadCrumb e lo inserisce in coda nella catena.
        /// Se nell'accodamento viene trovato un elemento che ha lo stesso nome, 
        /// quell'elemento viene sostituito con quello nuovo che diventa, dunque,
        /// l'ultimo anello della catena.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015 
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento (visualizzato)</param>
        /// <param name="p_Link">Pagina/maschera di destinazione.</param>
        /// <returns>Numero di elementi che costituisce la catena.</returns>
        public int Add(String p_Name, String p_Link)
        {
            SimplexBreadCrumb _sbc = SimplexBreadCrumb.createNewBreadCrumb(p_Name, p_Link);
            return Add(_sbc);
        }

        /// <summary>
        /// Crea un nuovo elemento SimplexBreadCrumb e lo inserisce in coda nella catena.
        /// Se nell'accodamento viene trovato un elemento che ha lo stesso nome, 
        /// quell'elemento viene sostituito con quello nuovo che diventa, dunque,
        /// l'ultimo anello della catena.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015  
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento (visualizzato)</param>
        /// <param name="p_Link">Pagina/maschera di destinazione.</param>
        /// <param name="p_TBL">Oggetto SQLtable a cui fa riferimento la maschera.</param>
        /// <returns>Numero di elementi che costituisce la catena.</returns>
        public int Add(String p_Name, String p_Link, simplex_ORM.SQLSrv.SQLTable p_TBL)
        {
            SimplexBreadCrumb _sbc = SimplexBreadCrumb.createNewBreadCrumb(p_Name, p_Link, p_TBL);
            return Add(_sbc);
        }

        /// <summary>
        /// Crea un nuovo elemento SimplexBreadCrumb e lo inserisce in coda nella catena.
        /// Se nell'accodamento viene trovato un elemento che ha lo stesso nome, 
        /// quell'elemento viene sostituito con quello nuovo che diventa, dunque,
        /// l'ultimo anello della catena.
        /// @MdN
        /// ----
        /// Crtd: 11/01/2015  
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento (visualizzato)</param>
        /// <param name="p_Link">Pagina/maschera di destinazione.</param>
        /// <param name="p_TBL">Oggetto SQLtable a cui fa riferimento la maschera.</param>
        /// <param name="p_TabSessionName">Nome in sessione dell'oggetto SQLtable a cui fa riferimento la maschera</param>
        /// <returns>Numero di elementi che costituisce la catena.</returns>
        public int Add(String p_Name, String p_Link, simplex_ORM.SQLSrv.SQLTable p_TBL, String p_TabSessionName)
        {
            SimplexBreadCrumb _sbc = SimplexBreadCrumb.createNewBreadCrumb(p_Name, p_Link, p_TBL,p_TabSessionName);
            return Add(_sbc);
        }

        /// <summary>
        /// Crea un nuovo elemento SimplexBreadCrumb e lo inserisce in coda nella catena.
        /// Se nell'accodamento viene trovato un elemento che ha lo stesso nome, 
        /// quell'elemento viene sostituito con quello nuovo che diventa, dunque,
        /// l'ultimo anello della catena.
        /// @MdN
        /// ----
        /// Crtd: 25/01/2015  
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento (visualizzato)</param>
        /// <param name="p_Link">Pagina/maschera di destinazione.</param>
        /// <param name="p_TBL">Oggetto SQLtable a cui fa riferimento la maschera.</param>
        /// <param name="p_TabSessionName">Nome in sessione dell'oggetto SQLtable a cui fa riferimento la maschera</param>
        /// <param name="p_Obj">Oggetto di supporto.</param>
        /// <returns>Numero di elementi che costituisce la catena.</returns>
        public int Add(String p_Name, String p_Link, simplex_ORM.SQLSrv.SQLTable p_TBL, String p_TabSessionName, object p_Obj)
        {
            SimplexBreadCrumb _sbc = SimplexBreadCrumb.createNewBreadCrumb(p_Name, p_Link, p_TBL, p_TabSessionName,p_Obj);
            return Add(_sbc);
        }


        /// <summary>
        /// Restituisce l'oggetto di classe SimplexBreadCrumb caratterizzato dal nome
        /// passato per parametro.
        /// @MdN
        /// ----
        /// Crtd: 12/01/2015
        /// </summary>
        /// <param name="p_Name">Nome dell'oggetto da cercare.</param>
        /// <returns>L'oggetto trovato o null in caso di ricerca negativa.</returns>
        internal SimplexBreadCrumb getItem(String p_Name)
        {
            SimplexBreadCrumb _toRet = null;
            if (MyHead == null)
                return null;
            _toRet = MyHead;
            while (_toRet != null)
            {
                if (_toRet.Name.CompareTo(p_Name) == 0)
                    return _toRet;
                _toRet = _toRet.Next;
            }
            _toRet = null;
            return _toRet;
        }//fine

        /// <summary>
        /// Restituisce l'attributo Name dell'oggetto SimplexBreadCrumbs in posizione p_idx ovvero
        /// null se non viene trovato alcun oggetto nella posizione p_idx specificata.
        /// @MdN
        /// ----
        /// Crtd: 12/01/2015
        /// </summary>
        /// <param name="p_idx">Indice</param>
        /// <returns>il nome dell'oggetto SimplexBreadCrumb in posizione idx o null.</returns>
        public String getName(int p_idx)
        {
            SimplexBreadCrumb _sbc = null;
            int _Count = 0;
            _sbc = MyHead;

            if (_sbc == null)
                return null;
            
            while (_sbc != null)
            {
                if (_Count == p_idx)
                    return _sbc.Name;
                _Count++;
                _sbc = _sbc.Next;
            }
            return null;
        }


        /// <summary>
        /// Restituisce il Link della pagina/form il cui nome viene passato come parametro.
        /// @MdN
        /// -----
        /// Crtd: 12/01/2015
        /// </summary>
        /// <param name="p_Name">parametro Name</param>
        /// <returns>la proprietà Link o null.</returns>
        public String getLink(String p_Name)
        {
            SimplexBreadCrumb _sbc = getItem(p_Name);
            if (_sbc == null)
                return null;
            return _sbc.Link;
        }


        /// <summary>
        /// Restituisce il Link della pagina/form che si trova in posizione p_idx.
        /// @MdN
        /// -----
        /// Crtd: 12/01/2015
        /// </summary>
        /// <param name="p_Name">indice posizionale</param>
        /// <returns>la proprietà Link o null.</returns>
        public String getLink(int p_idx)
        {
            String _Name = null;
            _Name = getName(p_idx);
            if (_Name == null)
                return null;
            return getLink(_Name);
        }
        

        /// <summary>
        /// Restituisce il nome di sessione dell'oggetto SQLTable corrispondente alla pagina/form
        /// il cui nome viene passato come parametro.
        /// @MdN
        /// -----
        /// Crtd: 12/01/2015
        /// </summary>
        /// <param name="p_Name">parametro Name</param>
        /// <returns>la proprietà TableSessionName o null.</returns>
        public String getTableSessionName(String p_Name)
        {
            SimplexBreadCrumb _sbc = getItem(p_Name);
            if (_sbc == null)
                return null;
            return _sbc.TableSessionName;
        }

        /// <summary>
        /// Restituisce il nome di sessione dell'oggetto SQLTable corrispondente alla pagina/form
        /// la cui posizione viene passata come parametro.
        /// @MdN
        /// -----
        /// Crtd: 12/01/2015
        /// </summary>
        /// <param name="p_Name">posizione dell'elemento</param>
        /// <returns>la proprietà TableSessionName o null.</returns>
        public String getTableSessionName(int p_idx)
        {

            String _Name = null;
            _Name = getName(p_idx);
            if (_Name == null)
                return null;
            return getTableSessionName(_Name);
        }

        /// <summary>
        /// Restituisce l'oggetto SQLTable corrispondente alla pagina/form
        /// il cui nome viene passato come parametro.
        /// Nel caso il metodo ritornasse null, l'oggetto SQLTable può essere
        /// prelevato dalla sessione.
        /// @MdN
        /// -----
        /// Crtd: 12/01/2015
        /// </summary>
        /// <param name="p_Name">parametro Name</param>
        /// <returns>l'oggetto SQLTable o null.</returns>
        public simplex_ORM.SQLSrv.SQLTable getTBL_UNDER_FORM(String p_Name)
        {
            SimplexBreadCrumb _sbc = getItem(p_Name);
            if (_sbc == null)
                return null;
            return _sbc.TBL_UNDER_FORM;
        }


        /// <summary>
        /// Restituisce l'oggetto SQLTable corrispondente alla pagina/form
        /// la cui posizione viene passata come parametro.
        /// Nel caso il metodo ritornasse null, l'oggetto SQLTable può essere
        /// prelevato dalla sessione.
        /// @MdN
        /// -----
        /// Crtd: 12/01/2015
        /// </summary>
        /// <param name="p_Name">posizione dell'oggetto nella catenaparam>
        /// <returns>l'oggetto SQLTable o null.</returns>
        public simplex_ORM.SQLSrv.SQLTable getTBL_UNDER_FORM(int p_idx)
        {
            String _Name = null;
            _Name = getName(p_idx);
            if (_Name == null)
                return null;
            return getTBL_UNDER_FORM(_Name);
        }


        /// <summary>
        /// Restituisce l'oggetto di supporto corrispondente alla pagina/form
        /// il cui nome viene passato come parametro.
        /// @MdN
        /// -----
        /// Crtd: 25/01/2015
        /// </summary>
        /// <param name="p_Name">parametro Name</param>
        /// <returns>l'oggetto SQLTable o null.</returns>
        public object getSupportObject(String p_Name)
        {
            SimplexBreadCrumb _sbc = getItem(p_Name);
            if (_sbc == null)
                return null;
            return _sbc.SupportObject;
        }

        /// <summary>
        /// Restituisce l'oggetto di supporto corrispondente alla pagina/form
        /// in posizione specificata dal parametro.
        /// @MdN
        /// -----
        /// Crtd: 25/01/2015
        /// </summary>
        /// <param name="p_idx">Posizione della pagina/form nella catena</param>
        /// <returns>l'oggetto SQLTable o null.</returns>
        public object getSupportObject(int p_idx)
        {
            String _Name = null;
            _Name = getName(p_idx);
            if (_Name == null)
                return null;
            return getSupportObject(_Name);
        }


        /// <summary>
        /// Restituisce (RO) il numero di elementi costituenti la catena di BreadCrumb.
        /// @MdN
        /// -----
        /// Crtd: 12/01/2015 
        /// </summary>
        public int Count
        {
            get
            {
                if (MyHead == null)
                    return 0;
                SimplexBreadCrumb _sbc = null;
                int _Count = 0;
                _sbc = MyHead;
                while (_sbc != null)
                {
                    _Count++;
                    _sbc = _sbc.Next;
                }
                return _Count;                              
            }
        }


        /// <summary>
        /// <p>
        /// Restituisce il link della pagina precedente a quella corrente.
        /// </p>
        /// <p>La pagina corrente è, per definizione, l'ultima della catena di elementi BreadCrumbs. Dunque il
        /// metodo restituisce il link alla pagina precedente, cioè la pagina chiamante. Questo metodo è utilissimo
        /// per implementare la funzione del tasto 'INDIETRO'.
        /// </p>
        /// <p>Se la catena di breadcrumbs è costituita solo da un elemento, cioè la HOME, la funzione restituisce
        /// l'indirizzo di se stessa.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 21/04/2015
        /// Mdfd: 30/04/2015
        /// </pre>
        /// </summary>
        /// <returns>il link della penultima pagina della catena di SimplexBreadCrumb</returns>
        public String getPreviousLink()
        {
            SimplexBreadCrumb _current = null;
            SimplexBreadCrumb _previous = null;

            //Caso particolare @MdN 30/04/2015
            if (this.MyHead.Next == null)
                return MyHead.Link;

            //Caso generale
            _current = this.MyHead;
            while (_current.Next != null)
            {
                _previous = _current;
                _current = _current.Next;
            }

            return _previous.Link;
        }

        #endregion

        #region PROPERTIES
        /// <summary>
        /// Imposta o ottiene la stringa usata come separatore.
        /// @MdN
        /// ----
        /// Crtd: 14/01/2015
        /// </summary>
        public String Separator
        {
            get
            {
                return MySeparator;
            }

            set
            {
                if (value == null)
                    MySeparator = "";
                else
                    MySeparator = value;
            }
        }
        
        #endregion
    }
}
